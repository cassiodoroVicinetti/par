\chapter{Cenni sull'architettura degli apparati di rete}
L'architettura dei router si è evoluta nel corso del tempo per aumentarne sempre di più la capacità di elaborazione dei pacchetti:
\begin{itemize}
\item \ul{prima generazione} (fino ad inizio 1990): inferiore a 500 Mbps (sez.~\ref{sez:router_prima_generazione});
\item \ul{seconda generazione} (inizio 1990): circa 5 Gbps (sez.~\ref{sez:router_seconda_generazione});
\item \ul{terza generazione} (fine 1990): a partire da 50 Gbps (sez.~\ref{sez:router_terza_generazione});
\item \ul{multi-chassis} (tendenza recente): dell'ordine dei Tbps (sez.~\ref{sez:router_multichassis}).
\end{itemize}

%2
\section{Prima generazione}
\label{sez:router_prima_generazione}
\begin{figure}
	\centering
	\includegraphics[scale=1.05]{../pic/B1/2}
	\caption{Architettura dei router di prima generazione.}
\end{figure}

\noindent
I router di prima generazione erano sostanzialmente dei PC modificati:
\begin{itemize}
\item \ul{interfacce di rete}: sono delle normali NIC, in numero maggiore e di molteplici tipologie rispetto a quelle di un normale PC;
\item \ul{memoria}: si preferisce la memoria allo stato solido (ad es. scheda SD) perché meno soggetta a guasti rispetto a un disco fisso meccanico;
\item \ul{sistema operativo}: è ottimizzato specificamente per l'elaborazione del traffico di rete.
\end{itemize}

\paragraph{Vantaggio} \ul{economia di scala}: usa componenti fabbricati a larga scala anziché componenti dedicati al mondo del networking.

\paragraph{Svantaggi}
\begin{itemize}
\item \ul{colli di bottiglia}:
\begin{itemize}
\item \ul{bus condiviso}:
\begin{itemize}
\item \ul{slow path}: ogni pacchetto transita due volte sul bus;
\item \ul{arbitraggio}: una NIC per volta può usare il bus $\Rightarrow$ non possono lavorare più interfacce in parallelo;
\end{itemize}
\item \ul{accesso a memoria}: la tabella di instradamento è memorizzata in una struttura dati nella memoria generica $\Rightarrow$ gli accessi non sono così localizzati come nei PC tradizionali, e la cache è poco usata;
\item \ul{capacità di elaborazione}: la CPU lancia un interrupt al sistema operativo ogni volta che arriva un pacchetto;
\end{itemize}

\item \ul{interfacce di rete}: le schede di rete tradizionali tipicamente non hanno molte porte e supportano solo i tipi di interfacce più comuni;
\item \ul{costi di manutenzione}: l'uso di componenti open-source (ad es. client PPP) porta problemi di integrazione e variegate modalità di configurazione.
\end{itemize}

Attualmente questa architettura è usata per i router di fascia medio-bassa, le cui prestazioni sono adeguate per i piccoli uffici.
\FloatBarrier

\section{Seconda generazione}
\label{sez:router_seconda_generazione}
\begin{figure}
	\centering
	\includegraphics[scale=1.05]{../pic/B1/3}
	\caption{Architettura dei router di seconda generazione.}
\end{figure}

%3-12-13-14
\noindent
I router di seconda generazione cercano di risolvere il problema delle prestazioni spostando il carico di elaborazione dalla CPU centrale alle unità periferiche: le NIC sono rimpiazzate dalle \textbf{line card}, schede di rete ``intelligenti'' che aggiungono ai componenti fisici/MAC dei moduli per l'inoltro:
\begin{itemize}
\item \ul{CPU}: il \textbf{packet processor} è un processore dedicato all'elaborazione dei pacchetti ``semplici'';
\item \ul{memoria}: ogni pacchetto in arrivo viene memorizzato in una memoria locale separata da quella (generalmente TCAM) contenente la tabella di instradamento;
\item \ul{tabella di instradamento}: ogni tanto la tabella di instradamento aggiornata viene copiata dall'unità centrale alle line card.
\end{itemize}

%4
I pacchetti possono seguire due strade:
\begin{itemize}
\item \textbf{fast path}: i pacchetti che rientrano nel caso tipico (ad es. IPv4) transitano una volta sola sul bus: vengono elaborati direttamente dal packet processor e vengono inviati subito all'interfaccia di uscita;
\item \textbf{slow path}: i pacchetti meno frequenti (ad es. IPv6, OSPF) vengono affidati alla CPU centrale per un'elaborazione più sofisticata, al costo di un throughput decisamente inferiore.
\end{itemize}

%16
Il lavoro delle line card deve essere coordinato dalla CPU centrale, su cui inoltre girano i protocolli di instradamento (ad es. OSPF), che è montata su una scheda detta \textbf{supervisor} nei sistemi di fascia alta.

\paragraph{Vantaggi}
\begin{itemize}
\item \ul{CPU centrale}: lavora solo per i pacchetti lungo il fast path $\Rightarrow$ può avere una potenza inferiore;
\item \ul{ottimizzazione del fast path}: i primi pacchetti della connessione seguono lo slow path, poi il sistema centrale segna la connessione come idonea al fast path e tutti i pacchetti successivi seguiranno il fast path;
\item \ul{flessibilità}: l'introduzione di un nuovo protocollo (ad es. IPv6) richiede solo l'aggiornamento del software nel sistema centrale.
\end{itemize}

\paragraph{Svantaggio} \ul{arbitraggio del bus condiviso}: una line card per volta può usare il bus $\Rightarrow$ non possono lavorare più interfacce in parallelo.
\FloatBarrier

\section{Terza generazione}
\label{sez:router_terza_generazione}
\begin{figure}
	\centering
	\includegraphics[scale=1.05]{../pic/B1/5}
	\caption{Architettura dei router di terza generazione.}
\end{figure}

%5-15
\noindent
I router di terza generazione si concentrano sul problema della parallelizzazione del lavoro rimpiazzando il bus condiviso con una \textbf{switching fabric} (o crossbar) in grado di gestire trasferimenti multipli: una line card è collegata a un'altra line card mettendo in cortocircuito l'interruttore all'intersezione dei relativi ``fili'' della matrice di commutazione.

%6
Due line card non possono essere collegate ad un'altra stessa line card allo stesso tempo $\Rightarrow$ è necessario un \textbf{arbitro} che pilota i punti di commutazione e risolve le situazioni di contesa senza collisioni.

Quando le interfacce di uscita sono lente nell'invio dei pacchetti rispetto alla velocità di commutazione della switching fabric, i pacchetti possono essere accodati in diversi modi:
\begin{itemize}
%7
\item \ul{output queuing}: i buffer sono posizionati sulle interfacce di uscita $\Rightarrow$ caso peggiore: switching fabric $N$ volte più veloce della velocità di ricezione, con $N$ = numero di interfacce di ingresso;

%8
\item \ul{input queuing}: i buffer sono posizionati sulle interfacce di ingresso $\Rightarrow$ la velocità di commutazione della switching fabric non dipende dal numero di interfacce di ingresso, ma soffre il problema dell'\textbf{head-of-line blocking}:
\begin{enumerate}
\item due pacchetti in due code di ingresso diverse sono destinati alla stessa coda di uscita;
\item l'arbitro fa passare uno dei pacchetti bloccando l'altro;
\item tutti i pacchetti successivi nella coda di ingresso, anche quelli destinati a code di uscita libere, devono aspettare il ``primo della fila'';
\end{enumerate}

%9
\item \ul{virtual output queuing}: risolve il problema dell'head-of-line blocking mantenendo ad ogni ingresso una coda per uscita;

%10
\item \ul{buffered fabric}: i buffer sono posizionati all'interno della crossbar in corrispondenza dei nodi di commutazione.
\end{itemize}

\paragraph{Svantaggi}
\begin{itemize}
\item \ul{arbitro}:
\begin{itemize}
\item costituisce un collo di bottiglia, soprattutto in caso di decisioni frequenti dovute a pacchetti piccoli (ad es. ATM);
\item le politiche di schedulazione per la qualità del servizio possono complicarne l'hardware;
\end{itemize}
\item \ul{buffer per le code}: sono memorie costose perché devono essere veloci.
\end{itemize}
\FloatBarrier

%11-18-19
\section{Router multi-chassis}
\label{sez:router_multichassis}
I \textbf{router multi-chassis} puntano sulla scalabilità: più rack affiancati sono collegati in modo da apparire come un router unico:
\begin{itemize}
\item la \ul{potenza di elaborazione} è moltiplicata per il numero di chassis;
\item c'è più spazio fisico per le interfacce di rete $\Rightarrow$ a uno stesso apparato può essere collegato un \ul{gran numero di link};
\item l'instradamento è concentrato in una \ul{singola centrale} (come nel mondo telefonico) anziché in tanti piccoli router sparsi per la rete $\Rightarrow$ la centrale dati può servire tutti gli utenti in un'area geografica estesa (ad es. Piemonte).
\end{itemize}

\section{Service card}
\label{sez:service_card}
Un'altra tendenza recente punta sulla flessibilità: i router non sono più degli apparati puramente di livello 3, poiché stanno aggiungendosi molte altre funzioni che non appartengono al livello 3, come cache, firewall, monitor di rete, ACL, ecc. L'obiettivo è personalizzare il servizio offerto dagli ISP, facendo ritornare una parte del business attualmente in mano ai content provider.

%17
Una soluzione oggi comune sono le \textbf{service card}, line card arricchite con servizi a valore aggiunto preconfigurati dal costruttore. Le service card non sono però così flessibili: non essendo programmabili, per aggiungere un servizio occorre acquistare una nuova service card.

%21
\section{Maggiori problematiche attuali}
Il throughput ormai non è più una problematica attuale, ma le maggiori problematiche attuali sono:
\begin{itemize}
\item \ul{costi di acquisto}: per ridurre il costo degli apparati di rete si sta passando da componenti dedicati a componenti general-purpose prodotti su larga scala:
\begin{itemize}
\item le CPU di massa hanno cicli di vita del prodotto troppo brevi e tempi di ricambio troppo lunghi $\Rightarrow$ Intel ha proposto di recente delle CPU embedded con cicli di vita del prodotto più lunghi e tempi di ricambio più brevi rispetto alle CPU di massa;
\item nei sistemi dedicati il programmatore poteva sfruttare la gerarchia di memorie, memorizzando la RIB in una memoria lenta e la FIB in una memoria veloce (ad es. TCAM), mentre i sistemi general-purpose decidono autonomamente che cosa mettere nella cache $\Rightarrow$ occorre rivolgersi alle GPU, dotate di memorie partizionate ma di versatilità di elaborazione limitata;
\end{itemize}

\item \ul{costi operativi}: il consumo di elettricità e la dissipazione termica possono essere significativi;

\item \ul{flessibilità}: bisogna muoversi verso la separazione delle funzioni di controllo dall'hardware fisico (si rimanda al capitolo~\ref{cap:SDN}).
\end{itemize}

% Catalist 6500: e' vecchia, ma continua ad essere presente. Si aggiorna un pezzo per volta. Si tratta di un'architettura estremamente longeva. Uno dei problemi principali, ma anche uno dei punti di forza era la necessita' di compatibilita' con i componenti piu' vecchi.