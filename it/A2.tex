\chapter{Routing Information Protocol}
%3
Il \textbf{Routing Information Protocol} (RIP) è un protocollo di instradamento intra-dominio basato sull'algoritmo Distance Vector (DV). RIP versione 1 fu definito nel 1988 e fu il primo protocollo di instradamento utilizzato su Internet.

RIP, a seconda dell'implementazione, include i meccanismi dell'orizzonte limitato, del route poisoning e del path hold down per limitare la propagazione di informazioni di instradamento non corrette.

%23
Il RIP è adatto per reti piccole, stabili e omogenee:
 \begin{itemize}
  \item \ul{piccole}: la metrica è basata semplicemente sul \textbf{numero di hop} (ogni link ha costo 1), ma non si può superare il limite di 16 hop $\Rightarrow$ non sono ammessi più di 15 router in cascata in uno stesso dominio RIP;
  \item \ul{stabili}: cambiamenti di stato possono scatenare dei transitori di lunga durata;
  \item \ul{omogenee}:
  \begin{itemize}
  \item link omogenei: non è possibile differenziare i costi su link diversi in base alla larghezza di banda;
  \item router omogenei: ogni router deve terminare l'elaborazione prima di produrre il suo nuovo DV $\Rightarrow$ la durata del transitorio è legata alle prestazioni del router più lento.
 \end{itemize}
\end{itemize}

\section{Formato dei pacchetti}
%4
I pacchetti RIP hanno il formato seguente:
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c}
    \multicolumn{1}{r@{}}{\footnotesize{8}} & \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{32}} & \multicolumn{1}{}{} \\
    \cline{1-3}
    Command & Version (1) & \makebox[3cm]{0} & \\
    \hline
    \multicolumn{2}{|c|}{Address Family Identifier} & \makebox[3cm]{0} & \parbox[t]{0mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\footnotesize{$\times$ N $\leq$ 25}}}} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{IP Address} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{0} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{0} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Metric} \\
    \hline
  \end{tabular}
  \caption{Formato di un pacchetto RIP (da 24 a 504 byte).}
\end{table}
%5
\noindent
dove i campi più significativi sono:
\begin{itemize}
 \item campo \ul{Command} (1 byte): specifica il tipo di messaggio:
 \begin{itemize}
  \item valore ``Response'': il pacchetto trasporta un DV contenente uno o più indirizzi;
  \item valore ``Request'': un router appena connesso alla rete notifica i vicini della sua presenza $\Rightarrow$ i vicini invieranno in risposta i loro DV senza dover attendere il timeout, aumentando la velocità di convergenza;
 \end{itemize}
 \item campo \ul{Address Family Identifier} (2 byte): specifica il protocollo di livello rete utilizzato (ad es. valore 2 = IP);
 \item campo \ul{IP Address} (4 byte): specifica l'indirizzo IP annunciato (senza netmask).\\
 Possono essere annunciati fino a 25 indirizzi in un singolo pacchetto RIP;
 \item campo \ul{Metric} (4 byte): specifica il costo relativo all'indirizzo annunciato.
\end{itemize}

%6
\paragraph{Incapsulamento} Il pacchetto RIP è incapsulato in un pacchetto UDP:
\begin{itemize}
 \item la \ul{porta UDP} di destinazione è la porta 520, che all'epoca fu scelta come meccanismo di sicurezza dato che le porte inferiori a 1024 possono essere utilizzate solo sotto privilegi amministrativi;
 \item l'\ul{indirizzo IP} di destinazione è l'indirizzo broadcast (255.255.255.255) $\Rightarrow$ tutti gli apparati possono riceverlo, compresi gli host, anche se conviene disabilitare i protocolli di instradamento al lato host per proteggerli da attacchi malevoli e apprendere rotte migliori ascoltando gli eventuali messaggi ICMP Redirect:
 \begin{figure}[H]
	\centering
	\includegraphics[width=0.35\linewidth]{../pic/A2/6.png}
 \end{figure}
\end{itemize}

\section{Timer}
Il RIP è pesantemente basato su timer:
\begin{itemize}
 \item è difficile rispettare precisamente dei timer fissati perché la CPU potrebbe essere impegnata $\Rightarrow$ le incertezze introducono degli ulteriori ritardi;
 \item tutti i router nella rete devono usare gli stessi timer, altrimenti i router possono interagire in modo non coordinato.
\end{itemize}

%7
\subsection[Routing update timer]{Routing update timer \textmd{{\small (predefinito 30 s)}}}
Definisce ogni quanto tempo vengono inviati i messaggi Response gratuiti contenenti le informazioni sui DV.

%10
\paragraph{Sincronizzazione dei router} Si cerca di evitarla non reimpostando il routing update timer all'invio di un triggered update e inviando i messaggi Response gratuiti con un ritardo variabile tra 25 e 35 secondi.

\subsection[Route invalid timer]{Route invalid timer \textmd{{\small (predefinito 180 s)}}}
Definisce il tempo per cui una entry può rimanere valida nella tabella di instradamento senza che venga rinfrescata. Quando scade il route invalid timer, il numero di hop della entry viene impostato al costo infinito (16), contrassegnando la destinazione come irraggiungibile.

\paragraph{Rilevamento dei guasti} Il router invalid timer è soprattutto utile per rilevare una connettività mancante verso un vicino quando il segnale ``link down'' non è disponibile.

%8
\subsection[Route flush timer]{Route flush timer \textmd{{\small (predefinito 240 s)}}}
Definisce il tempo per cui una entry può rimanere nella tabella di instradamento senza che venga rinfrescata. Quando scade il route flush timer, la entry viene cancellata dalla tabella di instradamento.

%12
\paragraph{Route poisoning} Quando il route invalid timer scade e la entry viene contrassegnata come non valida, rimangono 60 s (con i valori predefiniti) in cui il router può annunciare la destinazione a costo infinito, per informare gli altri router prima che la entry venga cancellata.

%13
\subsection[Hold down timer]{Hold down timer \textmd{{\small (predefinito 180 s)}}}
Definisce il tempo per cui una entry non può subire modifiche in seguito a un sospetto inizio di count to infinity. L'hold down timer è una funzionalità proprietaria di Cisco.

\paragraph{Path hold down} L'hold down timer parte quando il numero di hop sta salendo a un valore più alto, per evitare di innescare un count to infinity e consentire alla rotta di stabilizzarsi.

%12
\paragraph{Route poisoning} Anche una destinazione bloccata dall'algoritmo path hold down può essere propagata a costo infinito.

\section{Limitazioni}
\subsection{Netmask}
La prima versione di RIP fu definita quando era ancora in uso l'\textbf{indirizzamento classful}, dove la subnet mask può essere ricavata automaticamente dall'indirizzo di rete stesso $\Rightarrow$ gli indirizzi di rete annunciati nei DV sono privi di informazioni sulle netmask $\Rightarrow$ la versione 1 di RIP può essere usata solo in reti dove ogni indirizzo appartiene a una \textbf{classe di indirizzi} secondo le vecchie regole dell'indirizzamento classful.

È possibile adottare uno stratagemma per far funzionare la versione 1 di RIP in reti con indirizzi a netmask di lunghezza variabile: dato un indirizzo di rete annunciato, il router esamina gli indirizzi di rete assegnati alle interfacce connesse:
\begin{itemize}
 \item se ad almeno un'interfaccia è assegnato un indirizzo avente una \ul{subnet mask} uguale alla subnet mask dell'indirizzo annunciato, il router assume come netmask dell'indirizzo annunciato la netmask dell'indirizzo dell'interfaccia;
 \item se a nessuna interfaccia è assegnato un indirizzo avente una subnet mask uguale alla subnet mask dell'indirizzo dato, il router assume come netmask dell'indirizzo annunciato la sua subnet mask.
\end{itemize}

\paragraph{Problemi} Può essere assunta una netmask errata se:
\begin{itemize}
 \item nessuna delle interfacce del router ha la subnet mask cercata;
 \item l'indirizzo annunciato ha in realtà una netmask diversa da quella dell'indirizzo dell'interfaccia selezionata.
\end{itemize}

%9
\subsection{Limite di numero di hop}
Il RIP definisce il limite di numero di hop uguale a 16 $\Rightarrow$ le destinazioni la cui distanza è maggiore di 15 sono considerate non raggiungibili.

Un valore massimo così basso è stato scelto per limitare il ben noto problema del count to infinity degli algoritmi basati su DV: quando il costo di una rotta raggiunge il valore 16, la rotta è considerata irraggiungibile e il costo non può salire ancora di più.

Questo \ul{non} significa che la rete non può avere più di 15 router in cascata: l'unico effetto è che due router troppo distanti non possono comunicare tra loro direttamente. È possibile risolvere questo problema partizionando la rete in due domini di instradamento, controllati da due differenti istanze di protocollo RIP, e attivando il processo di ridistribuzione tra di essi in modo da ``falsificare'' i costi delle rotte esterne.

%14
\subsection{Mancanza del campo ``age''}
Il RIP non associa un campo ``age'' alle rotte nei DV $\Rightarrow$ le informazioni annunciate possono essere vecchie, ma il router che le riceve le assume come nuove e reimposta i timer a zero $\Rightarrow$ la durata del transitorio aumenta tanto più quanto ci si allontana dal cambiamento di stato.

\paragraph{Esempio} In una rete con topologia A\textemdash B\textemdash C:\footnote{Si assumono: valori dei timer predefiniti, no triggered update, no route poisoning.}
\begin{itemize}
 \item tempo 0 s: avviene un guasto sul link A\textemdash B $\Rightarrow$ il nodo A non è più raggiungibile;
 \item tempo 179 s: il nodo B annuncia al nodo C il suo DV, l'ultimo a contenere la destinazione A;
 \item tempo 180 s: il nodo B contrassegna la destinazione A come non raggiungibile;
 \item tempo 359 s: il nodo C contrassegna la destinazione A come non raggiungibile.
\end{itemize}

\section{RIP versione 2}
%16
\textbf{RIP versione 2} estende la prima versione di RIP sfruttando alcuni campi che erano inutilizzati nei messaggi:
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c}
    \multicolumn{1}{r@{}}{\footnotesize{8}} & \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{32}} & \multicolumn{1}{}{} \\
    \cline{1-3}
    Command & Version (2) & Routing Domain & \\
    \hline
    \multicolumn{2}{|c|}{Address Family Identifier} & Route Tag & \parbox[t]{0mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\footnotesize{$\times$ N $\leq$ 25}}}} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{IP Address} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Subnet Mask} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Next Hop} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Metric} \\
    \hline
  \end{tabular}
  \caption{Formato di un pacchetto RIP versione 2 (da 24 a 504 byte).}
\end{table}
%15
\noindent
dove i nuovi campi sono:
\begin{itemize}
 \item campo \ul{Routing Domain} (2 byte): specifica il dominio di instradamento a cui è destinato questo messaggio RIP per la gestione di più domini di instradamento sullo stesso router di frontiera:
 \begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{../pic/A2/15}
	\caption{Il router A scarta i messaggi destinati al dominio B.}
 \end{figure}
 %17
 \item campo \ul{Route Tag} (2 byte): specifica se l'indirizzo annunciato è una \textbf{rotta esterna}, ovvero è stata appresa tramite un processo di ridistribuzione da un altro dominio di instradamento;
 \item campo \ul{Subnet Mask} (4 byte): contiene la \textbf{netmask} associata all'indirizzo di rete annunciato per supportare l'indirizzamento classless;
 %18
 \item campo \ul{Next Hop} (4 byte): ottimizza l'instradamento quando più router RIP appartengono alla stessa LAN ma a due domini RIP diversi, e quindi il traffico da un dominio all'altro passerebbe sempre dal router di frontiera $\Rightarrow$ il router di frontiera può annunciare di mandare il traffico direttamente al router next hop nell'altro dominio:
 \begin{figure}[H]
	\centering
	\includegraphics[scale=0.97125]{../pic/A2/18}
	\caption{Il router di frontiera B insegna al router A di usare il router C come next hop per la destinazione D.}
 \end{figure}
\end{itemize}

%19
\subsection{Autenticazione}
RIP versione 2 introduce un meccanismo di \textbf{autenticazione basata su password}: un router deve essere autenticato per poter annunciare il suo DV ai suoi vicini.

Se la prima entry nel pacchetto RIP ha il campo ``Address Family Identifier'' uguale al valore 0xFFFF, allora il resto della entry contiene le informazioni di autenticazione:
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|}
    \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{32}} \\
    \hline
    0xFFFF & Authentication Type \\
    \hline
    \multicolumn{2}{|c|}{} \\
    \hdashline
    \multicolumn{2}{|c|}{Authentication} \\
    \hdashline
    \multicolumn{2}{|c|}{} \\
    \hdashline
    \multicolumn{2}{|c|}{} \\
    \hline
  \end{tabular}
  \caption{Formato della entry di autenticazione in un pacchetto RIP.}
\end{table}
\noindent
dove i campi sono:
\begin{itemize}
 \item campo \ul{Authentication Type} (2 byte): solo il tipo ``password semplice'' è stato definito (valore 2);
 \item campo \ul{Authentication} (16 byte): contiene la password in chiaro.
\end{itemize}

Questo meccanismo di autenticazione è piuttosto debole perché la password può essere facilmente sniffata $\Rightarrow$ è raramente utilizzato. Meccanismi di autenticazione più complessi non sono possibili a causa della mancanza di spazio nel messaggio RIP.

%20
\subsection{Multicast}
RIP versione 1 invia il DV in broadcast $\Rightarrow$ tutte le entità, compresi gli host, devono elaborare i messaggi RIP.

RIP versione 2 definisce un indirizzo IP multicast (224.0.0.9) di destinazione, in modo che il pacchetto RIP venga ricevuto solo dalle entità che sono iscritte al \textbf{gruppo multicast} $\Rightarrow$ gli host e i router che non utilizzano il protocollo RIP possono scartare il pacchetto a livello data-link.

%23
\section{Vantaggi}
\begin{itemize}
 \item è adatto per reti piccole, stabili e omogenee;
 \item richiede poche risorse di elaborazione;
 \item è semplice da implementare;
 \item è semplice da configurare (non ci sono i sottodomini come in OSPF);
 \item è disponibile su un'ampia gamma di apparati, anche su router economici.
\end{itemize}