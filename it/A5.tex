\chapter{Open Shortest Path First}
\label{cap:OSPF}
%3
L'\textbf{Open Shortest Path First} (OSPF) è un protocollo di instradamento intra-dominio basato sull'algoritmo Link State (LS). Le prime due versioni sono utilizzate con IPv4, mentre la versione 3 è pensata per IPv6.

%77
Il principale vantaggio di OSPF rispetto agli altri protocolli di instradamento intra-dominio è la \ul{scalabilità} (fino a qualche centinaio di router):
\begin{itemize}
\item \ul{algoritmo LS}: la conoscenza della topologia della rete permette una maggiore stabilità rispetto ai protocolli basati sull'algoritmo Distance Vector;
\item \ul{instradamento gerarchico}: OSPF suggerisce di non avere più di 200 router in una singola area:
\begin{itemize}
\item le informazioni di instradamento sulle altre aree possono essere sommarizzate;
\item i cambiamenti delle rotte in un'area non perturbano le altre aree.
\end{itemize}
\end{itemize}

\section{Aree}
\begin{figure}
 %4
 \centering
 \includegraphics[scale=0.925]{../pic/A5/4}
 \caption{Esempio di rete OSPF.}
\end{figure}

%5-6
\noindent
OSPF definisce la propria terminologia, che non è sempre allineata a quella degli altri protocolli:
\begin{description}
\item[Autonomous System (AS)] un dominio sotto il controllo di una singola entità dal punto di vista \ul{amministrativo} (ad es. GARR)\footnote{Si veda la sezione~\ref{sez:AS}.}
\item[Autonomous System OSPF] un dominio sotto il controllo di una singola entità dal punto di vista \ul{tecnico} (cioè di configurazione degli apparati), e gestito da una singola istanza di protocollo OSPF
\item[Autonomous System boundary router (ASBR)] un router di frontiera posto tra l'AS OSPF (tipicamente l'area 0) e un dominio di instradamento esterno (EGP, oppure IGP se l'AS OSPF convive all'interno dell'AS stesso insieme ad altri domini di instradamento con protocolli IGP anche differenti)
\item[area edge] uno dei sotto-domini gerarchici in cui è suddiviso un AS OSPF, composto da una \ul{rete fisicamente contigua}: ogni router interno può comunicare con qualsiasi altro router nella stessa area senza dover uscire dall'area stessa
\item[area 0] l'area di backbone, non necessariamente fisicamente contigua\footnote{Si rimanda alla sezione~\ref{sez:OSPF_Virtual_Link}.}, attraverso la quale deve passare tutto il traffico tra un'area edge e l'altra o tra un'area edge e l'esterno dell'AS OSPF:
\begin{itemize}
 \item \ul{no collo di bottiglia}: i link non devono essere sottodimensionati
 \item \ul{robusta}: non deve diventare un'area partizionata
\end{itemize}
\item[area border router (ABR)] un router di frontiera posto tra l'area 0 e un'area edge
\end{description}
\FloatBarrier

\begin{figure}
 \centering
 \includegraphics{../pic/A5/39}
 \caption{Vista della rete dall'area 1.}
\end{figure}

%39-40
Ogni router conosce perfettamente la topologia dell'area a cui appartiene, ma la topologia precisa delle altre aree non è nota: il router \ul{può} conoscere la lista delle destinazioni raggiungibili al di fuori della sua area, che possono venire sommarizzate o sostituite da una rotta di default.

%33
Il database di un router interno contiene tre tipi di record:
\begin{itemize}
\item \ul{Link State}: sono generati dagli altri router interni all'area e contengono le rotte interne all'area, incluse le informazioni sulla topologia, che non vengono mai sommarizzate.\\
Un ABR conosce i Link State di entrambe le aree che connette: ha più database, uno per ogni area, che danno origine naturalmente a una singola tabella di instradamento;
\item Summary/External Record: contengono le rotte esterne all'area, escluse le informazioni sulla topologia (solo indirizzo di rete + netmask), che possono essere sommarizzate:
\begin{itemize}
\item \ul{Summary Record}: sono generati dall'ABR e contengono le rotte esterne in altre aree dello stesso AS OSPF (compresa l'area 0);
\item \ul{External Record}: sono generati dall'ASBR e contengono le rotte esterne al di fuori dell'AS OSPF.
\end{itemize}
\end{itemize}

%15
I router nell'area 0 sono solitamente configurati al fine di aggregare gli indirizzi di rete, così da propagare i riassunti delle reti da un'area all'altra. Tuttavia l'aggregazione deve essere specificata manualmente dall'operatore, al fine di non avere problemi con la sommarizzazione di rete.
\FloatBarrier

\subsection{Aree stub}
%7-8
Un'area edge normale, oltre a conoscere i dettagli sulla topologia di tutte le rotte interne all'area stessa, importa tutte le rotte esterne dall'area 0 senza alcuna ulteriore aggregazione.

Un'area è \textbf{stub} quando alcune rotte esterne sono sostituite da una singola rotta di default per ridurre le informazioni di instradamento importate dall'esterno:
\begin{itemize}
\item \ul{area stub}: mantiene i Summary Record, ma rimuove gli External Record;
\item \ul{area totally stubby}: rimuove sia i Summary Record sia gli External Record, lasciando solo i Link State e la singola rotta di default verso l'uscita;
\item \ul{area not-so-stubby}: è simile all'area stub, ma può iniettare in altre aree le rotte esterne all'AS OSPF (senza importarle nell'area).
\end{itemize}

Le aree stub sono attivate su esplicita configurazione del gestore della rete:
\begin{itemize}
 \item area stub: \texttt{area \textit{xx} stub}
 \item area totally stubby: \texttt{area \textit{xx} stub no summary}
 \item area not-so-stubby: \texttt{area \textit{xx} nssa}
\end{itemize}

%9-10
Sebbene OSPF non impedisca di avere un'area stub con più di un ABR, ha più senso configurare un'area stub quando è connessa all'area 0 tramite un solo ABR: non è necessario propagare le rotte esterne perché c'è un solo percorso che connette l'area al resto della rete, mentre le rotte esterne sono utili solo se esiste più di un router egress.

%Supponiamo R15 avere una connessione ad internet. Area 3 come una not so stubby area. Puo’ dire quali sono le rotte esterne che lui raggiunge per dare in modo all’area 0 di decidere come raggiungere una rotta esterna, ma non e’ in grado di accettare dall’area 0 una distribuzione delle rotte…. Non puo’ sapere che in area 0 c’e’ un’atra via d’uscita verso internet.

\subsection{Virtual Link}
Il \textbf{Virtual Link} è una sorta di ``tunnel'' tra due router, di cui almeno uno appartenente all'area 0, che logicamente appartiene all'area 0, ma fisicamente è composto da una sequenza di link all'interno di un'area edge. Lo scopo è far credere a OSPF che quei due router siano collegati da un link fittizio in area 0.

%31
L'attivazione del Virtual Link richiede solo l'area da attraversare (una sola) e i Router ID dei due router coinvolti, non gli indirizzi IP delle loro interfacce: OSPF deriverà automaticamente gli indirizzi IP corretti. I messaggi di instradamento OSPF sono incapsulati in pacchetti unicast IP che attraversano il link $\Rightarrow$ per avere un tunnel bidirezionale, occorre configurare il Virtual Link su entrambi i router alle estremità.

\subsubsection{Aree partizionate}
\label{sez:OSPF_Virtual_Link}
\begin{figure}
\centering
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[scale=0.925]{../pic/A5/27}
		\caption{Partizionamento di un'area edge.}
	\end{subfigure}%
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[scale=0.925]{../pic/A5/29}
		\caption{Partizionamento dell'area 0.}
	\end{subfigure}
\end{figure}

%26
\noindent
Il problema delle aree partizionate\footnote{Si veda la sezione~\ref{sez:aree_partizionate}.} è gestito in OSPF differentemente a seconda del tipo di area:
\begin{itemize}
%27-28
\item \ul{area edge}: l'ABR non sommarizza le informazioni su tutte le reti presenti nell'area edge, ma annuncia solo le reti che è in grado di raggiungere $\Rightarrow$ i pacchetti verso la partizione verranno inviati solo agli ABR per cui esiste un percorso interno per giungere a destinazione;
%29-30
\item \ul{area 0}: OSPF non è in grado di risolvere automaticamente i problemi di aree partizionate nel backbone (viene scelto sempre l'ABR più vicino come punto di uscita), ma in alcuni casi l'operatore può attivare manualmente tra due ABR un Virtual Link che fisicamente passa attraverso un'area edge: quando un pacchetto arriva a un ABR, rientra nell'area andando all'ABR all'altra estremità del tunnel per poi finalmente entrare nell'area 0 $\Rightarrow$ l'area 0 deve sempre essere logicamente contigua, ma non necessariamente lo è fisicamente.
\end{itemize}
\FloatBarrier

\subsubsection{Estensione dell'area di backbone}
\begin{figure}
 \centering
 \includegraphics[scale=0.925]{../pic/A5/30}
\end{figure}

\noindent
Un link tra due router appartenenti ad aree edge differenti normalmente non può essere usato: il traffico da un'area all'altra infatti deve passare sempre attraverso l'area 0.

Grazie al Virtual Link è possibile portare nel backbone un router di un'area edge che è collegato direttamente a un solo router di backbone: quel router interno diventa un ABR di accesso all'area 0, e quindi tutti i link ad esso collegati possono essere usati per il traffico.
\FloatBarrier

\section{Metriche e costi}
%12
OSPF supporta più di una metrica simultaneamente su un singolo link: il percorso migliore può essere, a seconda dei pacchetti, ad esempio:
\begin{itemize}
\item il percorso più \ul{breve};
\item il percorso con la migliore \ul{capacità di banda};
\item il percorso con il minore \ul{ritardo}.
\end{itemize}

OSPF consente di definire le metriche a seconda del campo ``Type of Service'' (ToS) del pacchetto IP $\Rightarrow$ in teoria, sono possibili 64 tipi di servizio e quindi 64 alberi di instradamento diversi, ma in pratica questa funzione è quasi inutilizzata perché il carico di elaborazione richiesto dall'algoritmo LS su ogni router sarebbe duplicato per ogni ToS.

%13-14
OSPF adotta il \ul{multipath routing a costi equivalenti}. A differenza di IGRP, OSPF non definisce un modo non ambiguo per calcolare il costo di un link: il costo è assegnato dal produttore dell'apparato di rete $\Rightarrow$ ogni produttore ha i propri valori predefiniti, creando possibili inconsistenze in reti multi-vendor $\Rightarrow$ è meglio personalizzare i valori di costo sui link più importanti (su entrambe le estremità).

%16
\section{Router ID}
Ogni router OSPF è identificato univocamente da un \textbf{Router ID}, che è usato come ``nome'' dei router nei pacchetti OSPF (ad es. come sorgente nell'intestazione OSPF).

OSPF non specifica come deve essere determinato il Router ID, ma si limita a specificare che deve essere un identificativo univoco lungo 32 bit. Sugli apparati Cisco, i Router ID possono essere ottenuti in due modi:
\begin{itemize}
\item \ul{manualmente}: l'amministratore di rete configura esplicitamente il valore del Router ID (in IPv4 tipicamente non si fa, mentre in IPv6 è obbligatorio\footnote{Si rimanda alla sezione~\ref{sez:OSPFv3}.});

\item \ul{automaticamente}: un algoritmo ricava il Router ID dagli indirizzi IPv4 del router:
\begin{itemize}
\item se c'è almeno un'interfaccia di loopback, il Router ID è uguale all'indirizzo più grande tra quelli delle \ul{interfacce di loopback}: le interfacce di loopback non dipendono dallo stato delle interfacce fisiche e sono così più stabili;
\item se non c'è alcuna interfaccia di loopback, il Router ID è uguale all'indirizzo più grande tra quelli delle \ul{interfacce di rete OSPF}.
\end{itemize}
\end{itemize}

\section{LSA}
%6
Il \textbf{Link State Advertisement} (LSA) è la struttura dati, contenuta nei pacchetti di Link State Update, che contiene le informazioni di instradamento OSPF:
%35
\begin{enumerate}
\item \textbf{Router LSA}: descrive un'adiacenza tramite un link punto-punto;

%23-38
\item \textbf{Network LSA}: elenca i router collegati a una rete di transito, ed è generato dal router designato della rete di transito;
% Il router principale si chiama designated router e backup designate router. 
% OSPF dice che questi sono i due router che hanno il router id piu’ alto. 
% Sono i primi due router che si accendono…
% Nelle reti si cerca sempre di evitare i transitori: una volta che sono stati decisi i desi.. e router design non cambiano piu’. 

%33
\item \textbf{Network Summary LSA}: contiene le rotte esterne in altre aree dello stesso AS OSPF (Summary Record), ed è generato da un ABR;

\item \textbf{ASBR Summary LSA}: comunica la posizione dell'ASBR se esso non si trova nell'area 0 ma in un'area edge.

\item \textbf{AS External LSA}: contiene le rotte esterne al di fuori dell'AS OSPF (External Record), ed è generato da un ASBR.
\end{enumerate}

\subsection{Router LSA}
\begin{figure}
\centering
	\begin{subfigure}[b]{.33\textwidth}
		%36
		\centering
		\includegraphics{../pic/A5/36}
		\caption{Router singolo.}
		\label{fig:router_lsa_a}
	\end{subfigure}%
	\begin{subfigure}[b]{.33\textwidth}
		%37
		\centering
		\includegraphics{../pic/A5/37}
		\caption{Router link.}
		\label{fig:router_lsa_b}
	\end{subfigure}%
	\begin{subfigure}[b]{.33\textwidth}
		%38
		\centering
		\includegraphics{../pic/A5/38}
		\caption{Network link.}
		\label{fig:router_lsa_c}
	\end{subfigure}
	\caption{Le adiacenze viste dal router R1.}
\end{figure}

%34
\noindent
OSPF definisce due tipi di link:
\begin{itemize}
\item \textbf{router link} (predefinito) (figura~\ref{fig:router_lsa_b}): in presenza di un link punto-punto tra due router (ad es. interfaccia seriale), ciascuno dei router lo vede suddiviso logicamente in due link punto-punto:
\begin{itemize}
\item una connessione punto-punto con il router adiacente, identificato dal suo Router ID;
\item una connessione punto-punto con la rete IP adiacente, chiamata \textbf{rete stub}\footnote{La ``rete stub'' non è da confondere con la ``area stub''.}, cioè quella a cui appartiene l'interfaccia di rete del router.\\
%36
Se l'interfaccia di un router è attiva ma non è collegata ad alcun altro router, solo la connessione con la rete stub è presente (figura~\ref{fig:router_lsa_a});
\end{itemize}

\item \textbf{network link} (figura~\ref{fig:router_lsa_c}): in presenza di una rete broadcast (ad es. Ethernet), chiamata \textbf{rete di transito}, ciascuno dei due\footnote{OSPF non impedisce di configurare un link punto-punto tra due router come una rete di transito.} o più router ad essa collegati la vede logicamente come una connessione punto-punto con la rete di transito adiacente.
\end{itemize}

%35
I Router LSA possono descrivere diversi tipi di adiacenze tramite link punto-punto:
\begin{enumerate}
%37
\item adiacenza con un \ul{router}: è generato da ciascuno dei due router adiacenti tra loro;
%38
\item adiacenza con una \ul{rete di transito}: è generato da ciascuno dei router adiacenti alla rete di transito (compreso il router designato);
%37
\item adiacenza con una \ul{rete stub}: è generato dal router la cui interfaccia è adiacente alla rete stub;
\item adiacenza con un \ul{Virtual Link}: è generato da ciascuno dei router alle estremità del Virtual Link.
\end{enumerate}
\FloatBarrier

% Router periferici, rete di switch di livello 2 per collegare i vari apparati.
% Infrastruttura di livello 2 puo’ essere estremamente vantaggiosa. Magari non si utilizza lo spanning tree. Se provo a mappare l’idea della rete ho un bus con tutti i router attaccati. Questa topologia fisica non si usa piu’, ma quella logica si’.
% Si tratta di topologie ancora attuali. Sono tante le reti che hanno un livello periferico L3. Topologia che va considerata dal punto di vista logico e non da quello fisico.

\section{Pacchetti OSPF}
%42
Tutti i pacchetti OSPF vengono incapsulati direttamente in IP (Protocol Type = 89), senza l'ausilio di un protocollo di trasporto intermedio.

%44
Un'intestazione OSPF, uguale per tutti i pacchetti, specifica il tipo del pacchetto OSPF trasportato:
\begin{itemize}
\item tipo 1: \textbf{Hello}
\item tipo 2: \textbf{Database Description}
\item tipo 3: \textbf{Link State Request}
\item tipo 4: \textbf{Link State Update}
\item tipo 5: \textbf{Link State Acknowledgement}
\end{itemize}

%32
\subsection{Protocollo di hello}
Il \textbf{protocollo di hello} effettua il \ul{rilevamento dei guasti} senza affidarsi al livello fisico.

I pacchetti Hello vengono inviati ogni HelloInterval (predefinito = 10 s), e l'adiacenza è considerata scomparsa e non viene più annunciata:
\begin{itemize}
\item appena il guasto è rilevato a livello fisico;
\item dopo un certo numero di pacchetti Hello persi (RouterDeadInterval predefinito = 40 s, equivalente a 4 pacchetti Hello), se il guasto non può essere rilevato a livello fisico.
\end{itemize}

L'LSA tuttavia rimane nel database OSPF, e se non rinnovato scade dopo un tempo pari al MaxAge (predefinito = 1 ora).

%E’ possibile anche velocizzare la convergenza di una rete OSPF (es. traffico voce). In reti ad alte prestazioni si puo’ arrivare a 100 ms con hello. I valori sono annunciati con gli hello packet per poter sincronizzare gli apparati. Impotante che tutti abbiano lo stesso valore.

%16
Il Router ID è calcolato all'avvio del processo OSPF, e non viene modificato anche se vengono modificati gli indirizzi IP sul router $\Rightarrow$ il router potrebbe apparire con un Router ID diverso al riavvio del processo OSPF (ad es. in seguito a un guasto o a un'interruzione dell'alimentazione) $\Rightarrow$ nella topologia calcolata dall'algoritmo LS rimane un nodo non più esistente, fino a quando non scade il relativo LSA.

%50
\subsection{Protocollo di exchange}
Il \textbf{protocollo di exchange} è usato per effettuare il \ul{riallineamento delle adiacenze}, cioè per sincronizzare il database di due router quando diventano adiacenti.

%18
Il riallineamento delle adiacenze viene svolto solo quando necessario, cioè quando un router ha delle informazioni non aggiornate nel suo database. La verifica della necessità di un aggiornamento del database viene fatta:
\begin{itemize}
\item in seguito a un cambiamento nella rete (ad es. in fase di avvio del sistema o quando diventa attivo un nuovo link);
\item a ogni scadenza del timer LSA Refresh (predefinito = 30 minuti), per rinfrescare l'age degli LSA ancora validi prima che scadano.
\end{itemize}

Durante il riallineamento delle adiacenze, vengono scambiati solo gli LSA vecchi o mancanti:
\begin{enumerate}
\item \ul{Database Description}: il router master invia la lista dei numeri di sequenza di tutti gli LSA nel suo database;
\item \ul{Link State Request}: il router slave invia la lista dei numeri di sequenza relativi agli LSA vecchi o mancanti nel suo database;
%19
\item \ul{Link State Update}: il router master invia gli LSA richiesti, e il pacchetto Link State Update viene propagato in selective flooding;
\item \ul{Link State Acknowledgement}: il router slave conferma la ricezione del Link State Update.
%Attenzione alle reti di tipo broacast. In questo caso il link state update viene inviato dal router verso il nodo designato, lo pseudo nodo lo conferma.
\end{enumerate}