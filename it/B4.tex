\chapter{Filtraggio dei pacchetti basato su software}
Il software in grado di analizzare i campi nei pacchetti trova spazio, eseguito su circuiti integrati o microprocessori, in varie applicazioni:
\begin{itemize}
 \item \ul{switch}: gli algoritmi di apprendimento si basano sugli indirizzi MAC sorgente e di destinazione delle trame\footnote{Si rimanda alla sezione \textit{Transparent bridge} nel capitolo \textit{Repeater e bridge} negli appunti di ``Progetto di reti locali''.}, e l'inoltro delle trame si basa sugli indirizzi MAC di destinazione;
 \item \ul{router}: l'inoltro dei pacchetti si basa sugli indirizzi IP sorgente e di destinazione;
 \item \ul{firewall}: se una regola basata sui campi del pacchetto è soddisfatta, lancia l'azione associata di filtraggio (ad es. scarta);
 \item \ul{NAT}: converte gli indirizzi IP tra privati e pubblici e le porte TCP/UDP di ogni pacchetto in transito;\footnote{Si rimanda al capitolo \textit{NAT} negli appunti di ``Reti di calcolatori''.}
 \item \ul{URL filter}: blocca il traffico HTTP da/verso URL di siti Web in una black list;
 \item \ul{pila protocollare}: il sistema operativo consegna il pacchetto alla pila di livello rete appropriata (ad es. IPv4 o IPv6), poi il pacchetto passa alla pila di livello trasporto appropriata (ad es. TCP o UDP), infine in base alla quintupla che identifica la sessione il pacchetto viene reso disponibile all'applicazione attraverso il socket giusto;
 \item \ul{cattura dei pacchetti}: le applicazioni per la cattura del traffico (ad es. Wireshark, tcpdump) possono impostare un filtro per ridurre la quantità di pacchetti catturati.
\end{itemize}

\section{Architettura tipica di un sistema di filtraggio dei pacchetti}
\begin{figure}
 %3
 \centering
 \includegraphics[scale=1.05]{../pic/B4/3.pdf}
 \caption{Architettura tipica di un sistema di filtraggio dei pacchetti.}
\end{figure}

\paragraph{Componenti di livello kernel}
\begin{itemize}
%5
\item \textbf{network tap}: intercetta i pacchetti dalla scheda di rete e li consegna a una o più\footnote{Ogni applicazione di cattura ha la sua pila di filtraggio, ma tutte condividono la stessa network tap.} pile di filtraggio;
%6
\item \textbf{packet filter}: lascia passare solo i pacchetti che soddisfano il filtro specificato dall'applicazione di cattura, aumentando l'efficienza della cattura: i pacchetti non voluti vengono scartati subito, e nel kernel buffer viene copiato un minor numero di pacchetti;
%9
\item \textbf{kernel buffer}: memorizza i pacchetti prima che vengano consegnati al livello user;
%12
\item \textbf{API di livello kernel}: fornisce al livello user le primitive, tipicamente chiamate di sistema \texttt{ioctl}, necessarie per accedere ai livelli sottostanti.
\end{itemize}

\paragraph{Componenti di livello user}
\begin{itemize}
%13
\item \textbf{user buffer}: memorizza i pacchetti nello spazio di indirizzamento dell'applicazione utente;
%14-15-17
\item \textbf{libreria di livello user} (ad es. libpcap, WinPcap): esporta delle funzioni che sono mappate con le primitive fornite dalla API di livello di kernel, e fornisce un compilatore di alto livello per creare al volo il codice pseudo-assembler da iniettare nel packet filter.
\end{itemize}
\FloatBarrier

\section{Principali sistemi di filtraggio dei pacchetti}
\subsection{CSPF}
%16
Il \textbf{CMU/Stanford Packet Filter} (CSPF, 1987) fu il primo packet filter, ed era implementato in parallelo alle altre pile protocollari.

Introdusse alcune migliorie chiave:
\begin{itemize}
 %4
 \item \ul{implementazione a livello kernel}: l'elaborazione è più veloce perché si evita il costo della commutazione di contesto tra il kernel space e lo user space, anche se è più facile danneggiare l'intero sistema;
 %10
 \item \textbf{packet batching}: il kernel buffer non consegna subito un pacchetto arrivato all'applicazione, ma aspetta che ne siano memorizzati un certo numero e poi li copia tutti insieme nello user buffer per ridurre il numero di commutazioni di contesto;
 %7
 \item \textbf{macchina virtuale}: i filtri non sono più hard-coded, ma il codice di livello user può istanziare in fase di esecuzione un pezzo di codice in linguaggio pseudo-assembler che specifica le operazioni di filtraggio per determinare se il pacchetto può passare o deve essere scartato, e una macchina virtuale nel packet filter, composta in pratica da uno \texttt{switch case} su tutte le istruzioni possibili, emula un processore che interpreta quel codice per ogni pacchetto in transito.
\end{itemize}

\subsection{BPF/libpcap}
%18
Il \textbf{Berkeley Packet Filter} (BPF, 1992) fu la prima implementazione seria di un packet filter, adottato storicamente dai sistemi BSD e usato ancora oggi in coppia con la libreria \textbf{libpcap} in user space.

\paragraph{Architettura}
\begin{itemize}
%5
\item \ul{network tap}: è integrata nel driver della NIC, e può essere chiamata con esplicite chiamate ai componenti di cattura;
%11
\item \ul{kernel buffer}: è suddiviso in due \ul{aree di memoria separate}, in modo che i processi di livello kernel e di livello user possano operare in modo indipendente (il primo scrive mentre il secondo legge) senza necessità di sincronizzazione sfruttando due core della CPU in parallelo:
\begin{itemize}
 \item lo \textbf{store buffer} è l'area in cui scrive il processo di livello kernel;
 \item l'\textbf{hold buffer} è l'area da cui legge il processo di livello user.
\end{itemize}
\end{itemize}

%http://www.winpcap.org/docs/docs_412/html/group__NPF.html
\subsection{NPF/WinPcap}
%19
La libreria \textbf{WinPcap} (1998), sviluppata inizialmente al Politecnico di Torino, può essere considerata un porting per Windows dell'intera architettura BPF/libpcap.

%20
\paragraph{Architettura}
\begin{itemize}
 %21
 \item \textbf{Netgroup Packet Filter} (NPF): è il componente di livello kernel e include:
 \begin{itemize}
  %5
  \item \ul{network tap}: giace sopra i driver della NIC, registrandosi come nuovo protocollo di livello rete accanto ai protocolli standard (come IPv4, IPv6);
  %24-25
  \item \ul{packet filter}: la macchina virtuale è un \textbf{compilatore just in time} (JIT): invece di interpretare il codice, lo traduce in istruzioni native del processore x86;
  %11
  \item \ul{kernel buffer}: è implementato come un \textbf{buffer circolare}: i processi di livello kernel e di livello user scrivono nella stessa area di memoria, e il processo di livello kernel sovrascrive i dati già letti dal processo di livello user $\Rightarrow$ ottimizza lo \ul{spazio} in cui memorizzare i pacchetti, ma:
  \begin{itemize}
 \item se il processo di livello user è troppo lento a leggere i dati, il processo di livello kernel potrebbe sovrascrivere dei dati non ancora letti (\textbf{cache pollution}) $\Rightarrow$ è necessaria la \ul{sincronizzazione} tra i due processi: il processo scrivente deve ispezionare periodicamente una variabile condivisa contenente la posizione di lettura corrente;
 \item l'area di memoria è condivisa tra i core della CPU $\Rightarrow$ il buffer circolare è \ul{meno efficiente dal punto di vista della CPU};
  \end{itemize}
 \end{itemize}
 
 %22
 \item \textbf{Packet.dll}: esporta a livello user delle funzioni, indipendenti dal sistema operativo, che sono mappate con le primitive fornite dalla API di livello di kernel;
 
 %23
 \item \textbf{Wpcap.dll}: è la libreria a collegamento dinamico con cui l'applicazione interagisce direttamente:
 \begin{itemize}
  \item offre al programmatore le \ul{funzioni di libreria} di alto livello necessarie per accedere ai livelli sottostanti (ad es. \texttt{pcap\_open\_live()}, \texttt{pcap\_setfilter()}, \texttt{pcap\_next\_ex()}/\texttt{pcap\_loop()});
  \item include il \ul{compilatore} che, dato un filtro definito dall'utente (ad es. stringa \texttt{ip}), crea il codice pseudo-assembler (ad es. ``se il campo `EtherType' è uguale a 0x800 restituisci vero'') da iniettare nel packet filter per il compilatore JIT;
  \item implementa lo \ul{user buffer}.
 \end{itemize}
\end{itemize}

%19
\paragraph{Nuove funzionalità}
\begin{itemize}
 \item \textbf{statistics mode}: registra dati statistici nel kernel senza commutazioni di contesto;
 \item \textbf{packet injection}: invia pacchetti attraverso l'interfaccia di rete;
 \item \textbf{remote capture}: attiva un server remoto che cattura i pacchetti e li consegna localmente.
\end{itemize}

\section{Ottimizzazioni prestazionali}
\begin{figure}
%37
\centering
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[scale=1.1]{../pic/B4/37a}
		\caption{Architettura tradizionale.}
	\end{subfigure}%
	\hspace{0.03\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[scale=1.1]{../pic/B4/37b}
		\caption{Architettura con shared buffer.}
	\end{subfigure}%
	\hspace{0.03\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
	%TODO La terza figura nella diapositiva è unica!
	%TODO Il sistema operativo c'è ancora ed è il riquadro blu.
		\centering
		\includegraphics[scale=1.1]{../pic/B4/37c}
		\caption{Architettura con driver accelerato.}
	\end{subfigure}
	\caption{Evoluzione delle tecniche di ottimizzazione delle prestazioni.}
\end{figure}

%28
\noindent
Negli ultimi anni il traffico sulle reti è cresciuto più velocemente rispetto alle prestazioni dei computer (memoria, CPU). Le prestazioni dell'elaborazione dei pacchetti possono essere migliorate in vari modi:
\begin{itemize}
\item \ul{incrementare le prestazioni della cattura}: migliorare la capacità di recapito dei dati al software;
\item \ul{creare componenti di analisi più intelligenti}: solamente i dati più interessanti vengono recapitati al software (ad es. l'URL per un URL filter);
\item \ul{ottimizzare l'architettura}: cercare di sfruttare le caratteristiche dell'applicazione per migliorare le prestazioni.
\end{itemize}

%34
\paragraph{Dati di profiling} (WinPcap 3.0, pacchetti da 64 byte)
\begin{itemize}
\item[\con] {\small [49,02\%]} \ul{driver NIC e sistema operativo}: quando entra nella NIC, il pacchetto impiega un sacco di tempo solo per arrivare alla pila di cattura:
\begin{enumerate}
\item la NIC trasferisce il pacchetto nella sua zona di memoria kernel tramite DMA (non usa la CPU);
\item la NIC lancia un \textbf{interrupt} (IRQ) al driver della NIC, interrompendo il programma correntemente in esecuzione;
\item il driver della NIC copia il pacchetto dalla memoria della NIC in una zona di memoria kernel del sistema operativo (usa la CPU);
\item il driver della NIC invoca il sistema operativo cedendo il controllo ad esso;
\item il sistema operativo chiama le varie pile protocollari registrate, incluso il driver di cattura;
\end{enumerate}
\item[\con] {\small [17,70\%]} \ul{tap processing}: operazioni svolte dal driver di cattura al principio della pila di cattura (ad es. ricevere i pacchetti, impostare gli interrupt);
\item[\con] {\small [8,53\%]} \ul{timestamping}: al pacchetto viene associato il timestamp;
\item[\pro] {\small [3,45\%]} \ul{packet filter}: i costi di filtraggio sono proporzionalmente bassi grazie al compilatore JIT;
\item[\con] \ul{doppia copia nei buffer}: il costo delle copie aumenta tanto più i pacchetti sono grandi:
\begin{enumerate}
\item {\small [9,48\%]} copia nel kernel buffer: il pacchetto è copiato dalla memoria del sistema operativo al kernel buffer;
\item {\small [11,50\%]} copia nello user buffer: il pacchetto è copiato dal kernel buffer allo user buffer;
\end{enumerate}
\item[\pro] {\small [0,32\%]} \ul{commutazione di contesto}: ha un costo insignificante grazie al packet batching.
\end{itemize}

\subsection{Interrupt}
%32-33
In tutti i sistemi operativi, ad un certo input rate la percentuale di pacchetti che arrivano all'applicazione di cattura non solo non aumenta più, ma diminuisce drasticamente a causa del \textbf{livelock}: gli interrupt sono così frequenti che il sistema operativo non ha il tempo di leggere i pacchetti dalla memoria della NIC e copiarli nel kernel buffer per consegnarli all'applicazione $\Rightarrow$ il sistema è vivo e sta facendo del lavoro, ma non sta facendo del lavoro utile.

%35
Esistono varie soluzioni per abbattere il costo degli interrupt:
\begin{itemize}
\item \textbf{interrupt mitigation} (basato su hardware): viene scatenato un interrupt solo quando è stato ricevuto un certo numero di pacchetti (un timeout evita la starvation se la soglia minima non è raggiunta entro un certo tempo);
\item \textbf{interrupt batching} (basato su software): quando arriva un interrupt, il sistema operativo serve il pacchetto arrivato e poi lavora in modalità polling: serve immediatamente i pacchetti successivi arrivati nel frattempo, fino a quando non ci sono più pacchetti e l'interrupt può essere riabilitato sulla scheda;
\item \textbf{device polling} (ad es. BSD [Luigi Rizzo]): il sistema operativo non attende più un interrupt, ma controlla autonomamente con un ciclo infinito la memoria della NIC $\Rightarrow$ siccome un core della CPU è perennemente impegnato nel ciclo infinito, questa soluzione è adatta quando sono necessarie prestazioni veramente elevate.
\end{itemize}

%36
\subsection{Timestamping}
Esistono due soluzioni per ottimizzare il timestamping:
\begin{itemize}
\item \ul{timestamp approssimato}: il tempo reale è letto solo ogni tanto, e il timestamp è basato sul numero di cicli di clock trascorsi dall'ultima lettura $\Rightarrow$ il timestamp dipende dalla frequenza di clock del processore, e i processori hanno frequenze di clock sempre maggiori;
\item \ul{timestamp hardware}: il timestamp è implementato direttamente nella scheda di rete $\Rightarrow$ i pacchetti arrivano al software già dotati di timestamp.
\end{itemize}

\subsection{Copia nello user buffer}
La zona di memoria kernel in cui è presente il kernel buffer viene \ul{mappata in user space} (ad es. tramite \texttt{nmap()}) $\Rightarrow$ la copia dal kernel buffer allo user buffer non è più necessaria: l'applicazione può leggere il pacchetto direttamente dallo \textbf{shared buffer}.

\paragraph{Implementazione} Questa soluzione è adottata in nCap di Luca Deri.

\paragraph{Problemi}
\begin{itemize}
%TODO ma il sistema operativo protegge da tentativi di scritture al di fuori dello spazio di indirizzamento logico mappato in user space
\item \ul{sicurezza}: l'applicazione accede a delle zone di memoria kernel $\Rightarrow$ può danneggiare il sistema;

\item \ul{indirizzamento}: il kernel buffer è visto attraverso due spazi di indirizzamento differenti: gli indirizzi usati in kernel space sono diversi dagli indirizzi usati in user space;
\item \ul{sincronizzazione}: l'applicazione e il sistema operativo devono lavorare su variabili condivise (ad es. le posizioni di lettura e scrittura dei dati).
\end{itemize}

%38
\subsection{Copia nel kernel buffer}
Il sistema operativo non è fatto per supportare un grande traffico di rete, ma è stato ingegnerizzato per eseguire applicazioni utente con un limitato consumo di memoria. Prima di arrivare alla pila di cattura, ogni pacchetto è memorizzato in un'area di memoria del sistema operativo che è allocata dinamicamente come una lista linkata di piccoli buffer (\texttt{mbuf} in BSD e \texttt{skbuf} in Linux) $\Rightarrow$ il costo di allocazione e di liberazione dei mini-buffer è troppo oneroso rispetto a un grande buffer allocato staticamente in cui memorizzare pacchetti di qualsiasi dimensione.

Le schede dedicate alla cattura non sono più viste dal sistema operativo, ma usano \textbf{driver accelerati} inglobati nella pila di cattura: la NIC copia il pacchetto nella sua zona di memoria kernel, e l'applicazione legge direttamente da quella zona di memoria senza l'intermediazione del sistema operativo.

%43
\paragraph{Implementazioni} Questa soluzione è adottata in netmap di Luigi Rizzo e in DNA di Luca Deri.

\paragraph{Problemi}
\begin{itemize}
\item \ul{applicazioni}: le altre pile protocollari (ad es. pila TCP/IP) sono scomparse $\Rightarrow$ la macchina è completamente dedicata alla cattura;
\item \ul{sistema operativo}: è richiesta una modifica intrusiva al sistema operativo;
\item \ul{NIC}: il driver accelerato è fortemente legato alla NIC $\Rightarrow$ non è possibile usare un altro modello di NIC;
\item \ul{prestazioni}: il collo di bottiglia rimane la banda del bus PCI;
\item \ul{timestamp}: non è preciso a causa dei ritardi dovuti al software.
\end{itemize}

\subsection{Commutazione di contesto}
%TODO Solo una parte dell'elaborazione (ad es. contatore), non tutta l'applicazione, viene spostata in kernel space.
L'elaborazione è spostata in kernel space, evitando la commutazione di contesto allo user space.

\paragraph{Implementazione} Questa soluzione è adottata nel Data Plane Development Kit (DPDK) di Intel, con lo scopo di realizzare apparati di rete programmabili via software su hardware Intel.

\paragraph{Problemi}
\begin{itemize}
\item \ul{packet batching}: il costo della commutazione di contesto è ridicolo grazie al packet batching;
\item \ul{debug}: è più facile in user space;
\item \ul{sicurezza}: l'intera applicazione lavora con memoria kernel;
\item \ul{programmazione}: è più difficile scrivere il codice nel kernel space.
\end{itemize}

%39
\subsection{NIC intelligenti}
%TODO Solo una parte dell'elaborazione (ad es. contatore), non tutta l'applicazione, viene spostata in kernel space.
L'elaborazione è svolta direttamente dalla NIC (ad es. Endace):
\begin{itemize}
\item \ul{elaborazione hardware}: evita il collo di bottiglia del bus PCI, limitando lo spostamento dei dati (anche se il miglioramento prestazionale è limitato);
\item \ul{precisione del timestamp}: non c'è il ritardo dovuto al software ed è basato sul GPS $\Rightarrow$ queste NIC sono adatte per catture su reti geograficamente estese.
\end{itemize}

%40
\subsection{Parallelizzazione in user space}
FFPF ha proposto un'architettura che cerca di sfruttare le caratteristiche dell'applicativo per andare più veloce, aumentando il \ul{parallelismo in user space}: l'applicazione di cattura è multi-thread ed è eseguita su CPU multi-core.

L'hardware può aiutare la parallelizzazione: la NIC può registrarsi al sistema operativo con più adapter, e ciascuno di essi è una coda logica distinta da cui escono i pacchetti a seconda della classificazione, effettuata da \textbf{filtri hardware}, in base ai loro campi (ad es. indirizzi MAC, indirizzi IP, porte TCP/UDP) $\Rightarrow$ più software possono leggere da code logiche distinte in parallelo.

\paragraph{Applicazioni}
\begin{itemize}
\item \ul{receive side scaling} (RSS): la classificazione è basata sull'identificativo di sessione (quintupla) $\Rightarrow$ tutti i pacchetti appartenenti alla stessa sessione vanno alla stessa coda $\Rightarrow$ è possibile bilanciare il carico sui server Web;\footnote{Si veda la sezione~\ref{sez:CDN_SLB}.}
\item \ul{virtualizzazione}: ogni macchina virtuale (VM) su un server ha un indirizzo MAC diverso $\Rightarrow$ i pacchetti entrano direttamente nella VM giusta senza essere toccati dal sistema operativo (hypervisor).\footnote{Si rimanda alla sezione~\ref{sez:NFV}.}
\end{itemize}
%\FloatBarrier