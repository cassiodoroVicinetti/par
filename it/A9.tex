\chapter{Border Gateway Protocol}
\label{cap:bgp}
Il \textbf{Border Gateway Protocol} (BGP) è il protocollo di instradamento inter-dominio comunemente utilizzato in Internet.

%7-8
\paragraph{Panoramica}
\begin{itemize}
 \item usa l'\ul{algoritmo Path Vector} (PV) per registrare la sequenza degli AS lungo il percorso senza il rischio di routing loop (sezione~\ref{sez:bgp_path_vector});
 \item i router possono \ul{aggregare} le informazioni di instradamento ricevute prima di propagarle (sezione~\ref{sez:bgp_aggregare});
 \item non scopre automaticamente l'esistenza di nuovi router vicini, ma le \ul{peering session} vanno configurate a mano (sezione~\ref{sez:bgp_peering});
 \item scambia gli aggiornamenti di instradamento usando \ul{connessioni TCP affidabili} (sezione~\ref{sez:bgp_affidabile});
 \item è un protocollo estensibile grazie al \ul{formato Type-Length-Value} (TLV) degli attributi (sezione~\ref{sez:bgp_tlv});
 \item supporta le \ul{politiche di instradamento} (sezione~\ref{sez:bgp_politiche}).
\end{itemize}

\section{Informazioni di instradamento}
Il BGP scambia informazioni di instradamento inter-dominio sulle rotte esterne, che sono nella forma indirizzo di rete/\ul{lunghezza del prefisso} (invece della netmask).

\subsection{Algoritmo Path Vector}
\label{sez:bgp_path_vector}
%12
Siccome le politiche di instradamento sono definite in base ai percorsi, il BGP non può basarsi sull'algoritmo DV perché non è sufficiente sapere i loro costi. Il BGP ha scelto di adottare l'algoritmo \textbf{Path Vector} (PV)\footnote{Si veda la sezione~\ref{sez:path_vector}.}: ogni AS costituisce un singolo nodo, identificato da un numero di 2 (o 4) byte, e l'informazione aggiuntiva è la \ul{lista degli AS attraversati}.

L'algoritmo PV è più stabile poiché è facile rilevare dei cicli:
\begin{itemize}
 \item se un router riceve un PV che contiene già il suo numero di AS, scarta il PV senza propagarlo, poiché si sta incorrendo in un routing loop;
 \item se no, il router inserisce il proprio numero di AS nel PV e quindi lo propaga ai suoi vicini.
\end{itemize}

%13
Il BGP non supporta una esplicita metrica di costo: il costo associato a ogni rotta è semplicemente pari al \ul{numero di AS attraversati} inclusi nella lista $\Rightarrow$ la rotta a minor costo può non essere quella effettivamente ottimale:
\begin{itemize}
 \item gli AS possono avere requisiti diversi, quindi possono adottare metriche diverse l'uno dall'altro (ad es. larghezza di banda, ritardo di trasmissione) $\Rightarrow$ è difficile calcolare costi coerenti per tutti gli AS;
 \item il costo annunciato può non corrispondere alla reale topologia della rete perché un ISP può voler nascondere a un concorrente le informazioni reali sulla propria rete per ragioni economiche.\footnote{Si veda la sezione~\ref{sez:interdominio_amministrativi}.}
\end{itemize}

\subsection{Aggregazione delle rotte}
\label{sez:bgp_aggregare}
Quando un router di frontiera propaga le informazioni sulle rotte ricevute, può essere configurato manualmente per includere delle \textbf{rotte aggregate} nei messaggi di annuncio per ridurne le dimensioni: due rotte possono essere aggregate in una rotta con la parte comune del prefisso di rete.

Tuttavia non tutte le rotte che sono state condensate in una rotta aggregata possono avere la stessa sequenza di AS attraversati, ma ci può essere una rotta più specifica che segue un altro percorso:
\begin{itemize}
 %33
 \item \textbf{rotta sovrapposta}: insieme alla rotta aggregata viene annunciata anche la rotta specifica, con la sua lista diversa di AS attraversati $\Rightarrow$ le informazioni sono complete, e l'algoritmo ``longest prefix matching'' selezionerà la rotta più specifica nella tabella di instradamento;
 %34
 \item \textbf{rotta not precise}: viene annunciata solamente la rotta aggregata $\Rightarrow$ le informazioni sono approssimative, perché la lista degli AS attraversati non corrisponde al percorso realmente seguito per tutti gli indirizzi di destinazione in quell'address range.
\end{itemize}

\section{Peering session}
\label{sez:bgp_peering}
%14
Due router di frontiera che si scambiano dei messaggi BGP vengono chiamati \textbf{peer}, e la sessione basata sul TCP è chiamata \textbf{peering session}.

Una differenza chiave in confronto agli altri protocolli di instradamento è il fatto che i peer non si sono in grado di scoprirsi a vicenda automaticamente: è necessaria la \ul{configurazione manuale} da parte dell'amministratore di rete, perché i peer potrebbero non essere connessi tramite un link diretto ma tra di essi potrebbero esistere altri router, per i quali gli aggiornamenti BGP sono dei normali pacchetti di dati da inoltrare a destinazione.

\subsection{TCP}
\label{sez:bgp_affidabile}
%10
La trasmissione delle informazioni di instradamento è \ul{affidabile} perché due peer stabiliscono una peering session instaurando una \textbf{connessione TCP} attraverso cui scambiare tutti i messaggi BGP:
\begin{itemize}
 \item vengono riutilizzati dei componenti esistenti anziché ridefinire ancora un altro meccanismo specifico del protocollo;
 \item il BGP non deve avere a che fare direttamente con ritrasmissioni, messaggi persi, ecc.
\end{itemize}

%9
L'utilizzo del TCP come protocollo di trasporto evita l'invio periodico degli aggiornamenti: un aggiornamento è inviato soltanto \ul{quando necessario}, includendo solo le rotte che sono cambiate, e l'invio viene ripetuto solo se il messaggio è andato perso $\Rightarrow$ viene ridotta la banda consumata per inviare le rotte.

%42
Siccome gli annunci non sono periodici, le rotte \ul{non scadono mai} $\Rightarrow$ è necessario informare esplicitamente che una rotta precedentemente annunciata è diventata non raggiungibile, per \textbf{ritirare} le rotte quando non sono più valide (analogamente al route poisoning dell'algoritmo DV\footnote{Si veda la sezione~\ref{sez:route_poisoning}.}).

%11
Tuttavia il TCP toglie all'applicazione il controllo sulle \ul{tempistiche}, perché i pacchetti di controllo possono essere ritardati dai meccanismi stessi del TCP: in caso di congestione il TCP riduce il bit rate di trasmissione impedendo la loro trasmissione puntuale $\Rightarrow$ è possibile configurare la qualità del servizio sui router interni all'AS in modo da dare priorità ai pacchetti BGP, considerando che sono pacchetti di servizio per permettere il funzionamento della rete.

%10
Il TCP non fornisce l'informazione se il peer remoto è ancora raggiungibile $\Rightarrow$ è richiesto un \textbf{meccanismo esplicito di keepalive} gestito dal BGP stesso. Anche i messaggi di keepalive si affidano ai meccanismi del TCP $\Rightarrow$ la reattività alla scomparsa di un peer o al guasto di un link è limitata, ma è ancora accettabile considerando che questi eventi sono rari (ad es. i link tra router di frontiera sono fortemente ridondati).

\subsection{I-BGP e E-BGP}
%15-53
Quando due router di frontiera instaurano tra di loro una peering session, ognuno comunica, tramite un messaggio di OPEN, il suo numero di AS all'altro party per determinare il tipo di sotto-protocollo:
\begin{itemize}
 \item \textbf{Exterior BGP} (E-BGP): i peer sono router di frontiera appartenenti a due AS differenti, solitamente connessi con un link diretto;
 \item \textbf{Interior BGP} (I-BGP): i peer sono router di frontiera appartenenti allo stesso AS, solitamente connessi attraverso una serie di router interni.
\end{itemize}

%19
L'elaborazione dei messaggi BGP e le rotte annunciate nelle peering session possono essere diverse a seconda di a quali AS appartengono i peer:
\begin{itemize}
 %22
 \item E-BGP: quando un router di frontiera propaga un PV a un peer E-BGP, antepone il numero dell'AS corrente a ogni lista di AS attraversati:
 \begin{itemize}
  \item \ul{rotte esterne}: sono propagate agli altri peer E-BGP, tranne i peer il cui AS è sul percorso migliore verso quelle destinazioni;
  \item \ul{rotte interne}: sono propagate agli altri peer E-BGP;
 \end{itemize}
 
 %23
 \item I-BGP: quando un router di frontiera propaga un PV a un peer I-BGP, trasmette la lista \ul{così com'è} perché il numero dell'AS rimane invariato:
 \begin{itemize}
  \item \ul{rotte esterne}: sono propagate agli altri peer I-BGP secondo varie modalità;
  %28
  \item \ul{rotte interne}: non sono mai propagate agli altri peer I-BGP, ma ogni router di frontiera le apprende da un processo di ridistribuzione indipendente.
 \end{itemize}
\end{itemize}

%18
Le sessioni I-BGP sono utilizzate per scambiare le rotte esterne:
\begin{itemize}
 \item indipendentemente dalle \ul{rotte} scambiate dal protocollo interior: la connessione diretta tra i peer evita di disturbare il protocollo IGP quando la variazione di una rotta esterna non richiede il ricalcolo delle rotte interne $\Rightarrow$ nessun transitorio, meno elaborazione;
 \item indipendentemente dal \ul{protocollo interior}: se i router di frontiera quando apprendono le rotte esterne dall'E-BGP si limitassero a ridistribuirle al protocollo IGP, lasciando che quest'ultimo le ridistribuisca naturalmente agli altri router di frontiera, verrebbero perse alcune informazioni importanti di cui il BGP ha bisogno $\Rightarrow$ servono dei messaggi BGP appositi, chiamati UPDATE, che includono queste informazioni nei loro attributi.
\end{itemize}

\subsubsection{Sincronizzazione IGP-BGP}
\begin{figure}[H]%WARNING
	%30
	\centering
	\includegraphics[scale=1.48]{../pic/A9/30}
	\caption{Esempio di sincronizzazione IGP-BGP.}
	\label{fig:sincronizzazione_igp_bgp}
\end{figure}

%29
\noindent
I router BGP in un AS di transito apprendono le destinazioni esterne da altri router BGP attraverso l'I-BGP, ma l'instradamento dei pacchetti attraverso l'AS (verso il router BGP egress) si affida ai router interni, le cui tabelle di instradamento sono riempite dal protocollo IGP e non dal BGP $\Rightarrow$ solo dopo che sono state annunciate anche dal protocollo IGP, le destinazioni esterne possono essere annunciate a router di frontiera di altri AS.

%31
Nell'esempio in figura~\ref{fig:sincronizzazione_igp_bgp}, il router R4 apprende la destinazione D tramite E-BGP e la annuncia al router R3 tramite I-BGP, ma R3 non può a sua volta annunciarla al router R5 tramite E-BGP finché la destinazione non è stata ridistribuita dal protocollo IGP a R3, altrimenti se R5 provasse a inviare un pacchetto verso D, R3 lo inoltrerebbe all'interno dell'AS dove i router interni lo scarterebbero.

%32
Potrebbe andare bene disabilitare la sincronizzazione quando:
\begin{itemize}
 \item l'AS non è di transito;
 \item tutti i router dell'AS utilizzano il BGP.
\end{itemize}
%\FloatBarrier

\subsection{Routing loop}
\begin{figure}
	\centering
	\includegraphics[scale=1.48]{../pic/A9/15}
	\caption{Esempio di routing loop.}
	\label{fig:bgp_routing_loop}
\end{figure}

\noindent
La mancanza delle informazioni sui router di frontiera attraversati quando essi appartengono allo stesso AS può essere causa di \ul{routing loop}: un router di frontiera non può più fare affidamento sulla lista degli AS attraversati per rilevare i percorsi che passano due volte per lo stesso router di frontiera.

Nell'esempio in figura~\ref{fig:bgp_routing_loop}, si determina un ciclo negli annunci:
\begin{enumerate}
 \item il router R4 apprende la rotta esterna verso la destinazione D;
 \item R4 propaga D al peer R3;
 \item R3 propaga D al peer R2;
 \item R2 propaga D al peer R4, che è il router che ha inizialmente appreso e annunciato D.
\end{enumerate}

Si crea così una situazione simile a quella che scatenava i count to infinity nell'algoritmo Distance Vector\footnote{Si veda la sezione~\ref{sez:count_to_infinity}.}: R4 non può stabilire se R2 sa raggiungere D passando attraverso R4 stesso oppure se esiste un percorso realmente alternativo $\Rightarrow$ se si verifica un guasto sul link tra R4 e il router di frontiera dell'AS in cui si trova D, R4 crede che D sia ancora raggiungibile attraverso R2.
\FloatBarrier

Le rotte esterne possono essere annunciate ai peer I-BGP secondo varie modalità: maglia completa, route reflector, AS confederation.

%16
\subsubsection{Maglia completa}
\begin{figure}
	\centering
	\includegraphics[scale=1.48]{../pic/A9/16a}
	\caption{Esempio di maglia completa.}
	\label{fig:maglia_completa_bgp}
\end{figure}

\noindent
Ogni router di frontiera ha una peering session I-BGP con ogni altro router di frontiera del suo AS.

Quando un router di frontiera apprende una rotta esterna dall'E-BGP, la propaga a tutti gli altri, che a loro volta la propagano a tutti, e così via.

In presenza di più di 2 router di frontiera, si possono creare dei routing loop dovuti a cicli negli annunci, come nella figura~\ref{fig:maglia_completa_bgp}.

Questa soluzione non è flessibile perché tutte le peering session vanno configurate a mano, anche se le peering session non cambiano molto nel tempo poiché i router di frontiera sono piuttosto fissi e resistenti ai guasti.
\FloatBarrier

\subsubsection{Route reflector}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[scale=1.48]{../pic/A9/16b}
	\caption{Esempio di route reflector.}
\end{figure}

\noindent
Uno dei router di frontiera viene eletto \textbf{route reflector} (RR), e tutti gli altri router di frontiera instaurano delle peering session solamente con esso senza creare dei percorsi chiusi.

Quando un router di frontiera apprende una rotta esterna dall'E-BGP, la propaga solo al RR, il quale è responsabile di propagare a sua volta la rotta agli altri router di frontiera evitando i routing loop.

Il route reflector costituisce un singolo punto di guasto.
%\FloatBarrier

%17
\subsubsection{AS confederation}
\begin{figure}
	\centering
	\includegraphics[scale=1.48]{../pic/A9/17}
	\caption{Esempio di AS confederation.}
\end{figure}

\noindent
I router di frontiera hanno una maglia completa di peering session I-BGP (come nella prima modalità), ma l'AS è suddiviso in mini-AS, ognuno con un numero di AS privato, e quando un router di frontiera propaga il PV antepone nella lista il suo numero di AS privato.

Quando arriva un annuncio, il router di frontiera può guardare se nella lista è già presente il proprio numero di AS privato, in modo da scartare il pacchetto se viene rilevato un routing loop.
\FloatBarrier

\section{Attributi di percorso}
\label{sez:bgp_tlv}
%35
Le informazioni BGP sulle rotte annunciate (ad es. la lista degli AS attraversati) sono contenute in \textbf{attributi di percorso} all'interno dei pacchetti di UPDATE.

Tutti gli attributi sono codificati nel \textbf{formato Type-Length-Value} (TLV) $\Rightarrow$ il BGP è un \textbf{protocollo estensibile}: gli RFC di estensione possono definire dei nuovi attributi senza rompere la compatibilità con il mondo esistente e, se il router non supporta quell'attributo (codice di tipo non riconosciuto), può ignorarlo e saltare al successivo (grazie all'informazione sulla lunghezza).

%36
Un attributo BGP può essere:
\begin{itemize}
 \item \textbf{well-known}: deve essere compreso da tutte le implementazioni, e non può mai essere saltato (sezione~\ref{sez:well-known_bgp}):
 \begin{itemize}
 \item \ul{mandatory}: deve essere presente in tutti i messaggi;
 \item \ul{discretionary}: può non essere presente in tutti i messaggi;
 \end{itemize}
 
 \item \textbf{optional}: può non essere compreso da tutte le implementazioni, e può essere saltato se non supportato (sezione~\ref{sez:optional_bgp}):
 \begin{itemize}
  \item \ul{transitive}: se il router non supporta l'attributo, deve propagarlo comunque impostando il flag P;
  \item \ul{non-transitive}: se il router non supporta l'attributo, non deve propagarlo.
 \end{itemize}
\end{itemize}

Ogni attributo ha il seguente formato TLV:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{1}} & \multicolumn{1}{r}{\footnotesize{2}} & \multicolumn{1}{r}{\footnotesize{3}} & \multicolumn{1}{r}{\footnotesize{4}} & \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24/32}} & \multicolumn{1}{r}{} \\
  \hline
  O & T & P & E & 0 & Type & Length & Value \\
  \hline
 \end{tabular}}
 \caption{Formato Type-Length-Value (TLV) di un attributo BGP.}
\end{table}
\noindent
dove i campi sono:
\begin{itemize}
 %37
 \item flag \ul{Optional} (O) (1 bit): specifica se l'attributo è optional o well-known;
 \item flag \ul{Transitive} (T) (1 bit): specifica se l'attributo è transitive o non-transitive;
 %38
 \item flag \ul{Partial} (P) (1 bit): specifica se almeno un router lungo il percorso ha incontrato un attributo optional transitive che non supportava;
 \item flag \ul{Extended Length} (E) (1 bit): specifica se il campo ``Length'' è codificato con uno o due byte;
 \item campo \ul{Type} (1 byte): contiene il codice di tipo che identifica l'attributo $\Rightarrow$ un router può determinare se supporta quell'attributo senza dover analizzare il suo valore;
 \item campo \ul{Length} (1 o 2 byte): contiene la lunghezza del valore dell'attributo $\Rightarrow$ un router può saltare un attributo non supportato e passare al successivo avanzando del numero di byte indicato da questo campo;
 \item campo \ul{Value} (lunghezza variabile): contiene il valore dell'attributo.
\end{itemize}

\subsection{Attributi well-known}
\label{sez:well-known_bgp}
\begin{itemize}
 %39
 %http://www.cisco.com/c/en/us/support/docs/ip/border-gateway-protocol-bgp/5816-bgpfaq-5816.html#difference
 \item attributo \textbf{ORIGIN} (tipo 1, mandatory): definisce l'origine dell'informazione di percorso:
 \begin{itemize}
  \item IGP: la rotta è stata specificata manualmente come una rotta statica (comando \texttt{bgp network});
  \item EGP: la rotta è stata appresa dal protocollo EGP\footnote{Qui s'intende il protocollo, non la classe di protocolli (si veda la sezione~\ref{sez:protocolli_egp}).};
  \item INCOMPLETE: la rotta è stata appresa da un protocollo IGP tramite un processo di ridistribuzione (comando \texttt{bgp redistribute});
 \end{itemize}
 
 %40
 \item attributo \textbf{AS\_PATH} (tipo 2, mandatory): contiene la lista degli AS attraversati suddivisa in segmenti di percorso:
 \begin{itemize}
  \item AS\_ SEQUENCE: i numeri di AS nel segmento di percorso sono in ordine di attraversamento, e se il primo segmento nel pacchetto è in ordine un nuovo numero di AS va aggiunto all'inizio di quel segmento;
  \item AS\_ SET: i numeri di AS nel segmento di percorso non sono in ordine di attraversamento, e se il primo segmento nel pacchetto non è in ordine va aggiunto prima di quel segmento un nuovo segmento in ordine in cui va inserito il nuovo numero di AS;
 \end{itemize}
 
 %41
 \item attributo \textbf{NEXT\_HOP} (tipo 3, mandatory): ottimizza l'instradamento quando più router appartengono alla stessa LAN ma a due AS diversi, e quindi il traffico da un AS all'altro passerebbe sempre dal router di frontiera $\Rightarrow$ il router di frontiera può annunciare di mandare il traffico direttamente al router next hop nell'altro AS:
 \begin{figure}[H]
	\centering
	\includegraphics[scale=0.97125]{../pic/A9/41}
	\caption{Il router di frontiera B insegna al router di frontiera A di usare il router C come next hop per la destinazione D.}
 \end{figure}
 
 %43
 \item attributo \textbf{LOCAL\_PREF} (tipo 5, discretionary): nell'I-BGP quando la destinazione esterna è raggiungibile tramite due router di frontiera egress, viene preferita la rotta con LOCAL\_PREF più alto;
 
 %44
 %http://netcerts.net/bgp-path-attributes-and-the-decision-process/
 \item attributo \textbf{ATOMIC\_AGGREGATE} (tipo 6, discretionary): indica che la rotta annunciata è una rotta aggregata not precise.
\end{itemize}

\subsection{Attributi optional}
\label{sez:optional_bgp}
\begin{itemize}
 %42
 \item attributo \textbf{MULTI\_EXIT\_DISC} (MED) (tipo 4, non-transitive): nell'E-BGP quando due AS sono connessi tramite più link, viene preferito il link con MED più basso e i link con MED più alti sono considerati link di backup;
 
 %44
 \item attributo \textbf{AGGREGATOR} (tipo 7, transitive): contiene il numero di AS e l'indirizzo IP del router che ha generato la rotta not precise;
 
 %37
 %https://tools.ietf.org/html/rfc1997
 \item attributo \textbf{COMMUNITIES} (tipo 8, transitive): indica a quale gruppo di peer questa rotta deve essere annunciata (ad es. all'intera Internet, solo nell'AS corrente, a nessuno);
 
 %42
 %https://tools.ietf.org/html/rfc4760#section-4
 \item attributo \textbf{MP\_UNREACH\_NLRI} (tipo 15, non-transitive): informa che una rotta precedentemente annunciata è diventata non raggiungibile (le rotte non scadono mai).
\end{itemize}

%45
\section{Processo di decisione}
\label{sez:bgp_politiche}
\begin{figure}[H]%WARNING
 %49
 %http://icawww1.epfl.ch/cn2/0910/slides/7.bgp
 \centering
 \includegraphics[scale=0.97125]{../pic/A9/49}
 \caption{Il processo di decisione seleziona le rotte dalle Adj-RIB-In in input e le scrive nella Loc-RIB e nelle Adj-RIB-Out in output.}
\end{figure}

\noindent
Il \textbf{processo di decisione} in esecuzione su ogni router di frontiera è responsabile di:
\begin{itemize}
 %51
 \item selezionare quali rotte sono annunciate agli altri peer BGP;
 \item selezionare quali rotte sono usate localmente dal router di frontiera;
 \item aggregare le rotte per ridurre le informazioni.
\end{itemize}
 
%46
Le basi di dati con cui il BGP ha a che fare sono:
\begin{itemize}
 \item \textbf{Routing Information Base} (RIB): consiste di tre parti distinte:
 \begin{itemize}
  \item \textbf{Adjacent RIB Incoming} (Adj-RIB-In): contiene tutte le rotte apprese dagli annunci ricevuti da un certo peer;
  \item \textbf{Local RIB} (Loc-RIB): contiene le rotte selezionate dal processo di decisione con il loro grado di preferenza;
  \item \textbf{Adjacent RIB Outgoing} (Adj-RIB-Out): contiene le rotte che verranno propagate negli annunci a un certo peer;
 \end{itemize}
 \item \textbf{Policy Information Base} (PIB): contiene le politiche di instradamento definite per configurazione manuale;
 \item \textbf{tabella di instradamento}: contiene le rotte utilizzate dal processo di inoltro dei pacchetti.
\end{itemize}

%50
Possono essere imposte delle \ul{politiche di instradamento} molto complesse per influenzare il processo di decisione:
\begin{enumerate}
 \item viene applicata a ogni rotta nelle Adj-RIB-In una certa funzione che, applicando le politiche definite sugli attributi, restituisce il \textbf{grado di preferenza} per quella rotta.\\
 Le politiche sono definite solo in base agli attributi della rotta corrente: il calcolo del grado di preferenza non è mai influenzato dall'esistenza, dalla non esistenza o dagli attributi di altre rotte;
 \item per ogni destinazione, la rotta con il grado di preferenza maggiore viene selezionata e viene inserita nella Loc-RIB;
 \item altre politiche determinano quali rotte vengono selezionate dalla Loc-RIB per essere inserite nelle Adj-RIB-Out.
\end{enumerate}
%\FloatBarrier