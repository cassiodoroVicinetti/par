\chapter{Instradamento inter-dominio}
\label{cap:interdominio}
%171
L'\textbf{instradamento inter-dominio} si occupa di decidere e propagare le informazioni sulle \textbf{rotte esterne} fra più AS interconnessi nella rete.

\section{Autonomous System}
\label{sez:AS}
%4[A7]-11[A7]
Un \textbf{Autonomous System} (AS) è un insieme di reti IP che sono sotto il controllo di un insieme di entità che concordano nel presentarsi come un'entità unica, adottando tutte lo stesso insieme di politiche di instradamento.

Dal punto di vista dell'instradamento inter-dominio, Internet è organizzata in AS: un AS rappresenta un'\ul{entità amministrativa omogenea}, generalmente un ISP, al livello gerarchico più alto sulla rete. %
%12[A7]
Ogni AS è univocamente identificato da un numero a 32 bit (era a 16 bit in passato) assegnato da IANA.

%5[A7]-14[A7]
Ogni AS è completamente \ul{indipendente}: può decidere l'instradamento interno secondo le proprie preferenze, e i pacchetti IP sono instradati al suo interno secondo le regole interne. Ogni AS può avere uno o più domini di instradamento interni serviti da protocolli IGP: ogni dominio può adottare il suo protocollo IGP preferito, e grazie alla ridistribuzione può scambiare le informazioni di instradamento con gli altri domini.

Una rete che è AS può tenere sotto controllo il traffico in entrata e in uscita grazie alle politiche di instradamento, ma è soggetta a una maggiore \ul{responsabilità}: l'instradamento è più difficile da configurare, ed eventuali errori di configurazione possono influenzare il traffico di altri AS.

%12[A7]
Per le porzioni di rete che intendono diventare degli AS, in passato erano applicate alcune regole aggiuntive che oggi sono state rilassate:
\begin{itemize}
 \item tutta la rete deve essere nello \ul{stesso dominio amministrativo}:
 \begin{itemize}
 \item[] oggi l'entità amministrativa di un AS non coincide necessariamente con l'organizzazione che effettivamente gestisce internamente la rete: ad esempio, la rete del Politecnico di Torino, pur essendo di proprietà dell'università ed essendo sotto il controllo di organi interni ad essa, è una delle sottoreti all'interno dell'AS amministrato dall'ente di ricerca GARR, a cui è affidato il compito di decidere le interconnessioni a lunga distanza verso gli altri AS;
 \end{itemize}
 
 \item la rete deve essere almeno di una \ul{dimensione} data:
 \begin{itemize}
 \item[] negli ultimi anni i content provider hanno avuto la necessità di avere degli AS sparsi per il mondo di dimensioni molto ridotte: ad esempio, Google possiede alcuni server Web in Italia che distribuiscono i contenuti personalizzati per il pubblico italiano (ad es. annunci pubblicitari) e che, essendo più vicini agli utenti, restituiscono più velocemente i risultati di ricerca agendo come una cache (si rimanda al capitolo~\ref{cap:CDN}) $\Rightarrow$ se quei server Web costituiscono da soli un AS, Google ha il controllo sulla distribuzione dei suoi contenuti agli ISP italiani, e può realizzare degli accordi commerciali con questi ultimi privilegiandone alcuni a scapito di altri;
 \end{itemize}
 
 \item l'AS deve essere connesso con almeno due altri AS per garantire, almeno tecnicamente, il \ul{transito} attraverso di esso del traffico da un AS all'altro:
 \begin{itemize}
 \item[] un ISP locale di piccole dimensioni (Tier 3) può acquistare da un ISP nazionale di grandi dimensioni (Tier 2) l'intera connettività verso Internet (si rimanda alla sezione~\ref{sez:accordi_as}).
 \end{itemize}
\end{itemize}

\section{Classe di protocolli EGP}
\label{sez:classe_egp}
%167
Un singolo router di frontiera posto tra AS appartenenti ad ISP diversi dà origine ad alcune problematiche:
\begin{itemize}
 \item a chi appartiene? chi lo configura?
 \item chi è responsabile in caso di guasto?
 \item come impedire a un ISP di raccogliere informazioni sulla rete di un concorrente?
\end{itemize}

%168-179
La soluzione è utilizzare due router di frontiera, ciascuno amministrato da uno dei due ISP, separati da una sorta di ``zona franca'' intermedia controllata da una terza istanza di protocollo di instradamento di tipo \textbf{Exterior Gateway Protocol} (EGP).

%16[A7]
Attraverso un protocollo EGP, ogni router di frontiera al bordo di un AS scambia informazioni di instradamento esterno con altri router di frontiera:
\begin{itemize}
 \item propaga agli altri AS le informazioni sulle destinazioni che si trovano all'interno del suo AS;
 \item propaga agli altri AS le informazioni sulle destinazioni che si trovano all'interno di altri AS ma che possono essere raggiunte attraverso il suo AS.
\end{itemize}

%177
I protocolli EGP si differenziano dai protocolli IGP soprattutto per il supporto alle \textbf{politiche di instradamento} che riflettono gli accordi commerciali tra gli AS (si rimanda alla sezione~\ref{sez:politiche_interdominio}).

\subsection{Protocolli EGP}
\label{sez:protocolli_egp}
\begin{itemize}
%4[A9]
\item \textbf{instradamento statico}: configurazione manuale dei router:
\begin{itemize}
\item[\pro] è l'``algoritmo'' migliore per implementare delle politiche complesse e per avere il completo controllo sui percorsi di rete;
\item[\pro] non è necessario traffico di controllo: si evita lo scambio di informazioni sulle destinazioni;
\item[\con] non reagisce ai cambiamenti topologici;
\item[\con] è facile introdurre inconsistenze;
\end{itemize}

\item \textbf{Exterior Gateway Protocol} (EGP)\footnote{Il protocollo EGP è uno dei protocolli appartenenti alla classe di protocolli EGP.}: fu il primo protocollo completamente dedicato all'instradamento tra domini, ma nessuno attualmente lo usa perché fornisce solo informazioni sulla raggiungibilità e non sulla distanza:
\begin{itemize}
\item se la raggiungibilità di una destinazione è annunciata attraverso più percorsi, non è possibile scegliere il percorso migliore a costo minore;
\item se la raggiungibilità di una destinazione è annunciata attraverso più percorsi, non è possibile garantire che tutti i router scelgano un percorso coerente $\Rightarrow$ può essere usato solo in reti senza percorsi chiusi dove non si possono creare dei cicli;
\end{itemize}

%16[A7]-5[A9]
\item \textbf{Border Gateway Protocol} (BGP): è l'unico protocollo EGP che è stato adottato nell'intera Internet a scapito di altri protocolli EGP: tutti i router di frontiera nell'intera rete di AS interconnessi devono adottare lo \ul{stesso protocollo EGP} per lo scambio delle rotte esterne, perché se due AS scegliessero di utilizzare protocolli EGP differenti, i loro router di frontiera non potrebbero comunicare tra loro (si rimanda al capitolo~\ref{cap:bgp});

%6[A9]
\item \textbf{Inter-Domain Routing Protocol} (IDRP): fu creato come un'evoluzione di BGP al fine di supportare l'indirizzamento OSI, ma nessuno attualmente lo usa perché:
\begin{itemize}
\item è composto da parti piuttosto complesse;
\item da allora i miglioramenti introdotti dall'IDRP sono stati portati nelle versioni successive di BGP;
\item non è compatibile con il BGP $\Rightarrow$ la sua adozione da parte di un AS romperebbe l'interoperabilità con il resto della rete che usa ancora il BGP.
\end{itemize}
\end{itemize}

%168-179
\section{Ridistribuzione}
Su ogni router di frontiera è in esecuzione un processo di ridistribuzione dal protocollo IGP all'interno dell'AS al protocollo EGP all'esterno dell'AS e viceversa $\Rightarrow$ le rotte vengono ridistribuite prima da un AS alla zona intermedia e poi da qui all'altro AS:
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{../pic/A1.5/168}
	\caption{Esempio di ridistribuzione tra AS appartenenti a ISP diversi.}
\end{figure}
%17[A7]
\begin{itemize}
 \item il protocollo IGP apprende le \textbf{rotte esterne} verso le destinazioni presenti in altri AS, e le propaga nell'AS come rotte interne;
 \item il protocollo EGP apprende le \textbf{rotte interne} verso le destinazioni presenti nell'AS, e le propaga agli altri AS come rotte esterne.
\end{itemize}
\FloatBarrier

%176-7[A7]-15[A7]
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/A1.6/176}
	\caption{Le rotte vengono ridistribuite sia tra domini gerarchici all'interno dell'AS, sia tra l'AS stesso e il mondo esterno.}
\end{figure}

%18[A7]
La ridistribuzione definisce:
\begin{itemize}
 \item quali reti interne devono essere note al mondo esterno: reti private ad esempio non devono essere propagate ad altri AS;
 \item quali reti esterne devono essere note all'interno dell'AS: è possibile ridurre la quantità di informazioni di instradamento annunciate evitando di includere i dettagli completi sulle reti esterne:
 \begin{itemize}
 %10[A7]
 \item gli indirizzi annunciati possono essere ``condensati'' in \ul{rotte aggregate} quando condividono una parte del loro prefisso di rete;
 \item può venire annunciata una singola \ul{rotta di default} quando l'AS ha un singolo punto di uscita.
 \end{itemize}
\end{itemize}

La ridistribuzione non deve introdurre delle incoerenze nell'instradamento:
\begin{itemize}
 \item si può creare un \ul{routing loop} se, ad esempio, una rotta appresa in IGP ed esportata in EGP viene poi re-importata in IGP apparendo come una rotta esterna;
 \item se un certo AS è raggiungibile attraverso più router di frontiera di uno stesso AS, questi router di frontiera devono accordarsi al fine di ridistribuire internamente un \ul{solo punto di uscita} per quella rotta.
\end{itemize}

Spesso la ridistribuzione su un router di frontiera al bordo di un AS è attivata in una sola direzione dal protocollo IGP al protocollo EGP: le rotte interne vengono esportate al mondo esterno, mentre le rotte esterne sono rimpiazzate da una rotta di default.
%\FloatBarrier