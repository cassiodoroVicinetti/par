\chapter{Instradamento gerarchico}
\label{cap:algoritmi_gerarchici}
%154
L'\textbf{instradamento gerarchico} permette di partizionare la rete in domini di instradamento autonomi. Un \textbf{dominio di instradamento} è la porzione della rete che è controllata dalla stessa istanza di un protocollo di instradamento.

\begin{figure}
\centering
	\begin{subfigure}[b]{.45\textwidth}
		\centering
		\includegraphics[width=0.85\linewidth]{../pic/A1.5/156a}
		\caption{Vista dal dominio A.}
	\end{subfigure}
	\
	\begin{subfigure}[b]{.45\textwidth}
		\centering
		\includegraphics[width=0.85\linewidth]{../pic/A1.5/156b}
		\caption{Vista dal dominio B.}
	\end{subfigure}
\caption{Esempio: inoltro di un pacchetto dal nodo A al nodo H.}
\end{figure}

%156
I router appartenenti a un dominio non conoscono la topologia esatta di un altro dominio, ma conoscono solo la \ul{lista delle destinazioni} contenute in esso con i relativi costi (a volte fittizi) $\Rightarrow$ una buona scelta per raggiungerle è prendere il percorso di uscita migliore verso il dominio di destinazione attraverso un router di frontiera.

Ogni \textbf{router di frontiera} ha visibilità su entrambi i domini che esso interconnette:
\begin{itemize}
 \item è chiamato \textbf{egress router} quando il pacchetto esce dal dominio;
 \item è chiamato \textbf{ingress router} quando il pacchetto entra nel dominio.
\end{itemize}

%155
L'instradamento gerarchico introduce una nuova regola per gestire le rotte relative alle destinazioni che si trovano al di fuori del dominio corrente:
\begin{itemize}
 \item \ul{destinazioni interne}: se la destinazione si trova all'interno dello stesso dominio di instradamento, devono essere utilizzate le informazioni di instradamento generate dal protocollo di instradamento ``interno'';
 \item \ul{destinazioni esterne}: se la destinazione si trova all'interno di un altro dominio di instradamento, il traffico deve essere inoltrato verso l'egress router più vicino in uscita dal dominio corrente al dominio di destinazione, e poi quest'ultimo sarà responsabile di consegnare il pacchetto a destinazione utilizzando le informazioni di instradamento interne.
\end{itemize}

%157
Il sotto-percorso dalla sorgente all'egress router più vicino e il sotto-percorso da qui alla destinazione finale presi singolarmente sono ottimali, ma il percorso complessivo che è la loro concatenazione è \ul{non ottimale}: data una destinazione, la prima parte del percorso (sorgente-router di frontiera) è uguale per tutte le destinazioni nel dominio remoto.
\FloatBarrier

%153
\paragraph{Motivazioni}
\begin{itemize}
 \item \ul{interoperabilità}: possono essere interconnessi domini controllati da protocolli di instradamento differenti;
 \item \ul{visibilità}: un ISP non vuole far conoscere i dettagli sulla sua rete a un concorrente;
 %158
 \item \ul{scalabilità}: una porzione di rete troppo ampia non può essere controllata da una singola istanza di un protocollo di instradamento, ma ha bisogno di essere partizionata:
 \begin{itemize}
  %159
  \item \ul{memoria}: esclude le informazioni sulla topologia precisa dei domini remoti, riducendo la quantità di informazioni che ogni router deve mantenere in memoria;
  \item \ul{sommarizzazione}: consente di annunciare una destinazione ``virtuale'' (con un costo convenzionale) che raggruppa insieme diverse destinazioni ``reali'' (ad es. nelle reti IP più indirizzi di rete si possono aggregare in un indirizzo di rete con una netmask più lunga);
  %160
  \item \ul{isolamento}: se all'interno di un certo dominio avviene un guasto o viene aggiunto un nuovo link, i cambi di rotta non perturbano gli altri domini, cioè le tabelle di instradamento nei router dei domini remoti rimangono invariate $\Rightarrow$ meno transitori, rete più stabile, convergenza più rapida.
 \end{itemize}
\end{itemize}

%162
\paragraph{Implementazioni} L'instradamento gerarchico può essere implementato in due modi (non mutualmente esclusivi):
\begin{itemize}
 \item \ul{automatico}: alcuni protocolli (come OSPF, IS-IS) partizionano automaticamente la rete in domini di instradamento (detti ``aree'' in OSPF, si rimanda al capitolo~\ref{cap:OSPF});
 \item \ul{manuale}: è possibile attivare il processo di ridistribuzione su un router di frontiera per interconnettere domini controllati da protocolli di instradamento anche differenti (sezione~\ref{sez:ridistribuzione}).
\end{itemize}

%161
\section{Domini partizionati}
\label{sez:aree_partizionate}
Un dominio diventa \textbf{partizionato} se a partire da un router di frontiera non è più possibile raggiungere tutte le sue destinazioni interne attraverso percorsi che rimangano sempre all'interno del dominio stesso.

\begin{figure}
	\centering
	\includegraphics[width=0.38\linewidth]{../pic/A1.5/161}
	\caption{Esempio di domini partizionati.}
	\label{fig:A1_5_partitioned}
\end{figure}

Nell'esempio in figura~\ref{fig:A1_5_partitioned}, il pacchetto inviato dal nodo A esce appena possibile dal dominio di instradamento A, ma una volta entrato nel dominio B non può raggiungere la destinazione finale H a causa del guasto del link tra il router di frontiera e il nodo I. Esiste in realtà un percorso alternativo che conduce alla destinazione H attraverso l'altro router di frontiera, ma non può essere preso perché richiederebbe al pacchetto di uscire dal dominio B ed attraversare il dominio A.

Inoltre, i percorsi possono essere \ul{asimmetrici}: il pacchetto di risposta potrebbe prendere un percorso diverso da quello preso dal pacchetto di query, passando per un router di frontiera differente $\Rightarrow$ i dati possono venire ricevuti, ma gli ACK di conferma della loro ricezione possono andare persi.

\paragraph{Soluzioni}
\begin{itemize}
\item è possibile ridondare i link all'interno di ogni dominio per renderlo \ul{fortemente connesso}, per evitare che il guasto di un link possa causare il partizionamento del dominio;
\item il protocollo OSPF permette la configurazione manuale di una sorta di tunnel virtuale tra due router di frontiera chiamato \textbf{Virtual Link} (si rimanda alla sezione~\ref{sez:OSPF_Virtual_Link}).
\end{itemize}
\FloatBarrier

\section{Ridistribuzione}
\label{sez:ridistribuzione}
%164
La \textbf{ridistribuzione} è il processo software, in esecuzione su un router di frontiera, che permette di trasferire le informazioni di instradamento da un dominio di instradamento a un altro.

%163
\begin{figure}
	\centering
	\includegraphics[width=0.38\linewidth]{../pic/A1.5/163}
	\caption{Esempio di ridistribuzione.}
	\label{fig:A1_5_ridistribuzione}
\end{figure}

Nell'esempio in figura~\ref{fig:A1_5_ridistribuzione}, le destinazioni apprese in un dominio RIP possono essere iniettate in un dominio OSPF e viceversa.

\paragraph{Osservazioni}
\begin{itemize}
\item Il comando di ridistribuzione è unidirezionale $\Rightarrow$ è possibile fare una ridistribuzione selettiva in una sola direzione (ad esempio l'ISP non accetta rotte non fidate annunciate dal cliente).
\item La ridistribuzione può essere effettuata anche tra domini controllati da istanze dello stesso protocollo.
\item Le rotte apprese con il processo di ridistribuzione possono essere contrassegnate come ``rotte esterne'' dal protocollo di instradamento.
\end{itemize}
\FloatBarrier

\subsection{Costi}
%165
I router in un dominio conosceranno un insieme più ampio di destinazioni, anche se alcune di esse possono avere una topologia ``errata'' (semplificata): infatti il processo di ridistribuzione può
\begin{itemize}
 \item mantenere il costo della rotta originale, al più aggiustata con un coefficiente, oppure
 \item impostare il costo a un valore convenzionale quando:
\begin{itemize}
 \item i due protocolli utilizzano metriche differenti: ad esempio non è possibile convertire un costo appreso in ``numero di hop'' in un altro che utilizza i ``ritardi'';
 \item più destinazioni con costi diversi vengono aggregate in una rotta sommarizzata.
\end{itemize}
\end{itemize}

%169
\begin{table}
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  \multicolumn{2}{|c|}{\textbf{Sorgente della rotta}} & \textbf{Distanza amministrativa} \\
  \hline
  \multicolumn{2}{|c|}{interfaccia connessa} & 0 \\
  \hline
  \multicolumn{2}{|c|}{rotta statica} & 1 \\
  \hline
  \multirow{5}{*}{rotta dinamica} & BGP esterno & 20 \\
  \cline{2-3}
  & EIGRP interno & 90 \\
  \cline{2-3}
  & IGRP & 100 \\
  \cline{2-3}
  & OSPF & 110 \\
  \cline{2-3}
  & RIP & 120 \\
  \hline
 \end{tabular}}
 \caption{Principali distanze amministrative predefinite nei router Cisco.}
\end{table}

%166
Quando una destinazione è annunciata come raggiungibile da entrambi i domini, controllati da protocolli di instradamento con metriche diverse, il router di frontiera come può confrontare i costi per determinare la rotta migliore verso quella destinazione? Ogni protocollo di instradamento ha un \textbf{costo intrinseco} pre-assegnato dal costruttore dell'apparato $\Rightarrow$ il router sceglie sempre il protocollo con costo intrinseco più basso (anche se la rotta selezionata potrebbe non essere la migliore).
%\FloatBarrier