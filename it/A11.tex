\chapter{Instradamento IPv6}
%3
Oggi i router sono per lo più pronti per IPv6, anche se le prestazioni in IPv6 sono ancora peggiori rispetto a quelle in IPv4 a causa della mancanza di esperienza e della più bassa domanda di traffico. Spesso l'instradamento IPv6 è disabilitato per impostazione predefinita anche se l'apparato supporta IPv6 (sui router Cisco si attiva con il comando \texttt{ipv6 unicast-routing}).

%2
Due aspetti sono da considerare:
\begin{itemize}
\item \ul{tabelle di instradamento}: come gestire l'inoltro dei pacchetti di dati? (sez.~\ref{sez:IPv6_routing_tables})
\item \ul{protocolli di instradamento}: come distribuire le rotte nella rete? (sez.~\ref{sez:IPv6_routing_protocols})
\end{itemize}

%Tipicamente i router che supportano IPv6 adottano l'approccio dual stack di tipo ``navi nella notte'': IPv4 e IPv6 sono supportati da due pile indipendenti per il livello trasporto $ \Rightarrow $ questo richiede la completa duplicazione di tutti i componenti: protocolli di instradamento, tabelle di instradamento, liste di accesso, ecc.

\section{Tabelle di instradamento}
\label{sez:IPv6_routing_tables}
%4
L'instradamento in IPv6 è effettuato nello stesso modo di IPv4 ma richiede due tabelle di instradamento distinte, una per le rotte IPv4 e l'altra per le rotte IPv6. Le tabelle di instradamento IPv6 possono memorizzare diversi tipi di entry, tra cui:
%10
\begin{itemize}
 \item \ul{entry indirette} (codici O/S): specificano gli indirizzi, tipicamente link local, delle interfacce dei router next hop ai quali inviare i pacchetti indirizzati verso link remoti;
 \item \ul{entry dirette}: specificano le interfacce del router stesso attraverso le quali inviare i pacchetti indirizzati verso i link locali:
 %11
 \begin{itemize}
  \item \ul{reti connesse} (codice C): specificano i prefissi dei link locali;
  \item \ul{indirizzi di interfaccia} (codice L): specificano gli interface identifier nei link locali.
 \end{itemize}
\end{itemize}

\subsection{Next hop}
%12
Come next hop nelle \textbf{rotte dinamiche} calcolate, i protocolli di instradamento utilizzano sempre gli \ul{indirizzi link local}, anche se è configurato un indirizzo global sull'interfaccia vicina, per motivi di semplicità: gli indirizzi link local esistono sempre, mentre gli indirizzi global potrebbero non essere utilizzati in alcune porzioni della rete.

%13-14-20
Tuttavia l'uso degli indirizzi link local rende difficile il compito di determinare la \ul{posizione} di quell'indirizzo: l'indirizzo di rete di un indirizzo global permette almeno di identificare la rete in cui l'host dovrebbe essere presente e determinare così l'interfaccia di uscita, ma un indirizzo link local che inizia con FE80:: può essere ovunque $\Rightarrow$ accanto all'indirizzo del next hop, i router stampano anche l'\textbf{interfaccia locale} di uscita per risolvere le ambiguità, come:\\
\centerline{\texttt{2001:1::/64 via FE80::1, \textbf{FastEthernet0/0}}}

%21-22
Per le \textbf{rotte statiche}, la scelta è lasciata al gestore della rete, che può utilizzare l'indirizzo che preferisce come next hop:\\
\centerline{\texttt{ipv6 route \textsl{indirizzo}/\textsl{netmask} [\textsl{interfaccia locale}] [\textsl{next hop}] [\textsl{distanza}]}}
\begin{itemize}
\item \ul{interfaccia broadcast} (ad es. Ethernet): è necessario specificare l'indirizzo del next hop:
\begin{itemize}
\item \ul{indirizzo global}: non è necessario specificare l'interfaccia locale perché può essere determinata dal prefisso di rete:\\
\centerline{\texttt{ipv6 route 2001:1::/64 2001::1}}
\item \ul{indirizzo link local}: è necessario specificare anche l'interfaccia locale per identificare l'ambito dell'indirizzo link local:\\
\centerline{\texttt{ipv6 route 2001:1::/64 \textbf{FastEthernet0/0} fe80::1}}
\end{itemize}

\item \ul{interfaccia punto-punto} (ad es. seriale): non è necessario specificare l'indirizzo del next hop perché è identificato univocamente dall'interfaccia locale:\\
\centerline{\texttt{ipv6 route 2001:1::/64 serial 0}}
\end{itemize}

%15
Siccome le rotte statiche non possono adattarsi ai cambiamenti della rete, è fortemente suggerito di utilizzare gli \ul{indirizzi global} come next hop per le rotte statiche. Ciò evita che una rotta diventi non valida se il next hop cambia: ad esempio, se la scheda di rete sul router next hop viene sostituita a causa di un guasto hardware:
\begin{itemize}
 \item indirizzo link local: dipende dall'indirizzo MAC della scheda $\Rightarrow$ occorre modificare la rotta;
 \item indirizzo global: è sufficiente assegnare alla nuova interfaccia lo stesso indirizzo global.
\end{itemize}

%5-8
\section{Protocolli di instradamento}
\label{sez:IPv6_routing_protocols}
I protocolli di instradamento che supportano IPv6 possono adottare due approcci:
\begin{itemize}
%6
  \item \textbf{instradamento integrato} (IS-IS, MP-BGP4): il protocollo consente di scambiare informazioni di instradamento sia IPv4 sia IPv6 allo stesso tempo:
  \begin{itemize}
  \item[\pro] \ul{efficienza}: gli indirizzi IPv4 e IPv6 appartenenti alla stessa destinazione possono essere trasportati tramite un singolo messaggio;
  \item[\con] \ul{flessibilità}: un singolo protocollo trasporta più famiglie di indirizzi;
  \item[\pro] \ul{reattività}: se avviene un guasto o un cambiamento nella rete, il protocollo lo scopre per entrambe le famiglie di indirizzi;
  \item[\con] \ul{bug}: un problema nel protocollo influenza reti IPv4 e IPv6 allo stesso modo;
  \item[\con] \ul{migrazione}: se il protocollo usa IPv4 per trasportare i pacchetti di Hello, non è possibile abolire IPv4 nella rete;
  \end{itemize}
  
%7
  \item \textbf{navi nella notte} (RIPng, EIGRP, OSPFv3): il protocollo consente di scambiare informazioni di instradamento solo IPv6:
  \begin{itemize}
  \item[\con] \ul{efficienza}: data una destinazione, deve essere scambiato un messaggio per il suo indirizzo IPv4 e un altro messaggio per il suo indirizzo IPv6, e i messaggi sono completamente indipendenti tra loro;
  \item[\pro] \ul{flessibilità}: si possono usare due protocolli diversi, uno per le informazioni di instradamento IPv4 e un altro per le informazioni di instradamento IPv6;
  \item[\con] \ul{reattività}: se avviene un guasto o un cambiamento nella rete, entrambi i protocolli devono scoprirlo, ciascuno con le proprie tempistiche e con messaggi duplicati;
  \item[\pro] \ul{bug}: un problema nel protocollo non influenza l'instradamento nell'altro;
  \item[\pro] \ul{migrazione}: ogni protocollo di instradamento genera messaggi della famiglia di indirizzi a cui appartiene.
  \end{itemize}
\end{itemize}

\subsection{RIPng}
%27
\textbf{RIPng} adotta l'approccio ``navi nella notte'', e apporta a RIP dei miglioramenti principalmente nell'interfaccia a riga di comando di Cisco:
\begin{itemize}
\item \ul{supporto alle istanze multiple}: il campo \texttt{tag} permette di specificare l'istanza di protocollo;\footnote{Il supporto per le istanze multiple era già presente in RIPv2, ma non era configurabile sui router Cisco.}
%28
\item \ul{configurazione per interfaccia}: sono stati introdotti nuovi comandi:
\begin{itemize}
\item \texttt{ipv6 rip \textsl{\textless tag\textgreater} enable}: sostituisce il comando \texttt{network} e configura automaticamente RIP su quell'interfaccia senza dover specificare un indirizzo;
\item \texttt{ipv6 rip \textsl{\textless tag\textgreater} default-information originate}: origina la rotta di default (::/0), cioè il resto del mondo è raggiungibile tramite questa interfaccia.
\end{itemize}
\end{itemize}

\subsection{OSPFv3}
\label{sez:OSPFv3}
\textbf{OSPFv3} adotta l'approccio ``navi nella notte'', e differisce da OSPF principalmente per tre aspetti:
\begin{itemize}
%49-50
\item \ul{configurazione per interfaccia}: è stato introdotto il comando \texttt{ipv6 ospf \textsl{\textless ID processo\textgreater} area \textsl{\textless ID area\textgreater}} che specifica che tutte le reti e gli indirizzi configurati su questa interfaccia verranno annunciati come appartenenti all'area specificata;
%54-55
\item \ul{Router ID}: sfortunatamente OSPFv3 usa ancora un Router ID da 32 bit, che non è neanche capace di impostare automaticamente quando nessun indirizzo IPv4 è disponibile $\Rightarrow$ il comando \texttt{ipv6 router-id \textsl{\textless numero\textgreater}} diventa obbligatorio quando il router è solo IPv6 o è in una rete solo IPv6;
%68
\item \ul{tunnel}: è possibile configurare un tunnel IPv6 su IPv4 per collegare tra loro isole IPv6 attraverso una rete IPv4.
\end{itemize}

\subsection{IS-IS}
\textbf{IS-IS} per IPv6 adotta l'approccio ``instradamento integrato'': usa infatti un proprio protocollo di livello 3 per trasportare i pacchetti specifici del protocollo, indipendentemente dalla versione del protocollo IP sottostante.

\subsection{MP-BGP4}
\textbf{MP-BGP4} può adottare entrambi gli approcci a seconda della configurazione: i pacchetti TCP possono essere imbustati in IPv4 o in IPv6 a seconda delle necessità.

La distribuzione più comune segue l'approccio ``instradamento integrato'', per la necessità di utilizzare il numero dell'AS (che è uguale sia per IPv4 sia per IPv6) per il processo BGP. L'integrazione si riflette anche nelle politiche: è possibile mischiare indirizzi IPv4 e indirizzi IPv6 a piacimento.