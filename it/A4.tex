\chapter{IGRP e EIGRP}
%3-4
L'\textbf{Interior Gateway Routing Protocol} (IGRP) è un protocollo di instradamento intra-dominio, proprietario di Cisco, basato sull'algoritmo Distance Vector (DV).

Anche nell'IGRP è assente il supporto all'\textbf{indirizzamento classless} (netmask), ma rispetto al RIP ha delle funzionalità aggiuntive ``orientate al marketing'' che tuttavia nascondono degli errori tecnici non previsti:
\begin{itemize}
 \item \ul{metriche più sofisticate}: introducono maggiore complessità e minore stabilità delle rotte;
 \item \ul{multipath routing}: il multipath routing a costi differenziati può dare origine a cicli;
 \item supporto per \ul{reti eterogenee}: un intervallo ampio per i costi dei link può rallentare la convergenza ad infinito;
 %15
 \item \ul{meno traffico} legato al protocollo di instradamento: l'aggiornamento dei DV avviene ogni 90 secondi;
 %14
 \item \ul{maggiore stabilità}: i triggered update sono inviati solo se il costo è cambiato più del 10\% per evitare la frequente riconfigurazione della rete;
 \item \ul{non più di una frammentazione IP}: i messaggi IGRP trasportano anche informazioni sulle MTU supportate dai router lungo il percorso $\Rightarrow$ il pacchetto può essere frammentato subito in base alla minima MTU, evitando che in un secondo tempo venga ri-frammentato con una MTU più piccola.
\end{itemize}

\section{Metriche}
%7
Il costo $C$ è ottenuto dalla combinazione di 4 metriche:
\[
 \begin{cases} \displaystyle C = \frac{10^7}{B} \left( k_1 + \frac{k_2}{256-L} \right) + k_3 D & \text{se } k_5 = 0 \\ \displaystyle C = \left[ \frac{10^7}{B} \left( k_1 + \frac{k_2}{256-L} \right) + k_3 D \right] \frac{k_5}{R+k_4} & \text{se } k_5 \neq 0 \end{cases}
\]
%5-6
\begin{itemize}
 \item[$B$ -] \textbf{larghezza di banda}: è direttamente proporzionale alla larghezza di banda del link (valori da 1 a 2\textsuperscript{24} con 1 = 1,2 kbit/s);
 \item[$D$ -] \textbf{ritardo}: è inversamente proporzionale alla larghezza di banda del link, e considera solo il ritardo di trasmissione ignorando altri componenti come il ritardo di propagazione e il ritardo di accodamento (valori da 1 a 2\textsuperscript{24} con 1 = 10 ms);
 \item[$R$ -] \textbf{affidabilità}: può essere piuttosto variabile nel tempo (valori da 1 a 255 con 255 = 100\%);
 \item[$L$ -] \textbf{carico}: dipende dal traffico istantaneo (valori da 1 a 255 con 255 = 100\%).
\end{itemize}

Con i valori predefiniti dei coefficienti $k_{1 \ldots 5}$, il costo tiene conto solo del ritardo $D$ e della larghezza di banda $B$:
\[
 k_1 = k_3 = 1 , \, k_2 = k_4 = k_5 = 0 \Rightarrow C = \frac{10^7}{B} + D
\]

I comandi IGRP richiedono la specificazione di una \ul{classe di servizio} (TOS), ma in pratica l'instradamento basato sulle classi di servizio non è stato mai implementato in questo protocollo, perché richiederebbe una tabella di instradamento e una funzione di costo differenti per ogni classe di servizio.

\paragraph{Problemi}
Una metrica così sofisticata soffre in realtà di alcuni problemi dal punto di vista tecnico:\footnote{Si veda la sezione~\ref{sez:metrica}.}
\begin{itemize}
 %8
 \item è difficile capire le \ul{scelte di instradamento}: gli esseri umani guardano la topologia della rete e misurano la distanza in ``numero di hop'' $\Rightarrow$ non è facile determinare qual è il percorso migliore quando viene adottata una metrica più sofisticata;
 \item è difficile capire come regolare i \ul{coefficienti} $k_{1 \ldots 5}$: che cosa succede alla rete quando i parametri vengono modificati? quali valori bisogna dare a essi al fine di ottenere il comportamento voluto?
 %9
 \item alcune \ul{metriche} (ad es. carico), non essendo molto stabili, forzano la rete ad adattare continuamente i percorsi perché questi ultimi cambiano spesso di costo $\Rightarrow$ la necessità di aggiornare frequentemente le rotte porta a più transitori con conseguenti buchi neri e bouncing effect, più traffico di instradamento e più risorse della CPU dedicate ai protocolli di instradamento;
 %10-11
 \item è difficile definire il giusto \ul{valore soglia per l'infinito}: IGRP lo definisce a 2\textsuperscript{24}, ma è necessario troppo tempo per aumentare i costi fino al valore soglia quando sono coinvolti link a basso costo.\footnote{Si veda la sezione~\ref{sez:soglia_infinito}.}
\end{itemize}

\section{Multipath routing}
%12
L'IGRP supporta il \textbf{multipath routing a costi differenziati}: sono ammesse più rotte per la stessa destinazione, anche se quelle rotte hanno costi differenti ($c_\text{sec} \leq V \cdot c_\text{prim}$), e il carico è distribuito proporzionalmente al costo della rotta.\footnote{Si veda la sezione~\ref{sez:unequal_multipath_routing}.}

%13
\paragraph{Problema} Il traffico può entrare in un ciclo quando percorsi differenti vengono scelti da due router: uno può scegliere il percorso primario (rotta ottimale) e l'altro il percorso secondario (rotta sub-ottimale) $\Rightarrow$ nell'ultima versione di IGRP è consentito solo il multipath routing a costi equivalenti (il coefficiente $V$ è impostato a 1) al fine di impedire queste problematiche.

%16-17-18-19-20
\section{EIGRP}
L'\textbf{Enhanced IGRP} introduce diversi miglioramenti all'IGRP, soprattutto dal punto di vista della scalabilità:
\begin{itemize}
 \item supporta l'\textbf{indirizzamento classless}: le reti sono finalmente annunciate con le coppie indirizzo-netmask corrette;
 \item implementa il \textbf{Diffusing Update Algorithm} (DUAL): la rete è libera da cicli, anche durante i transitori, e la convergenza è più rapida (nessun fenomeno di count to infinity);\footnote{Si veda la sezione~\ref{sez:dual}.}
 \item disaccoppia la funzione di \textbf{neighbor discovery} dal meccanismo di aggiornamento delle rotte: i router si scambiano periodicamente dei piccoli messaggi di Hello, mentre i DV vengono generati solo quando è cambiato qualcosa nella rete:
 \begin{itemize}
  \item i \ul{messaggi di Hello} possono essere mandati ad alta frequenza, rendendo più veloci il rilevamento dei guasti e quindi la convergenza, perché:
  \begin{itemize}
   \item consumano meno banda $\Rightarrow$ il traffico di instradamento è ridotto;
   \item consumano meno risorse della CPU rispetto all'elaborazione e al calcolo dei DV;
  \end{itemize}
  \item i DV devono essere inviati tramite un \ul{protocollo affidabile}: ogni DV deve essere confermato da un messaggio di acknowledgment, e deve essere ritrasmesso se è andato perso.
 \end{itemize}
\end{itemize}