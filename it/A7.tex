\chapter{Instradamento inter-dominio: peering e transito in Internet}
%20
Il traffico all'interno dell'AS è quasi ``gratis'', escludendo i costi dell'infrastruttura (manutenzione, amministrazione, elettricità, ecc.) $\Rightarrow$ gli ISP provano a convincere gli utenti a trascorrere la maggior parte del loro tempo all'interno dell'AS.

Tuttavia, un AS deve connettersi ad altri AS per due motivi:
\begin{itemize}
 \item un AS deve essere in grado di raggiungere tutte le destinazioni presenti su Internet per la legge di Metcalfe (= la rete dev'essere la più estesa possibile per essere utile);
 \item un AS vorrebbe raggiungere la resilienza nelle sue connessioni verso il mondo esterno.
\end{itemize}

%13-31
Gli AS su Internet sono interconnessi con un'\ul{organizzazione gerarchica}:
\begin{itemize}
 \item \textbf{Tier 1} (ad es. Seabone, Sprint): operatore internazionale che interconnette le grandi città con link a larga banda e a lunga distanza e trasporta grandi flussi di traffico lungo le dorsali;
 \item \textbf{Tier 2} (ad es. Telecom Italia): operatore nazionale che raccoglie il traffico dai singoli utenti attraverso molti punti di accesso grazie alla sua presenza capillare sul territorio;
 \item \textbf{Tier 3}: operatore locale che serve un'area geografica molto ristretta.
\end{itemize}

\section{Accordi commerciali tra AS}
\label{sez:accordi_as}
%21
Le interconnessioni tra un operatore e un altro possono non essere gratuite: di solito, l'interconnessione tra due AS è stabilita solo al momento di un \textbf{accordo economico}. Due tipi di accordi sono possibili:
\begin{itemize}
 \item \ul{transito}: rappresenta la scelta più naturale dal punto di vista economico (sezione~\ref{sez:transito});
 \item \ul{peering}: quando due AS scoprono che possono fare di meglio (sezione~\ref{sez:peering}).
\end{itemize}

%28-59
L'instradamento inter-dominio su Internet è principalmente guidato dagli accordi commerciali tra gli operatori ai vari livelli gerarchici:
\begin{itemize}
 %32-33
 \item Tier 1: può annunciare, indipendentemente dalla copertura geografica della sua rete, la raggiungibilità della \ul{full route} (0.0.0.0/0), cioè la raggiungibilità di (quasi) ogni AS di destinazione su Internet, senza dover acquistare il transito da altri provider né pagare qualche tassa di accesso;
 %34
 \item Tier 2: deve acquistare il transito da un operatore Tier 1 al fine di poter raggiungere l'intera Internet, e può stabilire molti accordi di \ul{peering} con altri provider Tier 2;
 \item Tier 3: non ha alcun accordo di peering, e semplicemente acquista il \ul{transito} da un provider Tier 2 (o Tier 1).
\end{itemize}

%22
\subsection{Transito}
\label{sez:transito}
Un accordo è di \textbf{transito} quando un ISP deve pagare un altro ISP per connettersi al suo AS. L'ISP che riceve il denaro garantisce il ``transito'', cioè il diritto di utilizzare la sua rete, al traffico proveniente dall'altro AS.

L'accordo economico può stabilire:
\begin{itemize}
\item la modalità di pagamento:
\begin{itemize}
 \item \ul{tariffa a volume}: una quantità massima di byte di dati al giorno o al mese, più un costo aggiuntivo per il traffico eccedente quella quantità;
 \item \ul{tariffa flat}: una tassa mensile per una banda massima (la larghezza di banda può essere limitata via software sull'interfaccia di accesso).
\end{itemize}
\item quali destinazioni sono raggiungibili attraverso il transito:
\begin{itemize}
 \item \ul{full route}: tutte le destinazioni nel mondo devono essere raggiungibili;
 \item solo le destinazioni in una certa \ul{area geografica} (ad es. USA): i pacchetti diretti verso altre destinazioni vengono scartati.
\end{itemize}
\end{itemize}

Il prezzo può essere influenzato dall'importanza dell'ISP che vende il transito:
\begin{itemize}
 \item un ISP statunitense ha il controllo della parte più importante della rete perché all'interno del suo AS sono presenti i server Web più visitati al mondo;
 \item un ISP molto grande può offrire una buona raggiungibilità con il resto del mondo grazie all'elevato numero di interconnessioni.
\end{itemize}

%23
\subsection{Peering}
\label{sez:peering}
Un accordo è di \textbf{peering} quando due ISP paritetici si mettono d'accordo per scambiare il traffico tra essi stessi senza dover pagare l'un l'altro.

Due ISP possono decidere di stipulare un accordo di peering se determinano che i costi per l'interconnessione diretta sono più bassi dei costi per l'acquisto del transito l'uno dall'altro: i costi di installazione e di manutenzione del link diretto tra gli AS sono equamente suddivisi tra i due ISP, che possono inviare dati alla massima velocità consentita dal link.

Gli operatori Tier 1 operano in un mercato molto competitivo:
\begin{itemize}
 %35
 \item gli operatori Tier 2 possono stabilire dei nuovi accordi di peering tra loro non appena diventano più convenienti del transito;
 \item un operatore Tier 2 può in breve tempo spostarsi a un operatore Tier 1 più conveniente;
 %38
 \item un operatore dominante può essere costretto dal garante del mercato a offrire delle connessioni in peering con gli ISP minori.
\end{itemize}

\section{Politiche di instradamento}
\label{sez:politiche_interdominio}
%171[A1]-3[A9]
Nell'instradamento inter-dominio, altri requisiti sono più importanti della semplice connettività di rete:
\begin{itemize}
 \item \ul{economici} (chi paga per la banda?): qualche volta si può preferire percorsi più lunghi ai percorsi migliori (sezione~\ref{sez:interdominio_economici});
 \item \ul{amministrativi} (è consentito passare?): qualche volta si può omettere alcuni percorsi all'altro party (sezione~\ref{sez:interdominio_amministrativi});
 \item \ul{di sicurezza} (quel dominio amministrativo è fidato?): qualche volta si può preferire percorsi più sicuri (e più lunghi) ai percorsi migliori (sezione~\ref{sez:interdominio_sicurezza}).
\end{itemize}

%9-19-24
Il percorso scelto dal protocollo di instradamento non è necessariamente il percorso a costo inferiore dal punto di vista tecnico, ma è il percorso migliore tra quelli che soddisfano i vincoli stabiliti dalle \textbf{politiche di instradamento} configurate dall'amministratore di rete, che riflettono gli accordi commerciali tra gli AS.

%49[A9]
Il \textbf{processo di decisione} sui router di frontiera è influenzato dalle politiche di instradamento:
\begin{itemize}
 %174[A1]
 \item \ul{tabella di instradamento}: può essere favorita la scelta di alcune rotte più economiche e scoraggiata la scelta di altre attraverso AS non fidati;
 %178[A1]
 \item \ul{annunci di rotte}: le rotte annunciate verso altri AS possono non corrispondere alla topologia reale della rete.
\end{itemize}

%172[A1]
\subsection{Requisiti economici}
\label{sez:interdominio_economici}
\begin{figure}
	\centering
	\includegraphics[scale=1.0175]{../pic/A1.6/172}
	\caption{Esempio di freeriding.}
	\label{fig:requisiti_economici}
\end{figure}

\noindent
L'invio del traffico su un link in transito costa $\Rightarrow$ un AS può avvantaggiarsi di un link in peering, anche se non è un link diretto, per far pagare il costo di transito all'altro AS paritetico (\textbf{freeriding}).

Nell'esempio in figura~\ref{fig:requisiti_economici}, i due AS italiani A e B sono interconnessi in peering, e ciascuno di essi è connesso in transito con l'AS statunitense C. Il percorso migliore secondo le regole dell'instradamento tradizionale è il percorso $x$ perché è costituito da un link diretto, ma A deve pagare per far passare il traffico su quel link. A può impostare una politica che preferisce il \ul{percorso} $y$ \ul{più economico}: dirotta tutto il traffico diretto a C sul link verso B, che è un link a basso costo per A $\Rightarrow$ B manderà il traffico di A sul suo link di transito verso C, pagando al posto di A.
\FloatBarrier

%175[A1]
\subsection{Requisiti amministrativi}
\label{sez:interdominio_amministrativi}
\begin{figure}
	\centering
	\includegraphics[scale=1.0175]{../pic/A1.6/175}
	\caption{Esempio di route hiding.}
	\label{fig:requisiti_amministrativi}
\end{figure}

\noindent
Un AS può impostare una politica amministrativa con lo scopo di non annunciare a un altro AS la connettività con alcun altro AS (\textbf{route hiding}).

Nell'esempio in figura~\ref{fig:requisiti_amministrativi}, B ha un link in transito verso C e lo usa per il suo traffico, ma annuncia la \ul{connettività parziale} omettendo l'informazione su questo link negli annunci che invia ad A, al fine di evitare che A si avvantaggi del link in peering per risparmiare sul costo di transito (e viceversa). A potrebbe non fidarsi di questo annuncio e a sua volta impostare una politica che forza staticamente tutto il traffico verso C a essere mandato comunque a B $\Rightarrow$ B si può difendere impostando una \textbf{Access Control List} (ACL) sul suo router di frontiera per scartare tutti i pacchetti provenienti da A e diretti verso C.
\FloatBarrier

%173[A1]
\subsection{Requisiti di sicurezza}
\label{sez:interdominio_sicurezza}
\begin{figure}
	\centering
	\includegraphics[scale=1.0175]{../pic/A1.6/173}
	\caption{Esempio di operatore non fidato.}
	\label{fig:requisiti_sicurezza}
\end{figure}

\noindent
Un operatore di rete può rappresentare una minaccia di sicurezza perché ad esempio compie abitualmente delle azioni di sniffing sul traffico che passa attraverso il suo AS $\Rightarrow$ un AS vuole evitare che il suo traffico diretto ad altri AS passi attraverso quell'operatore non fidato.

Nell'esempio in figura~\ref{fig:requisiti_sicurezza}, A per raggiungere C preferisce il \ul{percorso} $x$ più lungo ma \ul{più sicuro} perché non attraversa l'operatore B non fidato, anche se quest'ultimo annuncia il percorso $y$ a basso costo verso C.
\FloatBarrier

\section{Internet Exchange Point}
%47
Non è conveniente interconnettere due AS per \textbf{connessione diretta}, cioè con un singolo link geografico tra di essi:
\begin{itemize}
 \item \ul{costo del link}: l'installazione può richiedere operazioni di scavo;
 \item \ul{costo delle interfacce sui router}: devono inviare il segnale a lunghe distanze;
 \item \ul{flessibilità}: è necessario intervenire sull'infrastruttura fisica per creare una nuova interconnessione.
\end{itemize}

%45
Un \textbf{Internet Exchange Point} (IXP) consente a più router di frontiera di AS (ISP) differenti di scambiarsi informazioni di instradamento esterno in modo più dinamico e flessibile.

%48-49
I router sono connessi tramite una rete locale intermedia \textbf{di livello data-link}: tecnicamente tutti i router sono direttamente raggiungibili, ma in pratica le politiche di instradamento definiscono le interconnessioni a seconda degli accordi commerciali tra gli AS $\Rightarrow$ per creare una nuova interconnessione, è sufficiente configurare le politiche di instradamento sui singoli router senza dover modificare l'infrastruttura fisica. Una interconnessione può anche essere attiva ma utilizzata solo come backup (selezione fatta in BGP).

%50-51
Di solito ogni AS paga una tassa mensile, che dipende ad esempio dalla velocità della connessione all'IXP. L'IXP è responsabile del funzionamento tecnico degli switch nella rete intermedia:
\begin{itemize}
 \item \ul{singola ubicazione}: spesso tutti i router sono concentrati all'interno di una stanza in un datacenter, dove vengono forniti:
 \begin{itemize}
 %46
 \item rete di livello data-link ad alta velocità;
 \item energia elettrica, sistema di condizionamento;
 \item servizio di monitoraggio;
 \item vicinanza alle dorsali in fibra ottica;
 \end{itemize}
 \item \ul{infrastruttura distribuita}: più punti di accesso sono disponibili nelle principali città sul territorio (ad esempio, il TOPIX si estende per l'intera regione piemontese).
\end{itemize}

%48
L'IXP è anche noto come \textbf{Neutral Access Point} (NAP): l'IXP deve essere neutrale e non coinvolto negli affari dei suoi clienti. Un IXP può decidere di non consentire accordi di transito: ad esempio, il MIX di Milano è un'organizzazione non profit che ammette solamente accordi di peering per favorire la diffusione di Internet in Italia, ma ciò può limitare l'ammontare di traffico scambiato attraverso l'IXP perché gli ISP disponibili solo ad accordi di transito sceglieranno degli altri IXP.

\section{Neutralità della rete}
La \textbf{neutralità della rete} è il principio secondo il quale tutto il traffico deve essere trattato equamente, senza privilegiare o danneggiare una parte del traffico per interessi economici.

%55
Gli operatori di rete possono essere tentati di dare un ``trattamento preferenziale'' a porzioni di traffico:
\begin{itemize}
 \item \ul{privilegiare} del traffico: offrire un servizio migliore a un certo tipo di traffico (ad es. maggiore velocità);
 \item \ul{danneggiare} del traffico: offrire un servizio peggiore, oppure totalmente nessun servizio, a un certo tipo di traffico.
\end{itemize}

%56-57
Una rete neutrale garantisce che tutte le entità (ad es. i content provider) abbiano lo stesso servizio, senza che qualche servizio venga ucciso a discrezione dell'operatore di rete, ma forzare la ``pura'' neutralità della rete implica che non è assolutamente possibile il controllo del traffico, che può essere utile in molti casi; dall'altro lato, se si ammette che la rete può non essere neutrale, all'operatore di rete è dato il potere di privilegiare del traffico o del contenuto. In un mercato aperto la palla è lasciata all'utente: se gli utenti non sono d'accordo che il loro traffico VoIP venga discriminato, possono passare a un altro operatore di rete (anche se in pratica ciò può non essere sempre possibile a causa dei cartelli tra gli operatori di rete).

\paragraph{Esempi di non neutralità}
\begin{itemize}
%53-54
\item \ul{content provider}: gli ISP vorrebbero avere una parte degli utili dei content provider $\Rightarrow$ un ISP può privilegiare il traffico diretto a un content provider con cui ha stipulato un contratto di ripartizione degli utili;
\item \ul{peer-to-peer} (P2P):
\begin{itemize}
 %29-30
 \item gli utenti finali non si preoccupano della destinazione del loro traffico, ma il traffico P2P può raggiungere ogni utente in ogni AS del mondo facendo pagare degli alti costi all'ISP $\Rightarrow$ un ISP può privilegiare il traffico che è generato all'interno dell'AS (ad es. AdunanzA di Fastweb);
 \item il traffico P2P è più simmetrico perché utilizza molto la banda in upload, mentre le reti sono state dimensionate per supportare un traffico asimmetrico $\Rightarrow$ un ISP può privilegiare il traffico asimmetrico (ad es. normale traffico Web);
\end{itemize}
%53
\item \ul{qualità del servizio} (QoS): un ISP può privilegiare il traffico con un livello di priorità maggiore (ad es. traffico VoIP);
%56
\item \ul{sicurezza}: un ISP può bloccare il traffico da utenti malintenzionati (ad es. attacco DDoS).
\end{itemize}