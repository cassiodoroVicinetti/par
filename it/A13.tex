\chapter{Content Delivery Network}
\label{cap:CDN}
%3
Una \textbf{cache Web} è un dispositivo che archivia una copia locale dei contenuti (ad es. risorse HTTP) richiesti più di recente e reagisce come un server proxy alle richieste dei client:
\begin{itemize}
\item la cache Web è più vicina all'utente rispetto al server Web:
\begin{itemize}
\item[\pro] \ul{prestazioni}: la risposta è più veloce quando la risorsa richiesta si trova già in cache;
\item[\pro] \ul{banda}: non vengono caricati costosi link a lunga distanza (ad es. link transoceanici);
\end{itemize}

\item[\con] \ul{soluzione reattiva}: se la risorsa richiesta non si trova in cache, l'utente deve attendere che la cache Web la acquisisca (\textbf{pull}) dal server Web;
\item[\con] \ul{no trasparenza}: il browser Web dell'utente deve essere configurato manualmente per contattare quella cache Web.
\end{itemize}

%7
Una \textbf{Content Delivery Network} (CDN) è una overlay network\footnote{Una \textbf{overlay network} è una rete di calcolatori che è costruita al di sopra di un'altra rete.} di cache Web sparse in giro per il mondo ma cooperanti con l'obiettivo di offrire all'utente una migliore quality of experience\footnote{La \textbf{quality of experience} è una misura delle esperienze di un utente con un servizio (ad es. navigazione Web).}:
%4
\begin{itemize}
\item \ul{soluzione proattiva}: il server Web copia (\textbf{push}) i contenuti (generalmente i più popolari) sulle cache Web prima che gli utenti li richiedano;
\item \ul{trasparenza}: l'utente si collega alla cache Web automaticamente, senza necessità di configurazione manuale sul suo client;
\item \ul{prestazioni}: l'utente, anche se si sposta, si collega sempre alla cache Web più vicina;
\item \ul{bilanciamento del carico}: l'utente si collega sempre alla cache Web meno carica;
%6
\item \ul{scalabilità}: la distribuzione dei contenuti in più repliche permette un grande numero di richieste che un singolo server Web da solo non riuscirebbe a servire;
\item \ul{accesso condizionato}: è possibile personalizzare i contenuti restituiti in base all'utente (ad es. annunci pubblicitari mirati).
\end{itemize}

%5
Le CDN sono l'ideale per contenuti che generano grandi quantità di traffico (ad es. risorse multimediali), ma non tutti i contenuti possono essere memorizzati in cache:
\begin{itemize}
\item pagine Web dinamiche (ad es. quotazioni di Borsa);
\item pagine Web dal contenuto personalizzato (ad es. account utente).
\end{itemize}

Le CDN possono essere distribuite in vari modi:
\begin{itemize}
\item \textbf{CDN basate sui DNS}: il traffico è reindirizzato alla replica migliore in base agli host name:
\begin{itemize}
\item \ul{instradamento basato sui DNS} (sez.~\ref{sez:CDN_DNS}): l'hosting provider deve stipulare accordi con i gestori dei server DNS;
\item \ul{approccio di Akamai} (sez.~\ref{sez:CDN_Akamai}): non è necessario intervenire sui server DNS;
\end{itemize}

%15
\item \textbf{CDN basate sugli URL}: il traffico è reindirizzato alla replica migliore in base agli URL completi:
\begin{itemize}
\item \ul{server load balancing} (sez.~\ref{sez:CDN_SLB}): il punto di terminazione della connessione TCP è vicino al server;
\item \ul{content routing} (sez.~\ref{sez:CDN_content_router}): il punto di terminazione della connessione TCP è vicino al client.
\end{itemize}
\end{itemize}

\section{CDN basate sui DNS}
\subsection{Instradamento basato sui DNS}
\label{sez:CDN_DNS}
\begin{figure}
	%11
	\centering
	\includegraphics{../pic/A13/11}
	\caption{Navigazione tradizionale.}
\end{figure}

\begin{figure}
	%12
	\centering
	\includegraphics{../pic/A13/12}
	\caption{Navigazione CDN basata sui DNS.}
\end{figure}

%10
\noindent
La selezione della replica migliore ha luogo quando l'host name viene tradotto in un indirizzo IP. La risposta DNS a una query non dipende solo dall'host name, ma anche dalla sorgente: uno speciale server DNS calcola, in base a più metriche possibili (RTT, carico dei server, tempo di risposta, ecc.), una tabella di instradamento delle repliche contenente entry del tipo:\\
\centerline{\{host name, indirizzo IP client\} $\rightarrow$ indirizzo IP replica}

%9
Il motore di instradamento nel server DNS ``modificato'' ha un'interfaccia standard per garantire la \ul{trasparenza}: l'utente crede che l'indirizzo IP corrispondente all'host name sia l'indirizzo IP del server Web reale, mentre è l'indirizzo IP di una sua replica.

L'aggiunta di un nuovo attore, l'\textbf{hosting provider}, costituisce una nuova opportunità di business nel mondo delle reti:
\begin{itemize}
\item \ul{access provider}: fornisce l'accesso alla rete agli utenti;
\item \ul{backbone provider}: fornisce la connettività a lungo raggio;
\item \ul{hosting provider}: fornisce il servizio di CDN ai content provider;
\item \ul{content provider}: fornisce i contenuti.
\end{itemize}
\FloatBarrier

\paragraph{Problemi}
\begin{itemize}
\item \ul{metriche}: la misurazione delle metriche, soprattutto di quelle dinamiche, non è facile, e le metriche di livello 3 da sole non sono particolarmente significative;
\item \ul{caching DNS}: solo il server autoritativo conosce tutte le repliche e può selezionare la replica migliore in base alla posizione del client $\Rightarrow$ i server DNS intermedi nella gerarchia non possono memorizzare in cache le risposte DNS;
%13
\item \ul{granularità}: la granularità della redirezione è a livello di host name, non di singoli URL $\Rightarrow$ il contenuto di grandi siti Web non può essere suddiviso in più cache, quindi due diverse pagine dello stesso sito Web saranno chieste alla stessa replica.
\end{itemize}

\subsection{Approccio di Akamai}
\label{sez:CDN_Akamai}
La CDN di Akamai sfrutta un algoritmo automatico proprietario per reindirizzare il traffico alle proprie repliche senza intervenire sui server DNS:
\begin{enumerate}
\item l'utente digita l'indirizzo di una pagina Web con il suo normale nome di dominio (ad es. \texttt{http://cnn.com/index.html});
\item il server del content provider (ad es. CNN) restituisce una pagina Web in cui l'indirizzo di ogni risorsa multimediale (ad es. immagine) ha uno speciale nome di dominio corrispondente a una specifica replica su una cache di Akamai (ad es. \texttt{http://a128.g.akamai.net/7/23/cnn.com/a.gif} invece di \texttt{http://cnn.com/a.gif});
\item il browser Web dell'utente durante il parsing della pagina effettua delle query DNS ai nuovi nomi di dominio e recupera le risorse multimediali dalle repliche più vicine.
\end{enumerate}

\section{CDN basate sugli URL}
\subsection{Server load balancing}
\label{sez:CDN_SLB}
\begin{figure}
	%20
	\centering
	\includegraphics[scale=1.05]{../pic/A13/20}
	\caption{SLB content-aware.}
\end{figure}

%17
\noindent
I server reali contenenti le repliche sono visti dai client come un unico server virtuale con lo stesso indirizzo IP.

%18
Il carico di traffico destinato al server virtuale è bilanciato tra i diversi server reali da un \textbf{Server Load Balancer} (SLB):
%19
\begin{itemize}
\item \ul{commutazione a livello 4}: le connessioni TCP non sono terminate dal SLB (\textbf{content-unaware}):
\begin{itemize}
\item uno dei server reali risponde al three-way handshake con il client;
\item tutte le query HTTP appartenenti alla stessa sessione TCP devono essere servite sempre dallo stesso server reale;
\item il bilanciamento del carico può essere basato sull'indirizzo IP sorgente, sulla porta TCP sorgente, ecc.;
\end{itemize}

%20
\item \ul{commutazione a livello 7}: le connessioni TCP sono terminate dal SLB (\textbf{content-aware}), che agisce come un proxy:
\begin{itemize}
\item il SLB risponde al three-way handshake con il client, per poter intercettare gli URL richiesti successivamente;
\item ogni query HTTP può essere servita dal server reale correntemente meno carico, in base alle decisioni del SLB;
\item il bilanciamento del carico è basato sull'URL completo.
\end{itemize}
\end{itemize}

\paragraph{Problemi}
\begin{itemize}
%19
\item \ul{connessioni cifrate} (HTTPS): il SLB deve avere la chiave crittografica SSL privata del server, e deve sopportare il carico di elaborazione per criptare/decriptare i pacchetti in transito;
%21
\item \ul{connessioni sticky}: alcune applicazioni richiedono che le connessioni TCP dallo stesso client siano reindirizzate allo stesso server (ad es. carrello della spesa) $\Rightarrow$ occorre considerare anche i cookie;
\item \ul{distribuzione geografica}: tutte le repliche sono vicine tra loro e al SLB, che è lontano dal client.
\end{itemize}
\FloatBarrier

\subsection{Content routing}
\label{sez:CDN_content_router}
%26
I \textbf{content router} sono dei router che instradano il traffico in base all'URL verso la replica migliore:
\begin{itemize}
\item \ul{TCP}: i content router in sequenza terminino tutti delle connessioni TCP tra uno e l'altro $\Rightarrow$ vengono introdotti troppo ritardi;
\item \ul{content delivery control protocol}: l'URL è estratto dal primo content router, ed è propagato da un protocollo specifico.
\end{itemize}

\paragraph{Problemi}
\begin{itemize}
%23
\item \ul{stateful}: il primo content router deve terminare la connessione TCP per poter intercettare l'URL che l'utente richiederà;
\item \ul{complessità degli apparati}: il parsing dei pacchetti per recuperare l'URL è complicato $\Rightarrow$ gli switch di livello 7 sono apparati complicati e costosi;
%26
\item \ul{complessità dei protocolli}: i content delivery control protocol proposti sono veramente complicati;
%28
\item \ul{riservatezza}: i content router leggono tutti gli URL richiesti dagli utenti.
\end{itemize}