\chapter[Algoritmi di instradamento]{Algoritmi di instradamento%
%26
\footnote{Gli algoritmi di instradamento presentati nel seguito assumono di operare su una rete basata sull'instradamento per indirizzo di rete.}}
%27
Un \textbf{algoritmo di instradamento} è un processo di tipo collaborativo responsabile di decidere, in ogni nodo intermedio, le direzioni che devono essere utilizzate per raggiungere le destinazioni:
\begin{enumerate}
 %28
 \item determina le \textbf{destinazioni raggiungibili} da ogni nodo:
 \begin{itemize}
  \item \ul{genera} le informazioni sulla raggiungibilità delle reti \ul{locali}: il router informa i router vicini che la rete locale esiste ed è raggiungibile attraverso di esso;
  \item \ul{riceve} le informazioni sulla raggiungibilità delle reti \ul{remote}: un router vicino informa il router che la rete remota esiste ed è raggiungibile attraverso di esso;
  \item \ul{propaga} le informazioni sulla raggiungibilità delle reti \ul{remote}: il router informa gli altri router vicini che la rete remota esiste ed è raggiungibile attraverso di esso;
 \end{itemize}
 %29
 \item calcola i \textbf{percorsi ottimali} (next hop), in maniera cooperativa con gli altri nodi, secondo certi criteri:
 \begin{itemize}
  \item occorre stabilire una \ul{metrica}: un percorso può essere il migliore in base a una metrica ma non in base a un'altra metrica;
  \item i criteri devono essere \ul{coerenti} tra tutti i nodi nella rete per evitare cicli, buchi neri, ecc.;
  \item l'algoritmo deve operare in maniera \ul{automatica} per evitare gli errori umani nella configurazione manuale e per favorire la scalabilità;
 \end{itemize}
 %35
 \item memorizza delle informazioni locali nella \textbf{tabella di instradamento} di ogni nodo: un algoritmo di instradamento non necessita di conoscere l'intera topologia della rete, ma è solo interessato a costruire la tabella di instradamento corretta.
\end{enumerate}

\paragraph{Caratteristiche di un algoritmo di instradamento ideale}
\begin{itemize}
%33
 \item \ul{semplice} da implementare: meno bug, facile da capire, ecc.;
 \item \ul{leggero} da eseguire: i router devono occupare meno risorse possibili per l'esecuzione dell'algoritmo poiché dispongono di CPU e memoria limitati;
 \item \ul{ottimale}: i percorsi calcolati devono essere ottimali secondo le metriche scelte;
 \item \ul{stabile}: deve passare a un altro percorso solo quando c'è un cambiamento di topologia o di costo per evitare il \textbf{route flapping}, cioè il cambiamento frequente delle rotte preferite con conseguente eccesso di periodi di transitorio;
 \item \ul{equo}: non deve favorire alcun nodo o percorso particolare;
%34
 \item \ul{robusto}: deve essere capace di adattarsi automaticamente ai cambiamenti di topologia o di costo:
 \begin{itemize}
  \item \textbf{rilevamento dei guasti}: non deve affidarsi a componenti esterni per rilevare un guasto (ad es. un guasto non può essere rilevato a livello fisico se avviene al di là di un hub);
  \item \textbf{autostabilizzazione}: a fronte di variazioni nella rete deve convergere a una soluzione senza alcun intervento esterno (ad es. configurazione manuale esplicita);
  \item \textbf{robustezza bizantina}: deve riconoscere e isolare un nodo vicino che sta inviando delle informazioni fasulle, a causa di un guasto o di un attacco malevolo.\\
  Internet non implementa la robustezza bizantina, ma si basa sulla fiducia $\Rightarrow$ i guasti e i comportamenti malevoli richiedono l'intervento umano.
 \end{itemize}
\end{itemize}

%50
\paragraph{Classificazione degli algoritmi di instradamento}
\begin{itemize}
 \item \textbf{algoritmi non adattativi} (statici): prendono le decisioni indipendentemente da come è fatta la rete (sezione~\ref{sez:algoritmi_non_adattativi}):
 \begin{itemize}
 \item \ul{instradamento statico} (o fixed directory routing)
 \item random walk
 \item flooding, flooding selettivo
 \end{itemize}
 \item \textbf{algoritmi adattativi} (dinamici): apprendono informazioni sulla rete per prendere meglio le decisioni (sezione~\ref{sez:algoritmi_adattativi}):
 \begin{itemize}
 \item \ul{instradamento centralizzato}
 \item \ul{instradamento isolato}: hot potato, backward learning
 \item \ul{instradamento distribuito}: Distance Vector, Link State
 \end{itemize}
 \item \textbf{algoritmi gerarchici}: permettono agli algoritmi di instradamento di scalare su infrastrutture ampie (capitolo~\ref{cap:algoritmi_gerarchici}).
\end{itemize}

\section{Metrica}
\label{sez:metrica}
%30
Una \textbf{metrica} è la misura di quanto buono è un percorso, ricavata dalla trasformazione di una grandezza fisica (ad es. distanza, velocità di trasmissione), o di una combinazione di esse, in forma numerica (\textbf{costo}), al fine di scegliere il percorso a costo inferiore come il percorso migliore.

Non esiste una metrica migliore per tutti i tipi di traffico: ad esempio la larghezza di banda è adatta per il traffico di trasferimento file, mentre il ritardo di trasmissione è adatto per il traffico in tempo reale. La scelta della metrica può essere determinata dal campo ``Type of Service'' (TOS) del pacchetto IP.

\paragraph{Problemi}
\begin{itemize}
  %31
  \item \ul{(non-)ottimizzazione}: il compito primario dei router è inoltrare il traffico degli utenti, non passare il tempo a calcolare percorsi $\Rightarrow$ conviene prediligere soluzioni che, pur non essendo pienamente ottimizzate, non compromettono la funzionalità primaria della rete e non manifestano dei problemi percepibili dall'utente finale:
  \begin{itemize}
    \item \ul{complessità}: più criteri vengono combinati, più l'algoritmo diventa complesso e richiede risorse computazionali in fase di esecuzione;
    \item \ul{stabilità}: una metrica basata sulla banda disponibile sul link è troppo instabile, poiché dipende dal carico istantaneo di traffico che è molto variabile nel tempo, e può portare al route flapping;
  \end{itemize}
  %32
  \item \ul{inconsistenza}: le metriche adottate dai nodi nella rete devono essere coerenti (per ogni pacchetto) per evitare il rischio di cicli, ovvero il ``rimbalzo'' di un pacchetto tra due router che utilizzano metriche differenti in conflitto.
\end{itemize}

\section{Transitori}
%41
Gli algoritmi di instradamento moderni sono sempre ``attivi'': si scambiano dei messaggi di servizio continuamente per rilevare i guasti autonomamente. Tuttavia, non cambiano la tabella di instradamento a meno che non viene rilevato un \textbf{cambiamento di stato}:
\begin{itemize}
 \item \ul{cambiamenti di topologia}: guasto di un link, aggiunta di una nuova destinazione;
 \item \ul{cambiamenti di costo}: ad esempio un link a 100 Mbps sale a 1 Gbps.
\end{itemize}

%36
%Tutti gli algoritmi di instradamento non possono non intervallare il normale funzionamento in convergenza della rete con delle fasi di transitorio:
I cambiamenti di stato danno origine a \textbf{fasi di transitorio}: non è possibile aggiornare allo stesso tempo tutti i nodi di un sistema distribuito, poiché una variazione è propagata nella rete a velocità finita $\Rightarrow$ durante il transitorio, le informazioni di stato nella rete possono non essere coerenti: alcuni nodi hanno già le informazioni nuove (ad es. il router che rileva il guasto), mentre altri hanno ancora le informazioni vecchie.

%37
Non tutti i cambiamenti di stato hanno lo stesso impatto sul traffico dati:
\begin{itemize}
 \item \ul{cambiamenti di stato positivi}: l'effetto del transitorio è limitato perché la rete può solo funzionare temporaneamente in una condizione sub-ottimale:
 \begin{itemize}
 \item un percorso ottiene un costo migliore: alcuni pacchetti possono continuare a seguire il percorso vecchio ora divenuto meno conveniente;
 \item viene aggiunta una nuova destinazione: la destinazione può apparire irraggiungibile a causa di buchi neri lungo il percorso verso di essa;
 \end{itemize}
 \item \ul{cambiamenti di stato negativi}: l'effetto del transitorio si manifesta più gravemente all'utente perché interferisce anche con il traffico che non dovrebbe essere influenzato dal guasto:
 \begin{itemize}
 \item avviene il guasto di un link: non tutti i router hanno appreso che il percorso vecchio non è più disponibile $\Rightarrow$ il pacchetto può cominciare a ``rimbalzare'' avanti e indietro saturando il link alternativo (routing loop);
 \item un percorso peggiora il proprio costo: non tutti i router hanno appreso che il percorso vecchio non è più conveniente $\Rightarrow$ analogo al caso di guasto (routing loop).
 \end{itemize}
\end{itemize}

In generale, due problemi comuni affliggono gli algoritmi di instradamento durante il transitorio: i buchi neri e i routing loop.

%38
\subsection{Buchi neri}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A1.2/38}
	\caption{Buco nero.}
\end{figure}

\noindent
Si dice \textbf{buco nero} un router che, pur esistendo almeno un percorso attraverso cui potrebbe raggiungere una certa destinazione, non ne conosce (ancora) nessuno.

\paragraph{Effetto} L'effetto sul traffico dati è limitato ai pacchetti diretti verso la destinazione interessata, che vengono scartati fino a quando il nodo non aggiorna la tabella di instradamento e acquisisce le informazioni su come raggiungerla. Il traffico diretto ad altre destinazioni non è assolutamente influenzato dal buco nero.
\FloatBarrier

%39-40
\subsection{Routing loop}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A1.2/40}
	\caption{Routing loop.}
\end{figure}

\noindent
Si dice \textbf{routing loop} un percorso ciclico dal punto di vista dell'instradamento: un router invia un pacchetto su un link ma, a causa di un'inconsistenza nelle tabelle di instradamento, il router all'altra estremità del link lo rispedisce indietro.

\paragraph{Effetto} I pacchetti diretti verso la destinazione interessata cominciano a ``rimbalzare'' avanti e indietro (\textbf{bouncing effect}) $\Rightarrow$ il link viene saturato $\Rightarrow$ viene influenzato anche il traffico diretto ad altre destinazioni che passa per quel link.
\FloatBarrier

%42-43
\subsection{Rotta di backup}
La \textbf{rotta di backup} è un concetto principalmente utilizzato nelle reti telefoniche basate su una organizzazione gerarchica: ogni centrale è collegata a una centrale di livello superiore con una rotta primaria, e a un'altra centrale di livello superiore con una rotta di backup $\Rightarrow$ se avviene un guasto nella rotta primaria, la rotta di backup è pronta a entrare in funzione senza periodi di transitorio.

Una rete dati è invece basata su una topologia magliata, dove i router sono interconnessi in vario modo $\Rightarrow$ è impossibile prevedere tutti i possibili guasti della rete per predisporre i percorsi di backup, ma quando avviene un guasto è preferibile un algoritmo di instradamento che calcoli automaticamente sul momento un percorso alternativo (anche se la fase di calcolo richiede un periodo di transitorio).

La rotta di backup nelle reti moderne può avere ancora delle applicazioni:
\begin{itemize}
 \item un'azienda collegata a Internet tramite ADSL può mantenere la connettività quando la linea ADSL cade passando a una rotta di backup tramite tecnologia HiperLAN (senza fili);
 \item il backbone Internet è ormai in mano alle compagnie telefoniche, che l'hanno modellato secondo i criteri delle reti telefoniche $\Rightarrow$ l'organizzazione è abbastanza gerarchica da consentire la predisposizione di rotte di backup.
\end{itemize}

%44
\section{Multipath routing}
Con l'instradamento per indirizzo di rete, tutti i pacchetti verso una destinazione passano per lo stesso percorso, anche se sono disponibili dei percorsi alternativi $\Rightarrow$ sarebbe preferibile far passare una parte del traffico su un percorso alternativo, anche se di costo superiore, per non saturare il percorso scelto dall'algoritmo.

Il \textbf{multipath routing}, anche noto come ``load sharing'', è una funzione di traffic engineering che mira a distribuire il traffico verso la stessa destinazione su più percorsi (quando disponibili), consentendo più entry per ogni destinazione nella tabella di instradamento, per un uso più efficiente delle risorse di rete.

%45
\subsection{Multipath routing a costi differenziati}
\label{sez:unequal_multipath_routing}
Un percorso alternativo viene usato solo se ha un costo $c_\text{sec}$ non troppo maggiore del costo $c_\text{prim}$ del percorso primario a costo minimo:
 \[
  \text{dati } K \ge 1 , \,  c_\text{sec} \ge c_\text{prim} : \; \text{sec usato} \Leftrightarrow c_\text{sec} \le K \cdot c_\text{prim}
 \]

Il traffico viene distribuito in modo inversamente proporzionale al costo delle rotte. Ad esempio in questo caso:
 \[
  \begin{cases} c_\text{prim} = 10 \\ c_\text{sec} = 20 \end{cases} \Rightarrow \begin{cases} \text{prim} = 66\% \text{ traffico} \\ \text{sec} = 33\% \text{ traffico} \end{cases}
 \]
il router può decidere di mandare il pacchetto con il 66\% di probabilità lungo il percorso primario e con il 33\% di probabilità lungo il percorso secondario.

\paragraph{Problema} Dato un pacchetto, ogni router decide autonomamente su quale percorso inoltrarlo $\Rightarrow$ decisioni incoerenti tra un router e l'altro possono far entrare il pacchetto in un routing loop, e siccome l'inoltro è di solito basato sulla sessione quel pacchetto non uscirà mai dal ciclo:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.47\linewidth]{../pic/A1.2/45}
	\caption{Routing loop causato dal multipath routing a costi differenziati.}
\end{figure}

\subsection{Multipath routing a costi equivalenti}
%46
Un percorso alternativo viene usato solo se ha un costo $c_\text{sec}$ esattamente uguale al costo $c_\text{prim}$ del percorso primario ($K=1$):
 \[
  \text{dato } c_\text{sec} \ge c_\text{prim} : \; \text{sec usato} \Leftrightarrow c_\text{sec} = c_\text{prim}
 \]
 
Il traffico viene partizionato equamente su entrambi i percorsi (50\%).

%47
\paragraph{Problemi} Se il primo pacchetto segue il percorso lento e il secondo pacchetto segue il percorso veloce, i meccanismi del TCP possono causare il peggioramento delle prestazioni complessive:
\begin{figure}
\centering
	\begin{subfigure}[b]{.47\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/A1.2/46}
		\caption{Problema di TCP reordering.}
	\end{subfigure}%
	\begin{subfigure}[b]{.53\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/A1.2/47}
		\caption{Riduzione della velocità di trasmissione.}
	\end{subfigure}
\caption{Problemi causati dal multipath routing a costi equivalenti.}
\end{figure}
\begin{itemize}
 \item \textbf{problema di TCP reordering}: i pacchetti possono arrivare fuori sequenza: il secondo pacchetto arriva alla destinazione prima del primo pacchetto $\Rightarrow$ il processo di riordinamento dei numeri di sequenza impegna le risorse computazionali del ricevitore;
 \item \textbf{riduzione della velocità di trasmissione}: se il pacchetto di acknowledgment (ACK) del primo pacchetto arriva troppo in ritardo, la sorgente crede che il primo pacchetto sia andato perso $\Rightarrow$ quando arrivano 3 ACK (cumulativi) duplicati e scade il timeout, entrano in azione i meccanismi a finestra di scorrimento del TCP:\footnote{Si rimanda alla sezione \textit{Uso dei protocolli a finestra} nel capitolo \textit{Livello Trasporto: Protocolli TCP-UDP} negli appunti di ``Reti di calcolatori''.}
\begin{itemize}
 \item \ul{fast retransmit}: la sorgente ritrasmette inutilmente il pacchetto $\Rightarrow$ il pacchetto diventa duplicato;
 \item \ul{fast recovery}: la sorgente crede che la rete sia congestionata $\Rightarrow$ rallenta l'invio di pacchetti limitando la finestra di trasmissione: imposta il valore di soglia alla metà del valore corrente della finestra di congestione, e fa ripartire la finestra di congestione dal valore 1 (cioè viene mandato un solo pacchetto per volta e se ne attende l'ACK prima di mandare il pacchetto successivo).
\end{itemize}
\end{itemize}
\FloatBarrier

%48-49
\subsubsection{Criteri}
Le implementazioni reali del multipath routing suddividono il traffico in modo che il traffico verso una stessa destinazione segua lo stesso percorso:
\begin{itemize}
 \item \textbf{in base al flusso}: ogni sessione di livello trasporto è identificata dalla quintupla:
 \begin{enumerate}
 \item \texttt{\textless indirizzo IP sorgente\textgreater}
 \item \texttt{\textless indirizzo IP di destinazione\textgreater}
 \item \texttt{\textless tipo di protocollo di livello trasporto\textgreater} (ad es. TCP)
 \item \texttt{\textless porta sorgente\textgreater}
 \item \texttt{\textless porta di destinazione\textgreater}
 \end{enumerate}
 e il router memorizza una tabella che mappa gli identificativi di sessione con le porte di uscita:
\begin{itemize}
 \item l'estrazione dal pacchetto dei campi che formano l'ID di sessione è onerosa, a causa della varietà di formati di pacchetto supportati (VLAN, MPLS\textellipsis);
 \item le informazioni sulle porte di livello trasporto non sono disponibili in caso di pacchetti IP frammentati;
 \item la ricerca della quintupla nella tabella degli ID di sessione è onerosa;
 \item spesso la chiusura delle connessioni TCP non è ``graceful leaving'', cioè non vengono inviati i pacchetti FIN e ACK $\Rightarrow$ le entry nella tabella degli ID di sessione non vengono cancellate $\Rightarrow$ occorre un thread che ogni tanto effettui la pulizia delle entry vecchie guardandone i timestamp;
\end{itemize}
\item \textbf{in base al pacchetto}: il router invia i pacchetti con indirizzo IP (di destinazione o sorgente o entrambi) pari su un percorso, e con indirizzo IP dispari sull'altro percorso $\Rightarrow$ l'operazione di hashing è molto rapida.
\end{itemize}

\paragraph{Problema} Il traffico verso una stessa destinazione non può usare entrambi i percorsi allo stesso tempo $\Rightarrow$ ci sono dei problemi in caso di traffico molto grande verso una certa destinazione (ad es. un backup notturno tra due server).

\section{Algoritmi non adattativi}
\label{sez:algoritmi_non_adattativi}
\subsection{Instradamento statico}
%51
L'amministratore della rete configura manualmente su ogni router la tabella di instradamento.

\paragraph{Svantaggio} Se avviene un cambiamento nella rete, bisogna aggiornare manualmente le tabelle di instradamento.

%53
\paragraph{Applicazione} L'instradamento statico è utilizzato principalmente sui router ai margini della rete:
%52
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/A1.2/52}
\end{figure}
\begin{itemize}
\item ai router edge non è consentito \ul{propagare} le informazioni di instradamento verso il backbone: il router core blocca tutti gli annunci in arrivo dalla periferia, altrimenti un utente potrebbe annunciare una rete con lo stesso indirizzo di una rete esistente (ad es. google.com) e dirottare verso di lui una parte del traffico diretto a quella rete.\\
Dato che gli utenti non possono annunciare le proprie reti, come fanno a essere raggiungibili dall'esterno? L'ISP, che sa quali indirizzi usano le reti che ha venduto agli utenti, deve configurare il router core in modo che annunci quelle reti agli altri router anche se esse non sono localmente connesse;
\item ai router edge non è consentito \ul{ricevere} le informazioni di instradamento dal backbone: un router edge è tipicamente connesso con un singolo link a un router core $\Rightarrow$ una rotta predefinita globale è sufficiente per raggiungere l'intera Internet.
\end{itemize}
\FloatBarrier

%56
\subsection{Random walk}
Quando arriva un pacchetto, il router sceglie una porta a caso (tranne quella da cui è stato ricevuto) e lo manda in uscita su quella porta.

\paragraph{Applicazioni} È utile quando la probabilità che il pacchetto raggiunga la destinazione è elevata:
\begin{itemize}
 \item reti peer-to-peer (P2P): per la ricerca dei contenuti;
 \item reti di sensori: l'invio di messaggi deve essere un'operazione a basso consumo.
\end{itemize}

\subsection{Flooding}
%57
Quando arriva un pacchetto, il router lo manda in uscita su tutte le porte (tranne quella da cui è stato ricevuto).

I pacchetti possono avere un campo ``numero di hop'' per limitare il flooding a una porzione della rete.

%58
\paragraph{Applicazioni}
\begin{itemize}
 \item applicazioni militari: in caso di attacco la rete potrebbe essere danneggiata $\Rightarrow$ è fondamentale che il pacchetto arrivi a destinazione, anche a costo di avere un'enorme quantità di traffico duplicato;
 %\item database distribuiti: l'aggiornamento simultaneo di più database può essere effettuato tramite la trasmissione di un singolo pacchetto;
 \item algoritmo Link State: ogni router quando riceve la mappa della rete da un vicino deve propagarla agli altri vicini (si rimanda alla sezione~\ref{sez:ls_flooding}).
\end{itemize}

\subsection{Flooding selettivo}
\label{sez:selective_flooding}
%59
Quando arriva un pacchetto, il router prima controlla se lo ha già ricevuto e mandato in flooding in passato:
\begin{itemize}
 \item pacchetto vecchio: lo scarta;
 \item pacchetto nuovo: lo memorizza e lo manda in uscita su tutte le porte (tranne quella da cui è stato ricevuto).
\end{itemize}

%60
Ogni pacchetto è riconosciuto tramite l'identificativo del mittente (ad es. l'indirizzo IP sorgente) e il numero di sequenza:
\begin{itemize}
 \item se il mittente si disconnette dalla rete o si spegne, quando si riconnette il numero di sequenza ricomincia da capo $\Rightarrow$ il router vede tutti i pacchetti ricevuti come pacchetti vecchi;
 \item i numeri di sequenza sono codificati su un numero limitato di bit $\Rightarrow$ lo spazio dei numeri di sequenza deve essere scelto in modo da minimizzare l'errato riconoscimento di pacchetti nuovi come pacchetti vecchi.
\end{itemize}

%61-62
\subsubsection{Spazi dei numeri di sequenza}
\paragraph{Spazio lineare} Può essere tollerabile se il selective flooding è usato per pochi messaggi di controllo:
 \begin{itemize}
 \item quando il numero di sequenza raggiunge il valore massimo possibile, si verifica un errore di overflow;
 \item pacchetto vecchio: il numero di sequenza è minore di quello corrente;
 \item pacchetto nuovo: il numero di sequenza è maggiore di quello corrente.
 \end{itemize}
 
\paragraph{Spazio circolare} Risolve il problema dell'esaurimento dello spazio dei numeri di sequenza, ma fallisce se arriva un pacchetto con numero di sequenza troppo lontano da quello corrente:
 \begin{itemize}
 \item quando il numero di sequenza raggiunge il valore massimo possibile, il numero di sequenza ricomincia dal valore minimo;
 \item pacchetto vecchio: il numero di sequenza è nella metà precedente a quello corrente;
 \item pacchetto nuovo: il numero di sequenza è nella metà successiva a quello corrente.
 \end{itemize}
 
\paragraph{Spazio lecca-lecca} La prima metà dello spazio è lineare, la seconda metà è circolare.

\section{Algoritmi adattativi}
\label{sez:algoritmi_adattativi}
%63-64
\subsection{Instradamento centralizzato}
Tutti i router sono collegati a un nucleo di controllo centralizzato chiamato \textbf{Routing Control Center} (RCC): ogni router dice al RCC quali sono i propri vicini, e il RCC usa queste informazioni per creare la mappa della rete, calcolare le tabelle di instradamento e comunicarle a tutti i router.

\paragraph{Vantaggi}
\begin{itemize}
 \item \ul{prestazioni}: i router non devono avere un'elevata capacità di calcolo, concentrata tutta su un singolo apparato;
 \item \ul{debug}: l'amministratore di rete può ottenere la mappa dell'intera rete da un singolo apparato per verificarne la correttezza;
 \item \ul{manutenzione}: l'intelligenza è concentrata nel centro di controllo $\Rightarrow$ per aggiornare l'algoritmo basta aggiornare il centro di controllo.
\end{itemize}

\paragraph{Svantaggi}
\begin{itemize}
 \item \ul{tolleranza ai guasti}: il centro di controllo è un singolo punto di guasto $\Rightarrow$ un malfunzionamento del centro di controllo impatta su tutta la rete;
 \item \ul{scalabilità}: più router compongono la rete, più il lavoro per il centro di controllo aumenta $\Rightarrow$ non è adatto per reti ampie come Internet.
\end{itemize}

\paragraph{Applicazione} Principi simili sono utilizzati nelle reti telefoniche.

\subsection{Instradamento isolato}
%65
Non c'è alcun centro di controllo, ma tutti i nodi sono paritetici: ogni nodo decide i percorsi autonomamente senza scambiare informazioni con gli altri router.

\paragraph{Vantaggi e svantaggi} Sono praticamente gli opposti di quelli dell'instradamento centralizzato.

%67-68
\subsubsection{Backward learning} Ogni nodo apprende informazioni sulla rete in base all'indirizzo sorgente dei pacchetti:\footnote{Si rimanda alla sezione \textit{Transparent bridge} nel capitolo \textit{Repeater e bridge} negli appunti di ``Progetto di reti locali''.}
\begin{itemize}
 \item funziona bene solo con nodi ``loquaci'';
 \item non è facile capire di dover passare a un percorso alternativo quando il percorso migliore diventa non più disponibile;
 \item non è facile rilevare le destinazioni diventate irraggiungibili $\Rightarrow$ è richiesto un timer speciale per eliminare le entry vecchie.
\end{itemize}

\subsection{Instradamento distribuito}
%69
L'instradamento distribuito utilizza un modello ``paritetico'': prende i vantaggi dell'instradamento centralizzato e quelli dell'instradamento isolato:
\begin{itemize}
 \item instradamento centralizzato: i router partecipano nello scambio di informazioni riguardanti la connettività;
 \item instradamento isolato: i router sono equivalenti e non c'è un router ``migliore''.
\end{itemize}

%70
\paragraph{Applicazioni} I moderni protocolli di instradamento utilizzano due principali algoritmi di instradamento distribuito:
\begin{itemize}
 \item Distance Vector: ogni nodo dice a tutti i vicini che cosa sa della rete (capitolo~\ref{cap:Distance_Vector});
 \item Link State: ogni nodo dice a tutta la rete che cosa sa dei propri vicini (capitolo~\ref{cap:Link_State}).
\end{itemize}

\subsubsection{Confronto}
%148
\paragraph{Vicini}
\begin{itemize}
 \item LS: ha bisogno del protocollo di ``hello'';
 \item DV: conosce i suoi vicini attraverso il DV stesso.
\end{itemize}

\paragraph{Tabella di instradamento} DV e LS creano la stessa tabella di instradamento, solo calcolata in modi differenti e con differente durata (e comportamento) del transitorio:
\begin{itemize}
 \item LS: i router cooperano per mantenere la mappa della rete aggiornata, quindi ognuno di essi calcola il proprio albero ricoprente: ogni router conosce la topologia della rete, e conosce il percorso preciso per raggiungere una destinazione;
 \item DV: i router cooperano per calcolare la tabella di instradamento: ogni router conosce solo i suoi vicini, e si fida di loro per determinare il percorso verso la destinazione.
\end{itemize}

%149
\paragraph{Semplicità}
\begin{itemize}
 \item DV: singolo algoritmo facile da implementare;
 \item LS: incorpora molti componenti diversi.
\end{itemize}

\paragraph{Debug} Migliore in LS: ogni nodo ha la mappa della rete.

%150
\paragraph{Consumo di memoria \textmd{(in ogni nodo)}} Possono essere considerati equivalenti:
\begin{itemize}
 \item LS: ognuno degli $N$ LS ha $A$ adiacenze (Dijkstra: $\sim N\cdot A$);
 \item DV: ognuno degli $A$ DV ha $N$ destinazioni (Bellman-Ford: $\sim A\cdot N$).
\end{itemize}

\paragraph{Traffico} Migliore in LS: i pacchetti Neighbor Greeting sono molto più piccoli dei DV.

\paragraph{Convergenza} Migliore in LS: il rilevamento dei guasti è più rapido perché è basato sui pacchetti Neighbor Greeting inviati ad alta frequenza.