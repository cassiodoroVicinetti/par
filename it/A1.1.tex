\chapter{Inoltro e instradamento}
\label{cap:inoltro_e_instradamento}
%7-9
L'\textbf{instradamento} è il processo che determina il percorso ``migliore'' per un pacchetto e lo invia in uscita verso la destinazione:
\begin{itemize}
  \item \textbf{algoritmo di instradamento}: è responsabile di decidere i percorsi da prendere per i pacchetti in arrivo:
  \begin{enumerate}
   \item determina le destinazioni raggiungibili da ogni nodo;
   \item calcola i percorsi migliori (secondo certi criteri) in maniera cooperativa con gli altri nodi;
   \item memorizza delle informazioni locali in ogni nodo;
  \end{enumerate}

  \item \textbf{algoritmo di inoltro}: è responsabile di prendere il percorso deciso per ogni pacchetto in arrivo:
  \begin{enumerate}
   \item effettua una ricerca nelle informazioni locali calcolate e memorizzate dall'algoritmo di instradamento;
   \item invia il pacchetto in uscita lungo il percorso migliore.
   \end{enumerate}
\end{itemize}

%177
I protocolli di instradamento si differenziano in due classi:
\begin{itemize}
 \item \textbf{Interior Gateway Protocol} (IGP): comprende i protocolli utilizzati nell'\textbf{instradamento intra-dominio} (ad es. RIP, IGRP, OSPF) per propagare le informazioni di instradamento all'interno di un Autonomous System\footnote{Un Autonomous System (AS) è generalmente la rete sotto il controllo di un ISP (si rimanda alla sezione~\ref{sez:AS}).};
 \item \textbf{Exterior Gateway Protocol} (EGP): comprende i protocolli utilizzati nell'\textbf{instradamento inter-dominio} (ad es. BGP) per propagare le informazioni di instradamento fra Autonomous System (si rimanda alla sezione~\ref{sez:classe_egp}).
\end{itemize}

%10
Secondo il modello OSI, l'instradamento è una funzionalità propria del livello rete, ma può essere implementato a livelli diversi:
\begin{itemize}
 \item l'instradamento è implementato interamente a \ul{livello rete} da protocolli come IP, X.25 e OSI/Decnet;
 \item alcuni degli algoritmi di instradamento sono implementati a \ul{livello data-link} da protocolli come Frame Relay e ATM e dai bridge nelle LAN commutate.
\end{itemize}

%11
I router moderni implementano due tabelle:
\begin{itemize}
 \item \textbf{Routing Information Base} (RIB): è la classica tabella di instradamento che elenca tutte le destinazioni raggiungibili nella rete;
 \item \textbf{Forwarding Information Base} (FIB): è una tabella di instradamento ottimizzata per velocizzare l'inoltro dei pacchetti:
 \begin{itemize}
  \item \ul{hardware dedicato}: le TCAM sono in grado di memorizzare bit i cui valori possibili sono 0, 1 e ``don't care'' $\Rightarrow$ la netmask è integrata nell'indirizzo di rete stesso: ogni bit nella parte aggregata ha valore ``don't care'';
  \item \ul{cache}: la FIB include solo gli ultimi indirizzi di destinazione utilizzati;
  \item \ul{informazioni aggiuntive}: porta di uscita, indirizzo MAC di destinazione.
 \end{itemize}
\end{itemize}

%13
\section{Algoritmi di inoltro}
%14
\subsection{Instradamento per indirizzo di rete}
\begin{itemize}
 \item Ogni nodo è identificato da un indirizzo di rete.
 \item Ogni pacchetto contiene l'indirizzo del nodo di destinazione.
 \item Ogni nodo contiene la lista degli indirizzi delle destinazioni raggiungibili con i relativi next hop.
\end{itemize}

Quando arriva un pacchetto, il nodo utilizza l'\textbf{indirizzo di destinazione} in esso contenuto come ``chiave'' nella tabella di inoltro per trovare il next hop.

\paragraph{Vantaggio} È un algoritmo semplice ed efficiente perché è \ul{stateless}: l'inoltro di un pacchetto avviene indipendentemente dall'inoltro degli altri pacchetti, cioè il nodo una volta inoltrato un pacchetto se ne dimentica.

\paragraph{Svantaggio} Non è possibile la selezione di rotte diverse per la stessa destinazione in base al tipo di traffico per la \ul{qualità del servizio}.

\paragraph{Adozione} I protocolli connectionless (come l'IP) tipicamente utilizzano questo algoritmo di inoltro.

%15
\subsection{Tecnica dei ``percorsi colorati''}
\begin{itemize}
 \item Ogni percorso tra due nodi è identificato da un PathID (``colore'').
 \item Ogni pacchetto contiene un'etichetta che corrisponde al PathID del percorso da seguire.
 \item Ogni nodo contiene la lista dei PathID con le relative porte di uscita.
\end{itemize}

Quando arriva un pacchetto, il nodo utilizza l'\textbf{etichetta} in esso contenuto come ``chiave'' nella tabella di inoltro per trovare la porta di uscita.

\paragraph{Vantaggio} Sono possibili molti percorsi verso una stessa destinazione $\Rightarrow$ è possibile scegliere il percorso migliore in base al tipo di traffico per la \ul{qualità del servizio}.

\paragraph{Svantaggio} Il PathID è \ul{globale}:
\begin{itemize}
 \item la ``colorazione'' dei percorsi deve essere coerente su tutti i nodi nella rete;
 \item \ul{scalabilità}: il numero di percorsi possibili tra tutte le coppie di nodi nella rete è molto grande $\Rightarrow$ sono necessari molti bit per codificare ogni PathID, ed è difficile trovare un identificativo che non è ancora stato utilizzato.
\end{itemize}

\subsection{Label swapping}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.77\linewidth]{../pic/A1.1/16}
	\caption{Label swapping in una rete MPLS.}
\end{figure}

%18
\noindent
La tabella di inoltro di ogni nodo contiene la mappatura tra le etichette delle porte in ingresso e le etichette delle porte in uscita, includendo entry di tipo:\\
\centerline{\texttt{\textless porta in ingresso\textgreater\ \textless etichetta in ingresso\textgreater\ \textless porta in uscita\textgreater\ \textless etichetta in uscita\textgreater}}

Quando arriva un pacchetto, il nodo utilizza l'etichetta in esso contenuto e la porta in ingresso come ``chiave'' nella tabella di inoltro per trovare la porta di uscita, e rimpiazza l'etichetta corrente del pacchetto con l'etichetta in uscita.

\paragraph{Vantaggi}
\begin{itemize}
%17
 \item \ul{scalabilità}: il PathID del percorso da seguire non è globale, ma l'etichetta viene decisa localmente nodo per nodo, e deve essere coerente solo tra i nodi alle estremità del link:
 \begin{itemize}
  \item le etichette sono composte da meno bit perché devono codificare meno percorsi;
  \item ogni nodo deve conoscere solo le etichette dei percorsi che passano per esso $\Rightarrow$ la tabella di inoltro è più piccola;
 \end{itemize}
 \item \ul{efficienza}: il label swapping è veloce rispetto ad algoritmi di inoltro come il ``longest prefix matching'' di IP.
\end{itemize}

%19
\paragraph{Adozione}
Il label swapping è utilizzato da:
\begin{itemize}
 \item tecnologie di rete di derivazione telecomunicazionista (ad es. X.25, Frame Relay, ATM): il label swapping permette la qualità del servizio, una funzionalità considerata importante dal mondo degli operatori telefonici;
 %22
 \item MPLS: nel backbone i percorsi sono in numero abbastanza limitato e piuttosto stabili poiché sono creati non end-to-end ma nella nuvola MPLS, dove la topologia della rete cambia meno di frequente e il traffico è più regolare rispetto ai margini.
\end{itemize}
%\FloatBarrier

\subsubsection{Instaurazione del percorso}
Quando un host vuole generare e inviare il primo pacchetto verso una destinazione, come chiede l'instaurazione di un nuovo percorso e quale etichetta deve utilizzare?

%20
\paragraph{Instaurazione manuale}
I percorsi e le relative etichette sono impostati manualmente dall'amministratore della rete.

\subparagraph{Svantaggi}
\begin{itemize}
 \item rischio elevato di errori umani nella configurazione;
 \item nessun re-instradamento automatico in caso di guasti;
 \item non adatto per reti altamente dinamiche dove gli utenti chiedono frequentemente nuovi percorsi.
\end{itemize}

%21
\paragraph{Instaurazione su richiesta}
È richiesta una fase di segnalazione per l'instaurazione del percorso, ovvero per la preparazione delle etichette in ogni nodo, al termine della quale l'host apprende l'etichetta da utilizzare e può inviare il primo pacchetto verso la destinazione.

\subparagraph{Vantaggi} La \ul{qualità del servizio} è più semplice:
\begin{itemize}
 \item è possibile instaurare dei percorsi diversi in base alla sorgente che ne richiede l'instaurazione (ad es. il rettore può disporre di un percorso privilegiato rispetto al ricercatore);
 \item è possibile includere all'interno del pacchetto di segnalazione un'informazione che specifica quanta larghezza di banda riservare per il percorso.
\end{itemize}

\subparagraph{Svantaggi}
\begin{itemize}
 \item \ul{complessità}: la segnalazione è ottenuta attraverso un'altra tecnica di inoltro (ad es. l'instradamento per indirizzo di rete) su un circuito dedicato $\Rightarrow$ la complessità aumenta perché la rete deve ora gestire due tecniche di inoltro differenti;
 \item \ul{scalabilità}: se il percorso è lungo e il numero di nodi da attraversare è elevato, la fase di segnalazione può durare troppo a lungo, soprattutto se le sessioni di comunicazione sono abbastanza brevi come nel mondo delle reti.
\end{itemize}

%23
\subsection{Source routing}
L'host mittente scrive nel pacchetto stesso l'intero percorso che deve seguire per arrivare fino alla destinazione.

\paragraph{Vantaggio} I router interni alla rete sono estremamente semplici.

\paragraph{Svantaggio} L'host mittente deve conoscere la topologia della rete e deve interagire direttamente con gli altri host per poter calcolare i percorsi $\Rightarrow$ ciò viola il paradigma secondo cui gli utenti finali devono limitarsi ad inviare i pacchetti e alla rete è affidato il compito di inoltrare i pacchetti verso le destinazioni.

\paragraph{Adozione} IPv4 e IPv6 prevedono un'opzione che influenza il percorso dei pacchetti.

%24
\subsection{Confronto}
\paragraph{Instradamento per indirizzo di rete}
\begin{itemize}
 \item[\pro] \textbf{semplicità}: no instaurazione, no stato
 \item[\pro] \textbf{scalabilità (inoltro)}: no stato ``per sessione'' (stateless)
 \item[\con] \textbf{efficienza}: intestazione dei pacchetti grande
 \item[\con] \textbf{scalabilità (instradamento)}: tabella di instradamento molto grande
 \item[\con] \textbf{affidabilità}: difficile garantire il servizio
 \item[\con] \textbf{multipath}: non supporta più percorsi tra due entità
\end{itemize}

\paragraph{Label swapping}
\begin{itemize}
 \item[\pro] \textbf{scalabilità (instradamento)}: tabella di instradamento ridotta
 \item[\pro] \textbf{efficienza}: intestazione dei pacchetti piccola
 \item[\pro] \textbf{garanzia di servizio}: possibilità di garantire il servizio (prenotazione del percorso)
 \item[\pro] \textbf{multipath}: più percorsi permessi tra due entità
 \item[\con] \textbf{scalabilità (instaurazione)}: elaborazione dei pacchetti per l'instaurazione del percorso (critico con sessioni ``brevi'')
 \item[\con] \textbf{scalabilità (inoltro)}: stato ``per sessione'' (necessario per la qualità del servizio)
 \item[\con] \textbf{complessità}: instaurazione del percorso (processo di instaurazione del percorso, inoltro ad-hoc dei pacchetti per l'instaurazione del percorso)
\end{itemize}

\paragraph{Source routing}
\begin{itemize}
 \item[\pro] \textbf{efficienza (router)}: i sistemi intermedi sono estremamente semplici
 \item[\con] \textbf{efficienza (sorgenti)}: gli end system devono preoccuparsi di calcolare i percorsi
\end{itemize}