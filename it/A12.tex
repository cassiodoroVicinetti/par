\chapter{Instradamento multicast}
\paragraph{Protocolli di instradamento multicast}
\begin{itemize}
%23
\item \ul{DVMRP}: DV (TRPB, RPM), indipendente dal protocollo unicast, albero specifico della sorgente (sez.~\ref{sez:DVMRP});
\item \ul{MOSPF}: LS, opera sul protocollo OSPF, albero specifico della sorgente (sez.~\ref{sez:MOSPF});
%24
\item \ul{PIM-DM}: DV (RPM), indipendente dal protocollo unicast, albero specifico della sorgente;
\item \ul{PIM-SM}: CBT, indipendente dal protocollo unicast, ibrido tra albero condiviso e specifico della sorgente (sez.~\ref{sez:PIMSM});
\item \ul{BGMP} (Border Gateway Multicast Protocol): protocollo di instradamento multicast inter-dominio basato sul BGP.
\end{itemize}

\section{DVMRP}
\label{sez:DVMRP}
%31a-26
Il \textbf{Distance Vector Multicast Routing Protocol} (DVMRP) è stato il primo protocollo di instradamento multicast, ed era usato alle origini in \textbf{Multicast backbone} (Mbone), una rete virtuale nata nel 1992 in ambito IETF che si appoggia per la trasmissione sulla stessa struttura fisica di Internet per fornire agli utenti la possibilità di sfruttare il multicast per le comunicazioni multimediali.

%25
DVMRP è basato sull'algoritmo DV:
\begin{itemize}
\item versione 1: adotta il TRPB\footnote{Si veda la sezione~\ref{sez:TRPB}.};
\item versione 3: adotta il RPM\footnote{Si veda la sezione~\ref{sez:RPM}.}.
\end{itemize}

L'istanza di protocollo DVMRP è eseguita in parallelo all'istanza di protocollo unicast: DVMRP ignora le informazioni di instradamento degli altri protocolli, e calcola rotte che possono differire da quelle utilizzate per il traffico unicast.

%24a
Usa una metrica basata sul \ul{numero di hop}, vale a dire il numero di mrouter che parlano DVMRP. In DVMRP possono essere configurati manualmente dei \textbf{tunnel}: il cammino che collega due vicini DVMRP può includere dei router che non supportano DVMRP (o su cui DVMRP è disabilitato):
\begin{itemize}
%25a
\item \ul{local end-point}: specifica l'mrouter all'inizio del tunnel;
\item \ul{remote end-point}: specifica l'mrouter all'altra estremità del tunnel;
%26a
\item \ul{metric}: specifica la misura del costo del tunnel;
\item \ul{threshold}: specifica il valore minimo del TTL che un pacchetto deve avere per poter essere instradato attraverso il tunnel $\Rightarrow$ permette di definire la visibilità dei pacchetti: i pacchetti che non devono uscire dalla rete aziendale vengono generati con un TTL pari alla threshold.
\end{itemize}

\section{MOSPF}
\label{sez:MOSPF}
%27
Il \textbf{Multicast OSPF} (MOSPF) è un protocollo di instradamento multicast basato sull'algoritmo LS\footnote{Si veda la sezione~\ref{sez:Link_State_multicast_routing}.} che estende OSPF, permettendo ai singoli router di avere una conoscenza completa della topologia della rete e dei costi relativi ai singoli link.

%29
MOSPF è retrocompatibile con OSPF: i router MOSPF possono essere mischiati con router solo OSPF, anche se i pacchetti multicast sono inoltrati solo tra router MOSPF e i cammini scelti per i pacchetti multicast non passano per i router solo OSPF.

%30
MOSPF aggiunge l'\textbf{LSA di tipo 6}:
\begin{itemize}
\item i \ul{router interni} MOSPF producono LSA di tipo 6 per informare gli altri router nella loro area dei gruppi multicast attivi sulle loro reti;
\item gli \ul{ABR} MOSPF producono LSA di tipo 6 per informare gli altri router nell'area 0 dei gruppi multicast attivi sulle loro aree edge (anche con l'aggregazione).
\end{itemize}

\section{PIM}
%31
Il \textbf{Protocol Independent Multicast} (PIM) si serve direttamente delle tabelle contenenti le informazioni di instradamento indipendentemente dal protocollo unicast sottostante (DV o LS) che le ha costruite.

%32
Esiste in due versioni, tra loro incompatibili, che affrontano diverse problematiche relative alla spazialità dei ricevitori:
\begin{itemize}
%33
\item \textbf{Dense Mode} (DM):
\begin{itemize}
\item è adatto per \ul{reti piccole} (LAN/MAN) con molti \ul{ricevitori concentrati};
\item non è parco in termini di banda: i pacchetti possono venire inoltrati in zone non interessate al particolare gruppo multicast;
\item adotta l'\ul{algoritmo RPM} basato su DV (come DVMRPv3);
\item \ul{Implicit Join Protocol}: in mancanza di espliciti messaggi di Prune, i pacchetti vengono inoltrati in una certa rete;
\item è necessario generare \ul{messaggi di Prune} per interrompere il traffico multicast (come DVMRP);
\end{itemize}

%37
\item \textbf{Sparse Mode} (SM):
\begin{itemize}
\item è adatto per \ul{reti estese} (WAN) con pochi \ul{ricevitori sparsi};
\item limita il più possibile l'overhead di banda: non usa mai il flooding;
\item è un'evoluzione dell'\ul{algoritmo CBT}\footnote{Si veda la sezione~\ref{sez:core_based_tree}.}: inizia sempre da un albero condiviso, che diventa un albero specifico della sorgente quando vantaggioso;
\item \ul{Explicit Join Protocol}: in mancanza di espliciti messaggi di Join, i pacchetti non vengono inoltrati in una certa rete (le informazioni di instradamento però vanno a tutti i router, non sono quelli che riceveranno traffico);
\item è necessario generare \ul{messaggi di Join} per attivare il traffico multicast (come MOSPF).
\end{itemize}
\end{itemize}

\subsection{PIM-SM}
\label{sez:PIMSM}
%38
PIM-SM gestisce due tipi di alberi di distribuzione:
\begin{itemize}
%39
\item \textbf{RP-Tree} (RPT): è l'albero condiviso usato inizialmente per tutti i pacchetti del gruppo multicast, ma non è ottimizzato in base alla sorgente $\Rightarrow$ i primi pacchetti della trasmissione possono essere consegnati in breve tempo ai ricevitori senza dover aspettare il calcolo dell'albero:
\begin{itemize}
\item non vengono usati i percorsi minimi;
\item il traffico è centralizzato;
\item ad ogni gruppo multicast corrisponde un albero;
\end{itemize}

%40
\item \textbf{Shortest-Path Tree} (SPT): è l'albero specifico della sorgente costruito in un secondo momento se i router lo ritengono conveniente (non è obbligatorio):
\begin{itemize}
\item vengono usati i percorsi minimi;
\item il traffico è distribuito;
\item ad ogni coppia (gruppo, sorgente) corrisponde un albero.
\end{itemize}
\end{itemize}

PIM-SM definisce tre nodi speciali:
\begin{itemize}
\item \textbf{rendez-vous point} (RP): è il core router, eletto in base a una formula algoritmica a partire da un elenco chiamato RP-set, che si occupa di:
\begin{itemize}
\item ricevere i messaggi di Join da parte dei DR;
\item ricevere le richieste di registrazione da parte delle sorgenti;
\item ricevere e inviare ai DR i primi pacchetti di dati multicast trasmessi da una sorgente;
\end{itemize}

%50
\item \textbf{designated router} (DR): è il router, eletto in base al percorso più breve verso il RP (o all'indirizzo IP più alto in caso di parità), che si occupa di:
\begin{itemize}
\item ricevere le richieste di iscrizione da parte degli host in una certa LAN;
\item inviare il messaggio di Join al RP per unirsi al RPT;
\item inviare il messaggio di Join alla sorgente per unirsi al SPT;
\item inviare il messaggio di Join periodicamente per adattarsi ai cambiamenti dei gruppi;
\end{itemize}
%51
\item \textbf{bootstrap router} (BSR): è il router, eletto in base al costo amministrativo migliore (o all'indirizzo IP più alto in caso di parità), che si occupa di distribuire l'RP-set a tutto il dominio PIM-SM.
\end{itemize}

\subsubsection{Algoritmo}
\paragraph{RPT}
\begin{enumerate}
%41
\item \ul{iscrizione dei DR come ricevitori}: ogni DR invia un messaggio di Join al RP;
%42
\item \ul{registrazione della sorgente come trasmettitore}: la sorgente invia una richiesta di registrazione al RP;
%43
\item \ul{registrazione del RP come ricevitore}: il RP invia un messaggio di Join alla sorgente;
\item \ul{trasmissione lungo il RPT}: la sorgente invia in multicast i primi pacchetti di dati, inoltrati dai router intermedi fino al RP;
\item \ul{propagazione lungo il RPT}: il RP propaga in multicast i pacchetti di dati ricevuti, inoltrati dai router intermedi fino ai DR.
\end{enumerate}

\paragraph{SPT}
\begin{enumerate}
%44
\item \ul{iscrizione dei DR come ricevitori}: ogni DR invia un messaggio di Join alla sorgente;
%45
\item \ul{trasmissione lungo il SPT}: la sorgente invia in multicast i pacchetti di dati, inoltrati dai router intermedi a ogni DR (oltre che al RP);
%46
\item \ul{distacco dal RPT}: ogni DR invia un messaggio di Prune al RP;
%47-48
\item \ul{unione al SPT}: un altro DR invia un messaggio di Join al RP, poi invia un messaggio di Join alla sorgente.
\end{enumerate}