\chapter{Border Gateway Protocol}
\label{cap:bgp}
\textbf{Border Gateway Protocol} (BGP) is the inter-domain routing protocol commonly used in the Internet.

%7-8
\paragraph{Overview}
\begin{itemize}
 \item it uses the \ul{Path Vector} (PV) \ul{algorithm} to record the sequence of ASes along the path without the risk of routing loops (section~\ref{sez:bgp_path_vector});
 \item routers can \ul{aggregate} the received routing information before propagating them (sezione~\ref{sez:bgp_aggregare});
 \item it does not automatically discover the existence of new neighbor routers, but \ul{peering sessions} must be configured by hand (section~\ref{sez:bgp_peering});
 \item it exchanges routing updates by using \ul{reliable TCP connections} (section~\ref{sez:bgp_affidabile});
 \item it is an extensible protocol thanks to the \ul{Type-Length-Value} (TLV) \ul{format} of attributes (section~\ref{sez:bgp_tlv});
 \item it supports \ul{routing policies} (section~\ref{sez:bgp_politiche}).
\end{itemize}

\section{Routing information}
BGP exchanges inter-domain routing information about external routes, which are in form network address/\ul{prefix length} (instead of the netmask).

\subsection{Path Vector algorithm}
\label{sez:bgp_path_vector}
%12
As routing policies are defined based on paths, BGP can not be based on the DV algorithm because it is not enough to know their costs. BGP chooses to adopt the \textbf{Path Vector} (PV) \textbf{algorithm}\footnote{Please see section~\ref{sez:path_vector}.}: every AS constitutes a single node, identified by a number of 2 (or 4) bytes, and the additional piece of information is the \ul{list of crossed ASes}.

The PV algorithm is more stable because it is easy to detect loops:
\begin{itemize}
 \item if a router receives a PV which already includes its AS number, it discards the PV without propagating it, because a routing loop is going to start;
 \item otherwise, the router enters its own AS number into the PV and then it propagates it to its neighbors.
\end{itemize}

%13
BGP does not support an explicit cost metric: the cost associated to each route is simply equal to the \ul{number of crossed ASes} included in the list $\Rightarrow$ the least-cost route may not be the actually optimal one:
\begin{itemize}
 \item ASes may have different requirements, hence they may adopt different metrics one from each other (e.g. bandwidth, transmission delay) $\Rightarrow$ it is difficult to compute coherent costs for all ASes;
 \item the announced cost may not match the actual network topology because an ISP may want to hide from a competitor the actual information about its own network for economic reasons.\footnote{Please see section~\ref{sez:interdominio_amministrativi}.}
\end{itemize}

\subsection{Route aggregation}
\label{sez:bgp_aggregare}
When a border router propagates information about received routes, it can be manually configured to include \textbf{aggregate routes} into its advertisement messages to reduce their size: two routes can be aggregated in a route with the common part of their network prefixes.

However not all the routes which has been collapsed into an aggregate route can have the same sequence of crossed ASes, but there could be a more specific route following another path:
\begin{itemize}
 %33
 \item \textbf{overlapping route}: also the specific route, with its different list of crossed ASes, is announced along with the aggregate route $\Rightarrow$ information is complete, and the `longest prefix matching' algorithm will select the more specific route in the routing table;
 %34
 \item \textbf{not precise route}: only the aggregate route is announced $\Rightarrow$ information is approximate, because the list of crossed ASes does not match the path actually followed for all the destination addresses within that address range.
\end{itemize}

\section{Peering sessions}
\label{sez:bgp_peering}
%14
Two border routers exchanging BGP messages between themselves are called \textbf{peers}, and the TCP-based session is called \textbf{peering session}.

A key difference compared to other routing protocols is the fact that peers are not able to discover each other automatically: \ul{manual configuration} by the network administrator is required, because peers may not be connected through a direct link but other routers, for which BGP updates are normal data packets to be forwarded to destination, may exist between them.

\subsection{TCP}
\label{sez:bgp_affidabile}
%10
Transmission of routing information is \ul{reliable} because two peers establish a peering session by setting up a \textbf{TCP connection} through which all BGP messages are exchanged:
\begin{itemize}
 \item existing components are reused instead of redefining yet another protocol-specific mechanism;
 \item BGP does not need to deal directly with retransmissions, lost messages, etc.
\end{itemize}

%9
Using TCP as transport protocol avoids to periodically send updates: an update is sent only \ul{when needed}, including just routes which have changed, and it is sent again only if the message went lost $\Rightarrow$ the bandwidth consumed to send routes is reduced.

%42
Since advertisements are not periodic, routes \ul{never expire} $\Rightarrow$ it is required to explicitly inform that a previously announced route has become unreachable, to \textbf{withdraw} routes when they are no longer valid (analogously to route poisoning in the DV algorithm\footnote{Please see section~\ref{sez:route_poisoning}.}).

%11
However TCP deprives the application of the control on \ul{timings}, because control packets may be delayed by TCP mechanisms themselves: in case of congestion TCP reduces the transmission bit rate preventing their timely transmission $\Rightarrow$ quality of service can be configured on internal routers within the AS so as to give priority to BGP packets, considering that they are service packets to allow the network operation.

%10
TCP does not provide any information about whether the remote peer is still reachable $\Rightarrow$ an \textbf{explicit keepalive mechanism} managed by BGP itself is required. Also keepalive messages rely on TCP mechanisms $\Rightarrow$ reactivity to a peer disappearance or a link fault is limited, but it is still acceptable considering that these events are rare (e.g. links between border routers are strongly redundant).

\subsection{I-BGP e E-BGP}
%15-54
When two border routers set up a peering session between themselves, each one communicates, through an OPEN message, its AS number to the other party to determine the type of sub-protocol:
\begin{itemize}
 \item \textbf{Exterior BGP} (E-BGP): peers are border routers belonging to two different ASes, usually connected by a direct link;
 \item \textbf{Interior BGP} (I-BGP): peers are border routers belonging to the same AS, usually connected through a series of internal routers.
\end{itemize}

%19
The processing of BGP messages and the routes announces on peering sessions may be different according to which ASes the peers are belonging to:
\begin{itemize}
 %22
 \item E-BGP: when a border router propagates a PV to an E-BGP peer, it prepends the current AS number to each list of crossed ASes:
 \begin{itemize}
  \item \ul{external routes}: they are propagated to other E-BGP peers, but peers whose AS is on the best path toward those destinations;
  \item \ul{internal routes}: they are propagated to other E-BGP peers;
 \end{itemize}
 
 %23
 \item I-BGP: when a border router propagates a PV to an I-BGP peer, it transmits the list \ul{as is} because the AS number remains unchanged:
 \begin{itemize}
  \item \ul{external routes}: they are propagated to other I-BGP peers according to various ways;
  %28
  \item \ul{internal routes}: they are never propagated to other I-BGP peers, but every border router learns them from an independent redistribution process.
 \end{itemize}
\end{itemize}

%18
I-BGP sessions are used to exchange external routes:
\begin{itemize}
 \item independently of \ul{routes} exchanged by the interior protocol: the direct connection between peers avoids to bother the IGP protocol when the variation of an external route does not require the re-computation of internal routes $\Rightarrow$ no transients, less processing;
 \item independently of the \ul{interior protocol}: if border routers when learning external routes from E-BGP limited to redistribute them to the IGP protocol, letting the latter redistributed them naturally to other border routers, some important information needed by BGP would go lost $\Rightarrow$ specific BGP messages, called UPDATES, are required, including this information in their attributes.
\end{itemize}

\subsubsection{IGP-BGP synchronization}
\begin{figure}
	%30
	\centering
	\includegraphics[scale=1.48]{../pic/A9/30}
	\caption{Example of IGP-BGP synchronization.}
	\label{fig:sincronizzazione_igp_bgp}
\end{figure}

%29
\noindent
BGP routers in a transit AS learn external destinations by other BGP routers via I-BGP, but packet forwarding across the AS (toward the egress BGP router) relies on internal routers, whose routing tables are filled by the IGP protocol and not by BGP $\Rightarrow$ only after they have been announced also by the IGP protocol, external destinations can be announced to border routers in other ASes.

%31
In the example in figure~\ref{fig:sincronizzazione_igp_bgp}, router R4 learns destination D via E-BGP and announces it to router R3 via I-BGP, but R3 can not in turn announce it to router R5 via E-BGP until the destination is redistributed from the IGP protocol to R3, otherwise if R5 tried to send a packet toward D, R3 would forward it inside the AS where internal routers would discard it.

%32
It might be good to disable synchronization when:
\begin{itemize}
 \item the AS is not a transit one;
 \item all routers in the AS use BGP.
\end{itemize}
\FloatBarrier

\subsection{Routing loops}
\begin{figure}
	\centering
	\includegraphics[scale=1.48]{../pic/A9/15}
	\caption{Example of routing loop.}
	\label{fig:bgp_routing_loop}
\end{figure}

\noindent
Lack of information about crossed border routers when they belong to the same AS can be the cause of \ul{routing loops}: a border router can no longer rely on the list of crossed ASes to detect paths going twice through the same border router.

In the example in figure~\ref{fig:bgp_routing_loop}, a loop is created in advertising:
\begin{enumerate}
 \item router R4 learns the external route toward destination D;
 \item R4 propagates D to peer R3;
 \item R3 propagates D to peer R2;
 \item R2 propagates D to peer R4, which is the router which first learnt and announced D.
\end{enumerate}

Thus a situation is created similar to the one which was triggering count to infinities in the Distance Vector algorithm\footnote{Please see section~\ref{sez:count_to_infinity}.}: R4 can not determine whether R2 can reach D by crossing R4 itself or an actually alternative path exists $\Rightarrow$ if a link fault occurs between R4 and the border router of the AS where D is located, R4 will believe that D is still reachable through R2.
\FloatBarrier

External routes can be announced to I-BGP peers in various ways: full mesh, route reflector, AS confederation.

%16
\subsubsection{Full mesh}
\begin{figure}
	\centering
	\includegraphics[scale=1.48]{../pic/A9/16a}
	\caption{Example of full mesh.}
	\label{fig:maglia_completa_bgp}
\end{figure}

\noindent
Each border router has an I-BGP peering session with every other border router of its AS.

When a border router learns an external route from E-BGP, it propagates it to all other ones, which in turn propagate it to everyone, and so on.

In presence of more than 2 border routers, routing loops can form due to loops in advertising, like in figure~\ref{fig:maglia_completa_bgp}.

This solution is not flexible because all peering sessions must configured by hand, although peering sessions do not change much over time because border routers are quite fixed and fault-tolerant.
\FloatBarrier

\subsubsection{Route reflector}
\begin{figure}
	\centering
	\includegraphics[scale=1.48]{../pic/A9/16b}
	\caption{Example of route reflector.}
\end{figure}

\noindent
One of the border routers is elected as the \textbf{route reflector} (RR), and all other border routers set up peering sessions only with it without creating closed paths.

When a border router learns an external route from E-BGP, it propagates it only to RR, which is in charge of in turn propagate the route to other border routes avoiding routing loops.

Route reflector constitutes a single point of failure.
\FloatBarrier

%17
\subsubsection{AS confederation}
\begin{figure}
	\centering
	\includegraphics[scale=1.48]{../pic/A9/17}
	\caption{Example of AS confederation.}
\end{figure}

\noindent
Border routers have a full mesh of I-BGP peering sessions (as in the first way), but the AS is split into mini-ASes, each one with a private AS number, and when a border router propagates the PV it prepends in the list its private AS number.

When an advertisement arrives, the border router can look whether its own private AS number is already in the list, so as to discard the packet if a routing loop is detected.
\FloatBarrier

\section{Path attributes}
\label{sez:bgp_tlv}
%35
BGP information about announced routes (e.g. the list of crossed ASes) is included in \textbf{path attributes} inside UPDATE packets.

All attributes are encoded into the \textbf{Type-Length-Value} (TLV) \textbf{format} $\Rightarrow$ BGP is an \textbf{extensible protocol}: extension RFCs can define new attributes without breaking compatibility with the existing world and, if the router does not support that attribute (unrecognized type code), it can ignore it and skip to the next one (thanks to the information about its length).

%36
A BGP attribute can be:
\begin{itemize}
 \item \textbf{well-known}: it must be understood by all implementations, and can never be skipped (section~\ref{sez:well-known_bgp}):
 \begin{itemize}
 \item \ul{mandatory}: it must be present in all messages;
 \item \ul{discretionary}: it may not be present in all messages;
 \end{itemize}
 
 \item \textbf{optional}: it may not be understood by all implementations, and can be skipped if not supported (section~\ref{sez:optional_bgp}):
 \begin{itemize}
  \item \ul{transitive}: if the router does not support the attribute, it must propagate it anyhow setting flag P;
  \item \ul{non-transitive}: if the router does not support the attribute, it must not propagate it.
 \end{itemize}
\end{itemize}

Each attribute has the following TLV format:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|c|c|c|}
  \multicolumn{1}{r}{\footnotesize{1}} & \multicolumn{1}{r}{\footnotesize{2}} & \multicolumn{1}{r}{\footnotesize{3}} & \multicolumn{1}{r}{\footnotesize{4}} & \multicolumn{1}{r}{\footnotesize{8}} & \multicolumn{1}{r}{\footnotesize{16}} & \multicolumn{1}{r}{\footnotesize{24/32}} & \multicolumn{1}{r}{} \\
  \hline
  O & T & P & E & 0 & Type & Length & Value \\
  \hline
 \end{tabular}}
 \caption{Type-Length-Value (TLV) format of a BGP attribute.}
\end{table}
\noindent
where the fields are:
\begin{itemize}
 %37
 \item \ul{Optional} (O) flag (1 bit): it specifies if the attribute is optional or well-known;
 \item \ul{Transitive} (T) flag (1 bit): it specifies if the attribute is transitive or non-transitive;
 %38
 \item \ul{Partial} (P) flag (1 bit): it specifies if at least a router along the path has encountered an optional transitive attributed which did not support;
 \item \ul{Extended Length} (E) flag (1 bit): it specifies if the `Length' field is encoded by one of two bytes;
 \item \ul{Type} field (1 byte): it includes the type code identifying the attribute $\Rightarrow$ a router can determine if it supports that attribute without having to parse its value;
 \item \ul{Length} field (1 o 2 bytes): it includes the length of the attribute value $\Rightarrow$ a router can skip an unsupported attribute and skip to the next one by advancing by the number of bytes indicated by this field;
 \item \ul{Value} field (variable length): it includes the attribute value.
\end{itemize}

\subsection{Well-known attributes}
\label{sez:well-known_bgp}
\begin{itemize}
 %39
 %http://www.cisco.com/c/en/us/support/docs/ip/border-gateway-protocol-bgp/5816-bgpfaq-5816.html#difference
 \item \textbf{ORIGIN} attribute (type 1, mandatory): it defines the origin of the path information:
 \begin{itemize}
  \item IGP: the route was manually specified as a static route (\texttt{bgp network} command);
  \item EGP: the route was learnt by the EGP protocol\footnote{The protocol, not the protocol class, is meant here (please see section~\ref{sez:protocolli_egp}).};
  \item INCOMPLETE: the route was learnt from an IGP protocol through a redistribution process (\texttt{bgp redistribute} command);
 \end{itemize}
 
 %40
 \item \textbf{AS\_PATH} attribute (type 2, mandatory): it contains the list of crossed ASes split into path segments:
 \begin{itemize}
  \item AS\_ SEQUENCE: AS numbers in the path segment are in traversal order, and if the first segment in the packet is in order a new AS number has to be added at the beginning of that segment;
  \item AS\_ SET: AS numbers in the path segment are not in traversal order, and if the first segment in the packet is not in order a new in-order segment, where the new AS number has to be inserted, has to be added before that segment;
 \end{itemize}
 
 %41
 \item \textbf{NEXT\_HOP} attribute (type 3, mandatory): it optimizes routing when multiple routers belong to the same LAN but to two different ASes, and therefore traffic from an AS to another one would always cross the border router $\Rightarrow$ the border router can announce to send traffic to the next hop router in the other AS:
 \begin{figure}[H]
	\centering
	\includegraphics[scale=0.97125]{../pic/A9/41}
	\caption{Border router B teaches border router A to use router C as its next hop for destination D.}
 \end{figure}
 
 %43
 \item \textbf{LOCAL\_PREF} attribute (type 5, discretionary): in I-BGP when the external destination is reachable across two egress border routers, the route with highest LOCAL\_PREF is preferred;
 
 %44
 %http://netcerts.net/bgp-path-attributes-and-the-decision-process/
 \item \textbf{ATOMIC\_AGGREGATE} attribute (type 6, discretionary): it indicates that the announced route is a not precise aggregate route.
\end{itemize}

\subsection{Optional attributes}
\label{sez:optional_bgp}
\begin{itemize}
 %42
 \item \textbf{MULTI\_EXIT\_DISC} (MED) attribute (type 4, non-transitive): in E-BGP when two ASes are connected via multiple links, the link with lowest MED is preferred and links with higher MEDs are considered as backup links;
 
 %44
 \item \textbf{AGGREGATOR} attribute (type 7, transitive): it contains the AS number and the IP address of the router which generated the not precise route;
 
 %37
 %https://tools.ietf.org/html/rfc1997
 \item \textbf{COMMUNITIES} attribute (type 8, transitive): it indicates which group of peers this route has to be announced to (e.g. to the entire Internet, only within the current AS, to no one);
 
 %42
 %https://tools.ietf.org/html/rfc4760#section-4
 \item \textbf{MP\_UNREACH\_NLRI} attribute (type 15, non-transitive): it informs that a previously announced route has become unreachable (routes never expire).
\end{itemize}

%45
\section{Decision process}
\label{sez:bgp_politiche}
\begin{figure}
 %49
 %http://icawww1.epfl.ch/cn2/0910/slides/7.bgp.pdf
 \centering
 \includegraphics[scale=0.97125]{../pic/A9/49}
 \caption{The decision process selects routes from the input Adj-RIBs-Ins and writes them into the output Loc-RIB and Adj-RIBs-Outs.}
\end{figure}

\noindent
The \textbf{decision process} running on every border router is responsible for:
\begin{itemize}
 %51
 \item selecting which routes are advertised to other BGP peers;
 \item selecting which routes are used locally by the border router;
 \item aggregating routes to reduce information.
\end{itemize}
 
%46
Databases which BGP has to deal with are:
\begin{itemize}
 \item \textbf{Routing Information Base} (RIB): it consists of three distinct parts:
 \begin{itemize}
  \item \textbf{Adjacent RIB Incoming} (Adj-RIB-In): it contains all the routes learnt from the advertisements received from a certain peer;
  \item \textbf{Local RIB} (Loc-RIB): it contains the routes selected by the decision process with their degree of preference;
  \item \textbf{Adjacent RIB Outgoing} (Adj-RIB-Out): it contains the routes which will be propagated in advertisements to a certain peer;
 \end{itemize}
 \item \textbf{Policy Information Base} (PIB): it contains the routing policies defined by manual configuration;
 \item \textbf{routing table}: it contains the routes used by the packet forwarding process.
\end{itemize}

%50
Very complex \ul{routing policies} can be imposed to affect the decision process:
\begin{enumerate}
 \item a certain function returning, by applying the policies defined on attributes, the \textbf{degree of preference} for that route is applied to each route in the Adj-RIBs-In.\\
 Policies are defined only based on the attributes of the current route: the computation of the degree of preference is never affected by the existence, the non-existence, or the attributes of other routes;
 \item for each destination, the route with the greatest degree of preference is selected and inserted into the Loc-RIB;
 \item other policies determine which routes are selected from the Loc-RIB to be inserted into the Adj-RIBs-Out.
\end{enumerate}
%\FloatBarrier