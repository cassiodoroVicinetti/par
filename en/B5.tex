\chapter{Introduction to Software-Defined Networks}
\label{cap:SDN}
\begin{figure}
 %3
 \centering
 \includegraphics[scale=1.1]{../pic/B5/6.pdf}
 \caption{SDN components.}
\end{figure}

%3
\noindent
Internet is still the one which was defined 30 years ago: a very efficient pipe which transports bits at high speed, with almost the same protocols and the same philosophy.

%4
Network devices are monolithic: every router contains, besides specialized hardware for packet forwarding, its own operating system and its own applications. This infrastructure is closed to innovations: software components can not be installed by the customer but are set by the hardware manufacturer, which is not interested in innovating if it is the market leader (i.e. Cisco).

%5
\textbf{Software-Defined Network}s (SDN) introduce the possibility to program the network, and are based on three pillars:
\begin{itemize}
\item \ul{separation of control and forwarding features}: software, the smart component, is split from hardware;
\item \ul{centralization of control}: the whole network is coordinated by a \textbf{controller}, made up of a network operating system and user-defined network applications (e.g. routing protocol, load balancer, firewall);
\item \ul{well-defined interfaces}:
\begin{itemize}
\item \textbf{northbound}: the network operating system exposes APIs to network applications;
\item \textbf{southbound}: the network operating system drives network nodes, made up of simple packet forwarding hardware.
\end{itemize}
\end{itemize}

%8-9
The \textbf{network operating system} is a software layer that offers a global, abstract view of the network to upper applications. %
%7-11
The view from `above' the network enables for example \textbf{traffic engineering}: decisions are taken by the centralized logic of the load balancer and are therefore coherent for all network nodes:
\begin{itemize}
\item \ul{proactive mode}: before the device starts forwarding packets, the controller fills a priori the forwarding table with all the rules needed for all sessions;
\item \ul{reactive mode}: when the first packet in a session arrives, the device sends it to the controller, which takes a decision and notifies to the device the rule needed to forward packets in that session.
\end{itemize}

%10
A \textbf{network slicing} layer can show to software even a network topology other than the actual physical infrastructure: it can be configured so as to show to every system operating instance different virtual topologies (e.g. a subset of actual links) $\Rightarrow$ traffic policies of a certain company affect only the network portion belonging to the company.

%13
\paragraph{Issues}
\begin{itemize}
\item \ul{controller}: it may constitute a single point of failure and a bottleneck;
\item \ul{versatility}: a firewall needs to inspect all packets, not only the first packet in the session $\Rightarrow$ a lot of traffic would be generated between the device and the controller;
\item \ul{scalability}: forwarding hardware can not be too simple in order to get high performance;
\item \ul{economy}: hardware simplification goes against economic interests of major network vendors.
\end{itemize}
\FloatBarrier

\section{OpenFlow}
%15
\textbf{OpenFlow}, introduced around 2008, is an implementation of the southbound interface.

It can be deployed in various ways:
\begin{itemize}
\item \ul{rules}: typically they are flow-based, that is defined based on the (MAC addresses, IP addresses, TCP ports) tuple;
\item \ul{controller}: typically it is physically centralized, but it could even be physically distributed (even though still logically centralized);
\item \ul{mode}: typically it is reactive, but nothing prevents from using the proactive mode.
\end{itemize}

%16-17
One or more \textbf{actions} are associated to each rule, for example:
\begin{itemize}
\item forward packet to port(s);
\item encapsulate and forward to controller;
\item drop packet;
\item send to normal processing pipeline (i.e. the classical routing table);
\item modify fields (e.g. NAT: change addresses and ports).
\end{itemize}

\paragraph{OpenFlow 1.3} It introduced some interesting features:
\begin{itemize}
\item the forwarding table is split into various subtables (e.g. firewall, routing, etc.) and every application accesses its subtable $\Rightarrow$ each packet is matched multiple times across the tables in sequence;
\item \textbf{virtual switch} (vSwitch, e.g. Open vSwitch): instead of being implemented in hardware, OpenFlow is run on a switch emulated by a software process $\Rightarrow$ a GRE logical tunnel can be created between two vSwitches on two different servers across a traditional switch network.\footnote{Please refer to section~\ref{sez:NFV}.}
\end{itemize}

%18
\paragraph{Issues}
\begin{itemize}
%30
\item \ul{data plane}: it only deals with packet forwarding $\Rightarrow$ it is suitable for environments (e.g. datacenters) where packet forwarding is a preponderant aspect with respect to the data plane, but it does not appear to be proper for an ISP network;
\item \ul{usefulness}: the southbound interface is less interesting than the northbound one: it is used by network operating system developers, not by application developers;
\item \ul{hardware cost}: rules can be based on a large number of fields which make entries very wide $\Rightarrow$ needed TCAMs are expensive and heat a lot;
%33
\item \ul{flexibility}: as opposed to Open Networking Foundation (ONF) (VMware), OpenDaylight project (Cisco) prefers \textbf{Network Configuration Protocol} (NETCONF) which, instead of make rules explicit, does not know the semantics of values read or set $\Rightarrow$ it can be used by the SDN controller to configure some advanced features on devices, such as `backup routes' in case of faults detected to be critical over an ISP network.
\end{itemize}

\section{Data plane}
It is not only important to forward packets to the right direction, but also to offer data-plane-oriented \textbf{services} which process packets (e.g. firewall, NAT, network monitor).

%20
\subsection{Service Function Chaining without SDN}
\begin{figure}
 \centering
 \includegraphics[scale=1.1]{../pic/B5/20.pdf}
 \caption{Service Function Chaining (SFC) without SDN.}
\end{figure}

\noindent
Nowadays services can be added to access routers (BNG), as well as by service cards\footnote{Please see section~\ref{sez:service_card}.}, by connecting boxes called appliances: an \textbf{appliance} is a separate and discrete hardware device with integrated software (firmware) dedicated to provide a specific service. Appliances are connected in a cascade by physical wires forming a static \textbf{service chain}, and each packet has to be processed throughout services before being able to exit the device.

\paragraph{Disadvantages}
\begin{itemize}
\item \ul{agility} in provisioning new services: the appliance should be physically connected to the device;
\item \ul{flexibility}: in order to connect a new appliance the chain needs to temporarily be broken stopping the network service;
\item \ul{reliability}: a faulty appliance breaks the chain stopping the network service;
\item \ul{optimization}: each appliance has a fixed amount of resources available, and during work peaks it can not exploit resources possibly left free in that moment by another appliance.
\end{itemize}
\FloatBarrier

\subsection{Service Function Chaining with SDN}
\begin{figure}
 \centering
 \includegraphics[scale=1.1]{../pic/B5/21.pdf}
 \caption{Service Function Chaining (SFC) with SDN.}
\end{figure}

%21
\noindent
Every appliance is connected to an output port and an input port of an OpenFlow switch, and traffic flows cross a service chain dynamically decided through OpenFlow rules defining paths from a switch port to another.

%22
\paragraph{Advantages}
\begin{itemize}
\item \ul{flexibility}: adding a new appliance requires to change on the fly the OpenFlow rule by the SDN controller without stopping the network service;
\item \ul{reliability}: an on-the-fly change to the OpenFlow rule by the SDN controller is enough to restore the network service;
\item \ul{business}: paths can be differentiated based on the customer (company) $\Rightarrow$ traffic goes only across services which the customer has bought.
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
\item \ul{agility} in provisioning new services: the appliance should be physically connected to the device;
\item \ul{optimization}: each appliance has a fixed amount of resources available, and during work peaks it can not exploit resources possibly left free in that moment by another appliance;
\item \ul{backward compatibility}: devices should be replaced with switch supporting OpenFlow.
\end{itemize}
\FloatBarrier

\subsection{Network Function Virtualization}
\label{sez:NFV}
\begin{figure}
 \centering
 \includegraphics[scale=1.1]{../pic/B5/23.pdf}
 \caption{Network Function Virtualization (NFV).}
\end{figure}

%23
\noindent
Services are implemented in a purely-software process: the switch is connected to OpenFlow vSwitches emulated on multiple remote servers, and each server has a hypervisor able to run \textbf{virtual machines} (VM) inside which services are running.

%25-26
\paragraph{Scaling}
Performance of a service can be enhanced in three ways:
\begin{itemize}
\item \textbf{scale up}: the VM is assigned more hardware resources $\Rightarrow$ this may not be enough if the service is not able to properly \ul{exploit the available hardware} (e.g. a single-thread program does not benefit much from a multi-thread environment);
\item \textbf{scale out}: multiple VMs are running in parallel on a same physical server $\Rightarrow$ a \ul{load balancer} is needed to send traffic to the least-loaded VM, and VMs need \ul{synchronization};
\item \ul{multiple servers}: multiple VMs are running in parallel on multiple physical servers $\Rightarrow$ a further load balancer is needed to send traffic to the least-loaded server.
\end{itemize}

\paragraph{Advantages}
\begin{itemize}
\item \ul{agility} in provisioning new services: a new service can be dynamically enabled by downloading and starting its software image;
\item \ul{optimization}: server hardware resources are shared among VMs;
\item \ul{backward compatibility}: if the switch does not support OpenFlow, the GRE tunnel between vSwitches can be exploited without having to replace the device;
\item \ul{consolidation}: by night the number of VMs running in parallel can be reduced (\textbf{scale in}) and the assigned hardware resources can be decreased (\textbf{scale down}).
\end{itemize}

%24
\paragraph{Disadvantages}
\begin{itemize}
\item \ul{traffic}: the classical NFV model may require packets to travel from a server to another across the switch, clogging the network which servers are spread over;
\item \ul{efficiency}: servers have general-purpose CPUs, not dedicated hardware (e.g. line cards), and effective hardware-acceleration technologies are not currently available;
\item \ul{migration}: when the user moves, the VM instance should be moved to the closest server and should be started as soon as possible;
\item \ul{scalability}: the architecture is potentially very scalable, but suffers from synchronization and load balancing problems when multiple service instances are running in parallel.
\end{itemize}
\FloatBarrier

\section{OpenStack}
\begin{figure}
 %13
 \centering
 \includegraphics[scale=1.1]{../pic/B6/13.pdf}
 \caption{OpenStack system components.}
\end{figure}

%11
\noindent
\textbf{OpenStack}, introduced in 2010, is an open-source distributed operating system:
\begin{itemize}
\item Linux:
\begin{itemize}
\item it handles the \ul{single local} host which it is running on;
\item the \ul{process} is the execution unit;
\end{itemize}

\item OpenStack:
\begin{itemize}
\item it is run on a \ul{remote server}, called \textbf{controller node};
\item it handles \ul{multiple distributed} physical servers in the cloud, called \textbf{compute nodes};
\item the \ul{virtual machine} is the execution unit.
\end{itemize}
\end{itemize}

Each \textbf{compute node} includes the following components:
\begin{itemize}
\item \ul{traditional operating system}: it handles the local hardware on the physical server;
\item \ul{agent}: it receives commands from the controller node, for example to launch VMs;
\item \ul{vSwitch} (e.g. Open vSwitch): it connects the server to the network infrastructure.
\end{itemize}

One of the tasks of the \textbf{controller node} is to launch VMs on the currently least-loaded compute node.
%\FloatBarrier