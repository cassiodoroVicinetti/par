\chapter{Forwarding and routing}
\label{cap:inoltro_e_instradamento}
%7-9
\textbf{Routing} is the process which determines the `best' path for a packet and sends it out toward the destination:
\begin{itemize}
  \item \textbf{routing algorithm}: it is in charge of deciding the paths to take for incoming packets:
  \begin{enumerate}
   \item it determines the destinations reachable by each node;
   \item it computes the best paths (according to certain criteria) in a cooperative way with the other nodes;
   \item it stores local information in each node;
  \end{enumerate}

  \item \textbf{forwarding algorithm}: it is in charge of taking the path decided for each incoming packet:
  \begin{enumerate}
   \item it performs a lookup in the local information computed and stored by the routing algorithm;
   \item it sends the outgoing packet along the best path.
   \end{enumerate}
\end{itemize}

%177
Routing protocols differentiate into two classes:
\begin{itemize}
 \item \textbf{Interior Gateway Protocol} (IGP): it includes the protocols used in \textbf{intra-domain routing} (e.g. RIP, IGRP, OSPF) to propagate routing information inside an Autonomous System\footnote{An Autonomous System (AS) is generally the network under the control of an ISP (please refer to section~\ref{sez:AS}).};
 \item \textbf{Exterior Gateway Protocol} (EGP): it includes the protocols used in \textbf{inter-domain routing} (e.g. BGP) to propagate routing information between Autonomous Systems (please refer to section~\ref{sez:classe_egp}).
\end{itemize}

%10
According to the OSI model, routing is a feature proper to the network layer, but it can be implemented at different layers:
\begin{itemize}
 \item routing is implemented altogether at the \ul{network layer} by protocols such as IP, X.25 and OSI/Decnet;
 \item some of the routing algorithms are implemented at the \ul{data-link layer} by protocols such as Frame Relay and ATM and by bridges in switched LANs.
\end{itemize}

%11
Modern routers implement two tables:
\begin{itemize}
 \item \textbf{Routing Information Base} (RIB): it is the classical routing table listing all the destinations reachable within the network;
 \item \textbf{Forwarding Information Base} (FIB): it is a routing table optimized to speed up packet forwarding:
 \begin{itemize}
  \item \ul{dedicated hardware}: TCAMs are able to store bits whose possible values are 0, 1 and `don't care' $\Rightarrow$ the netmask is integrated in the network address itself: each bit in the aggregated part has value `don't care';
  \item \ul{cache}: the FIB only includes the last used destination addresses;
  \item \ul{additional information}: output port, destination MAC address.
 \end{itemize}
\end{itemize}

%13
\section{Forwarding algorithms}
%14
\subsection{Routing by network address}
\begin{itemize}
 \item Each node is identified by a network address.
 \item Each packet contains the address of the destination node.
 \item Each node contains the list of the reachable destination addresses with their corresponding next hops.
\end{itemize}

When a packet comes, the node uses the \textbf{destination address} included in it as the `key' in the forwarding table to find the next hop.

\paragraph{Advantage} It is a simple and efficient algorithm because it is \ul{stateless}: packet forwarding takes place regardless of the forwarding of other packets, that is the node once a packet is forwarded will forget about it.

\paragraph{Disadvantage} It is not possible to select different routes for the same destination based on the kind of traffic for \ul{quality of service}.

\paragraph{Adoption} Connectionless protocols (such as IP) typically use this forwarding algorithm.

%15
\subsection{`Coloured path' technique}
\begin{itemize}
 \item Each path between two nodes is identified by a PathID (`color').
 \item Each path contains a label corresponding to the PathID of the path to follow.
 \item Each node contains the lists of PathIDs with their corresponding output ports.
\end{itemize}

When a packet comes, the node uses the \textbf{label} included in it as the `key' in the forwarding table to find the output port.

\paragraph{Advantage} Several paths towards the same destination are possible $\Rightarrow$ it is possible to choose the best path based on the kind of traffic for \ul{quality of service}.

\paragraph{Disadvantage} The PathID is \ul{global}:
\begin{itemize}
 \item path `colouring' must be coherent on all the nodes over the network;
 \item \ul{scalability}: the number of possible paths between all the node pairs in the network is very big $\Rightarrow$ a lot of bits are needed to encode each PathID, and it is hard to find an identifier which has not been used yet.
\end{itemize}

\subsection{Label swapping}
\begin{figure}
	\centering
	\includegraphics[width=0.77\linewidth]{../pic/A1.1/16}
	\caption{Label swapping in an MPLS network.}
\end{figure}

%18
\noindent
The forwarding table in each node contains the mapping between the labels of the input ports and the labels of the output ports, including entries like:\\
\centerline{\texttt{\textless input port\textgreater\ \textless input label\textgreater\ \textless output port\textgreater\ \textless output label\textgreater}}

When a packet comes, the node uses the label included in it and the input port as the `key' in the forwarding table to find the output port, and it replaces the current label in the packet with the output label.

\paragraph{Advantages}
\begin{itemize}
%17
 \item \ul{scalability}: the PathID of the path to follow is not global, but the label is decided locally node by node, and it must be coherent only between the nodes to the link endpoints:
 \begin{itemize}
  \item labels are made up of less bits because they have to encode less paths;
  \item each node must know only the labels of the paths crossing it $\Rightarrow$ the forwarding table is smaller;
 \end{itemize}
 \item \ul{efficiency}: label swapping is fast with respect to forwarding algorithms such as `longest prefix matching' in IP.
\end{itemize}

%19
\paragraph{Adoption}
Label swapping is used by:
\begin{itemize}
 \item telecommunication-derived network technologies (e.g. X.25, Frame Relay, ATM): label swapping allows quality of service, a feature considered as important by the world of phone operators;
 %22
 \item MPLS: in the backbone paths are in a fairly limited number and quite stable because they are created not end-to-end but in the MPLS cloud, where the network topology changes less frequently and traffic is more regular with respect to edges.
\end{itemize}
\FloatBarrier

\subsubsection{Path setup}
When a host wants to generate and send the first packet toward a destination, how does it ask for setup of a new path and which label should it use?

%20
\paragraph{Manual setup}
Paths and their corresponding labels are manually set by the network administrator.

\subparagraph{Disadvantages}
\begin{itemize}
 \item high risk of human configuration mistakes;
 \item no automatic re-routing in case of faults;
 \item not suitable for highly dynamic networks where users frequently ask for new paths.
\end{itemize}

%21
\paragraph{On-demand setup}
A signaling phase for path setup, that is for preparing labels in every node, is required, after which the host learns the label to use and it can send the first packet toward the destination.

\subparagraph{Advantages} \ul{Quality of service} is simpler:
\begin{itemize}
 \item it is possible to set up different paths based on the source asking for its setup (e.g. the rector can have a path privileged compared to the researcher);
 \item it is possible to include inside the signaling packet a piece of information specifying how much bandwidth to reserve for the path.
\end{itemize}

\subparagraph{Disadvantages}
\begin{itemize}
 \item \ul{complexity}: signaling is achieved through another forwarding technique (e.g. routing by network address) over a dedicated circuit $\Rightarrow$ complexity increases because the network must now manage two different forwarding techniques;
 \item \ul{scalability}: if the path is long and the number of nodes to cross is high, the signaling phase may last too long, especially if the communication sessions are quite short like in the network world.
\end{itemize}

%23
\subsection{Source routing}
The sender host writes into the packet itself the whole path which must follow to arrive at the destination.

\paragraph{Advantage} Internal routers in the network are extremely simple.

\paragraph{Disadvantage} The sender host must know the network topology and must interact directly with the other hosts in order to be able to compute paths $\Rightarrow$ this breaks the paradigm according to which end users should just send packets and the network is in charge of forwarding packets toward their destinations.

\paragraph{Adoption} IPv4 and IPv6 contemplate an option affecting the path of packets.

%24
\subsection{Comparison}
\paragraph{Routing by network address}
\begin{itemize}
 \item[\pro] \textbf{simplicity}: no setup, no state
 \item[\pro] \textbf{scalability (forwarding)}: no `per-session' state (stateless)
 \item[\con] \textbf{efficiency}: big packet header
 \item[\con] \textbf{scalability (routing)}: very big routing table
 \item[\con] \textbf{reliability}: difficult to guarantee the service
 \item[\con] \textbf{multipath}: it does not support multiple paths between two entities
\end{itemize}

\paragraph{Label swapping}
\begin{itemize}
 \item[\pro] \textbf{scalability (routing)}: reduced routing table
 \item[\pro] \textbf{efficiency}: small packet header
 \item[\pro] \textbf{guarantee of service}: possibility to guarantee the service (path booking)
 \item[\pro] \textbf{multipath}: multiple paths allowed between two entities
 \item[\con] \textbf{scalability (setup)}: processing of the packets for path setup (critical with `short' sessions)
 \item[\con] \textbf{scalability (forwarding)}: `per-session' state (needed for quality of service)
 \item[\con] \textbf{complexity}: path setup (path setup process, ad-hoc forwarding for path setup packets)
\end{itemize}

\paragraph{Source routing}
\begin{itemize}
 \item[\pro] \textbf{efficiency (routers)}: intermediate systems are extremely simple
 \item[\con] \textbf{efficiency (sources)}: end systems should worry about computing paths
\end{itemize}