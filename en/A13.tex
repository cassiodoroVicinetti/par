\chapter{Content Delivery Networks}
\label{cap:CDN}
%3
A \textbf{web cache} is a device that stores a local copy of most recently required content (e.g. HTTP resources) and reacts as a proxy server to clients' requests:
\begin{itemize}
 \item the web cache is closer to the user with respect to the web server:
\begin{itemize}
\item[\pro] \ul{performance}: the reply is faster when the requested resource is already in cache;
\item[\pro] \ul{bandwidth}: expensive long-distance links (e.g. transoceanic links) are not loaded;
\end{itemize}

\item[\con] \ul{reactive solution}: if the requested resource is not in cache, the user needs to wait for the web cache to acquire (\textbf{pull}) it from the web server;
\item[\con] \ul{no transparency}: the user's web browser needs to be manually configured to contact that web cache.
\end{itemize}

%7
A \textbf{Content Delivery Network} (CDN) is an overlay network\footnote{An \textbf{overlay network} is a computer network which is built on the top of another network.} of web caches scattered all around the world but cooperating with the purpose of offering to the user a better quality of experience\footnote{\textbf{Quality of experience} is a measure of a user's experiences with a service (e.g. web browsing).}:
%4
\begin{itemize}
\item \ul{proactive solution}: the web server copies (\textbf{push}) content (generally the most popular one) to the web cache before the users will ask for it;
\item \ul{transparency}: the user connects to the web cache automatically, without the need for manual configuration on his own client;
\item \ul{performance}: the user, even if he moves, always connects to the closest web cache;
\item \ul{load balancing}: the user always connects to the least loaded web cache;
%6
\item \ul{scalability}: the content deployment into multiple replicas allows a large number of requests which a single web server alone would not be able to serve;
\item \ul{conditional access}: it is possible to customize returned content based on the user (e.g. targeted advertisements).
\end{itemize}

%5
CDNs are ideal for content generating large amounts of traffic (e.g. multimedia resources), but not all content can be cached:
\begin{itemize}
\item dynamic web pages (e.g. stock market prices);
\item customized-content web pages (e.g. user account).
\end{itemize}

CDNs can be deployed in a variety of ways:
\begin{itemize}
\item \textbf{DNS-based CDNs}: traffic is redirected to the best replica based on host names:
\begin{itemize}
\item \ul{DNS-based routing} (sect.~\ref{sez:CDN_DNS}): the hosting provider needs to enter into agreements with DNS server managers;
\item \ul{Akamai approach} (sect.~\ref{sez:CDN_Akamai}): intervention on DNS servers is not needed;
\end{itemize}

%15
\item \textbf{URL-based CDNs}: traffic is redirected to the best replica based on full URLs:
\begin{itemize}
\item \ul{server load balancing} (sect.~\ref{sez:CDN_SLB}): the TCP connection termination point is close to the server;
\item \ul{content routing} (sect.~\ref{sez:CDN_content_router}): the TCP connection termination point is close to the client.
\end{itemize}
\end{itemize}

\section{DNS-based CDNs}
\subsection{DNS-based routing}
\label{sez:CDN_DNS}
\begin{figure}
	%11
	\centering
	\includegraphics{../pic/A13/11}
	\caption{Traditional browsing.}
\end{figure}

\begin{figure}
	%12
	\centering
	\includegraphics{../pic/A13/12}
	\caption{DNS-based CDN browsing.}
\end{figure}

%10
\noindent
Selection of the best replica takes place when the host name is translated to an IP address. The DNS reply to a query does not depend only on the host name, but also on the source: a special DNS server computes, based on as many metrics as possible (RTT, server load, response time, etc.), a replica routing table containing entries like:\\
\centerline{\{host name, client IP address\} $\rightarrow$ replica IP address}

%9
The routing engine in the `modified' DNS server has a standard interface to guarantee \ul{transparency}: the user believes that the IP address corresponding to the host name is the IP address of the real web server, while it is the IP address of one of its replicas.

Adding a new actor, the \textbf{hosting provider}, constitutes a new business opportunity in the network world:
\begin{itemize}
\item \ul{access provider}: it provides network access to users;
\item \ul{backbone provider}: it provides long-range connectivity;
\item \ul{hosting provider}: it provides the CDN service to content providers;
\item \ul{content provider}: it provides content.
\end{itemize}
\FloatBarrier

\paragraph{Issues}
\begin{itemize}
\item \ul{metrics}: metric measurement, especially the dynamic ones, is not easy, and layer-3 metrics alone are not particularly meaningful;
\item \ul{DNS caching}: only the authoritative server knows all replicas and can select the best replica based on the client location $\Rightarrow$ intermediate DNS servers in the hierarchy can not cache DNS replies;
%13
\item \ul{granularity}: redirection granularity is at host-name, not single-URL, level $\Rightarrow$ content of large web sites can not be split into multiple caches, hence the same replica will be asked for two different pages in the same web site.
\end{itemize}

\subsection{Akamai approach}
\label{sez:CDN_Akamai}
Akamai CDN exploits a proprietary automatic algorithm to redirect traffic to its replicas without any intervention on DNS servers:
\begin{enumerate}
\item the user types the address of a web page with its normal domain name (e.g. \texttt{http://cnn.com/index.html});
\item the server of the content provider (e.g. CNN) returns a web page where the address of every multimedia resource (e.g. image) has a special domain name corresponding to a specific replica on an Akamai cache (e.g. \texttt{http://a128.g.akamai.net/7/23/cnn.com/a.gif} instead of \texttt{http://cnn.com/a.gif});
\item the user's web browser when parsing the page performs DNS queries to the new domain names and gets multimedia resources from the closest replicas.
\end{enumerate}

\section{URL-based CDNs}
\subsection{Server load balancing}
\label{sez:CDN_SLB}
\begin{figure}
	%20
	\centering
	\includegraphics[scale=1.05]{../pic/A13/20}
	\caption{Content-aware SLB.}
\end{figure}

%17
\noindent
The real servers containing replicas are seen by clients as a single virtual server with the same IP address.

%18
The traffic load destined to the virtual server is balanced among the several real servers by a \textbf{Server Load Balancer} (SLB):
%19
\begin{itemize}
\item \ul{layer-4 switching}: TCP connections are not terminated by the SLB (\textbf{content-unaware}):
\begin{itemize}
\item one of the real servers answers the three-way handshake with the client;
\item all HTTP queries belonging to the same TCP session have to be always served by the same real server;
\item load balancing can be based on the source IP address, the source TCP port, etc.;
\end{itemize}

%20
\item \ul{layer-7 switching}: TCP connections are terminated by the SLB (\textbf{content-aware}), acting as a proxy:
\begin{itemize}
\item the SLB answers the three-way handshake with the client, to be able to catch URLs requested at a later time;
\item each HTTP query can be served by the currently least loaded real server, based on SLB decisions;
\item load balancing is based on the full URL.
\end{itemize}
\end{itemize}

\paragraph{Issues}
\begin{itemize}
%19
\item \ul{encrypted connections} (HTTP): the SLB needs to have the private SSL cryptographic key of the server, and needs to support the processing load for encrypting/decrypting packets in transit;
%21
\item \ul{sticky connections}: some applications require that TCP connections from the same client are redirected to the same server (e.g. shopping cart) $\Rightarrow$ cookies should be considered too;
\item \ul{geographical distribution}: all replicas are close to each other and to the SLB, which is far away from the client.
\end{itemize}
\FloatBarrier

\subsection{Content routing}
\label{sez:CDN_content_router}
%26
\textbf{Content routers} are routers which route traffic based on the URL toward the best replica:
\begin{itemize}
\item \ul{TCP}: all content routers in a sequence terminate TCP connections between them $\Rightarrow$ too many delays are introduced;
\item \ul{content delivery control protocol}: the URL is extracted by the first content router, and is propagated by a specific protocol.
\end{itemize}

\paragraph{Issues}
\begin{itemize}
%23
\item \ul{stateful}: the first content router needs to terminate the TCP connection to be able to catch the URL the user will query;
\item \ul{complexity of devices}: packet parsing for getting the URL is complex $\Rightarrow$ layer-7 switches are complex and expensive devices;
%26
\item \ul{complexity of protocols}: proposed content delivery control protocols are really complex;
%28
\item \ul{privacy}: content routers read all the URLs queried by users.
\end{itemize}