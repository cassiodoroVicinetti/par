\chapter{IGRP and EIGRP}
%3-4
\textbf{Interior Gateway Routing Protocol} (IGRP) is an intra-domain routing protocol, proprietary by Cisco, based on the Distance Vector (DV) algorithm.

Also in IGRP the support for \textbf{classless addressing} (netmask) is absent, but with respect to RIP it has some additional `marketing-oriented' features which however hide some unexpected technical mistakes:
\begin{itemize}
 \item \ul{more sophisticated metrics}: they introduce more complexity and less route stability;
 \item \ul{multipath routing}: unequal-cost multipath routing may originate loops;
 \item support for \ul{heterogeneous networks}: a wide range for link costs may slow down convergence to infinity;
 %15
 \item \ul{less traffic} related to routing protocol: DV update happens every 90 seconds;
 %14
 \item \ul{greater stability}: triggered updates are sent only if the cost has changed more than 10\% to avoid frequent reconfiguration of the network;
 \item \ul{not more than one IP fragmentation}: IGRP messages also transport information about the MTUs supported by the routers along the path $\Rightarrow$ the packet can be fragmented immediately based on the minimum MTU, avoiding that at a later time it will be fragmented again by a smaller MTU.
\end{itemize}

\section{Metrics}
%7
Cost $C$ is obtained by the combination of 4 metrics:
\[
 \begin{cases} \displaystyle C = \frac{10^7}{B} \left( k_1 + \frac{k_2}{256-L} \right) + k_3 D & \text{if } k_5 = 0 \\ \displaystyle C = \left[ \frac{10^7}{B} \left( k_1 + \frac{k_2}{256-L} \right) + k_3 D \right] \frac{k_5}{R+k_4} & \text{if } k_5 \neq 0 \end{cases}
\]
%5-6
\begin{itemize}
 \item[$B$ -] \textbf{bandwidth}: it is directly proportional to the link bandwidth (values 1 to 2\textsuperscript{24} with 1 = 1.2 kbit/s);
 \item[$D$ -] \textbf{delay}: it is inversely proportional to the link bandwidth, and it only considers transmission delay ignoring other components such as propagation delay and queuing delay (values 1 to 2\textsuperscript{24} with 1 = 10 ms);
 \item[$R$ -] \textbf{reliability}: it can be quite variable in time (values 1 to 255 with 255 = 100\%);
 \item[$L$ -] \textbf{load}: it depends on instantaneous traffic (values 1 to 255 with 255 = 100\%).
\end{itemize}

With default values for coefficients $k_{1 \ldots 5}$, the cost only considers delay $D$ and bandwidth $B$:
\[
 k_1 = k_3 = 1 , \, k_2 = k_4 = k_5 = 0 \Rightarrow C = \frac{10^7}{B} + D
\]

IGRP commands require the specification of a \ul{class of service} (TOS), but in practice routing based on classes of service has never been implemented in this protocol, because it would require a different routing table and a different cost function for each class of service.

\paragraph{Problems}
Such a sophisticated metric really suffers from some problems from the technical point of view:\footnote{Please see section~\ref{sez:metrica}.}
\begin{itemize}
 %8
 \item it is difficult to understand the \ul{routing choices}: humans look at the network topology and measure the distance in `hop count' $\Rightarrow$ it is not easy to determine which is the best path when a more sophisticated metric is adopted;
 \item it is difficult to understand how to tune \ul{coefficients} $k_{1 \ldots 5}$: what happens to the network when parameters are changed? which values have to be given to them in order to obtain the wanted behavior?
 %9
 \item some \ul{metrics} (e.g. load), being not very stable, force the network to continuously adapt its paths because the latter often change their costs $\Rightarrow$ the need to frequently update routes leads to more transients with consequent black holes and bouncing effects, more routing traffic and more CPU resources dedicated to routing protocols;
 %10-11
 \item it is difficult to define the right \ul{threshold value for infinity}: IGRP defines it to 2\textsuperscript{24}, but it is required too much time when low-cost links are involved.\footnote{Please see section~\ref{sez:soglia_infinito}.}
\end{itemize}

\section{Multipath routing}
%12
IGRP supports \textbf{unequal-cost multipath routing}: multiple routes are allowed for the same destination, even if those routes have different costs ($c_\text{sec} \leq V \cdot c_\text{prim}$), and load is distributed proportionally to the cost of the route.\footnote{Please see section~\ref{sez:unequal_multipath_routing}.}

%13
\paragraph{Problem} Traffic may enter a loop when different paths are chosen by two routers: one may choose the primary path (optimal route) and the other one the secondary path (sub-optimal route) $\Rightarrow$ in the latest version of IGRP only equal-cost multipath routing is allowed (coefficient $V$ is set to 1) in order to prevent these issues.

%16-17-18-19-20
\section{EIGRP}
\textbf{Enhanced IGRP} introduces several enhancements to IGRP, especially from the scalability point of view:
\begin{itemize}
 \item it supports \textbf{classless addressing}: networks are at last announces with their proper address-netmask pairs;
 \item it implements \textbf{Diffusing Update Algorithm} (DUAL): the network is loop-free, even during transients, and convergence is faster (no count-to-infinity phenomena);\footnote{Please see section~\ref{sez:dual}.}
 \item it decouples the \textbf{neighbor discovery} functionality from the route update mechanism: routers periodically exchange small Hello messages, while DVs are generated only when something has changed in the network:
 \begin{itemize}
  \item \ul{Hello messages} can be sent at high frequency, making fault detection and hence convergence faster, because:
  \begin{itemize}
   \item they consume less bandwidth $\Rightarrow$ routing traffic is reduced;
   \item they consume less CPU resources with respect to DV processing and computation;
  \end{itemize}
  \item DVs must be sent through a \ul{reliable protocol}: every DV must be confirmed by an acknowledgment message, and must be retransmitted if it went lost.
 \end{itemize}
\end{itemize}