\chapter{Inter-domain routing}
\label{cap:interdominio}
%171
\textbf{Inter-domain routing} is in charge of deciding and propagating information about \textbf{external routes} among multiple interconnected ASes over the network.

\section{Autonomous Systems}
\label{sez:AS}
%4[A7]-11[A7]
An \textbf{Autonomous System} (AS) is a set of IP networks that are under control of a set of entities that agree to present themselves as a unique entity, everyone adopting the same set of routing policies.

From the inter-domain routing point of view, Internet is organized into ASes: an AS represents an \ul{homogeneous administrative entity}, generally an ISP, at the highest hierarchical level on the network. %
%12[A7]
Each AS is uniquely identified by a 32-bit number (it was 16-bit in the past) assigned by IANA.

%5[A7]-14[A7]
Each AS is completely \ul{independent}: it can decide internal routing according to its own preferences, and IP packets are routed inside it according to internal rules. Each AS can have one or more internal routing domains served by IGP protocols: each domain can adopt its favourite IGP protocol, and thanks to redistribution it can exchange routing information with other domains.

A network being AS can keep under its control incoming and outgoing traffic thanks to routing policies, but is subject to a greater \ul{responsibility}: routing is more difficult to configure, and possible configuration mistakes may affect traffic of other ASes.

%12[A7]
For network portions who are going to become ASes, in the past some additional rules were enforced which nowadays have been relaxed:
\begin{itemize}
 \item all the network has to be on the \ul{same administrative domain}:
 \begin{itemize}
 \item[] nowadays the administrative entity of an AS does not necessarily coincides with the organization actually managing internally the network: for example, the network at the Politecnico di Torino, although being owned by the university and being under the control of bodies inside it, is one of the subnetworks inside the AS administered by the GARR research body, which is in charge of deciding long-distance interconnections toward other ASes;
 \end{itemize}
 
 \item the network has to be of at least a given \ul{size}:
 \begin{itemize}
 \item[] in recent years content providers have needed to have some ASes spread around the world of very small size: for example, Google owns some web servers in Italy which distribute custom content for the Italian audience (e.g. advertisements) and which, being closer to users, return more quickly search results acting as a cache (please refer to chapter~\ref{cap:CDN}) $\Rightarrow$ if those web servers constitute themselves an AS, Google has control over the distribution of its content to Italian ISPs, and can make commercial agreements with the latter favouring some of them at the expense of other ones;
 \end{itemize}
 
 \item the AS has to be connected with at least two other ASes to guarantee, at least technically, \ul{transit} across it for traffic from an AS to another one:
 \begin{itemize}
 \item[] a local ISP of small size (Tier 3) may buy by a national ISP of big size (Tier 2) the whole connectivity toward the Internet (please refer to section~\ref{sez:accordi_as}).
 \end{itemize}
\end{itemize}

\section{EGP protocol class}
\label{sez:classe_egp}
%167
A single border router put between ASes belonging to different ISPs arises some issues:
\begin{itemize}
 \item who owns it? who configures it?
 \item who is responsible in case of failure?
 \item how to prevent an ISP from collecting information about a competitor's network?
\end{itemize}

%168-179
The solution is to use two border routers, each one administered by either of the two ISPs, separated by a sort of intermediate `free zone' handled by a third routing protocol instance of type \textbf{Exterior Gateway Protocol} (EGP).

%16[A7]
Through an EGP protocol, every border router at the border of an AS exchanges external routing information with other border routers:
\begin{itemize}
 \item it propagates to other ASes information about destinations which are inside its AS;
 \item it propagates to other ASes information about destinations which are inside other ASes but can be reached through its AS.
\end{itemize}

%177
EGP protocols differentiate from IGP protocols especially for support to \textbf{routing policies} reflecting commercial agreements among ASes (please refer to section~\ref{sez:politiche_interdominio}).

\subsection{EGP protocols}
\label{sez:protocolli_egp}
\begin{itemize}
%4[A9]
\item \textbf{static routing}: configuration of routers by hand:
\begin{itemize}
\item[\pro] this is the best ``algorithm'' to implement complex policies and to have the complete control over network paths;
\item[\pro] no control traffic is needed: information about destinations is avoided to be exchanged;
\item[\con] it does not react to topological changes;
\item[\con] it is easy to introduce inconsistencies;
\end{itemize}

\item \textbf{Exterior Gateway Protocol} (EGP)\footnote{The EGP protocol is one of the protocols belonging to the EGP protocol class.}: it was the first protocol completely dedicated to routing among domains, but currently nobody uses it because it provides just information about reachability and not about distance:
\begin{itemize}
\item if the reachability of a destination is advertised across multiple paths, the least-cost best path can not be chosen;
\item if the reachability of a destination is advertised across multiple paths, all routers are not guaranteed that will choose a coherent path $\Rightarrow$ this can be used only in networks without closed paths where no loop can form;
\end{itemize}

%16[A7]-5[A9]
\item \textbf{Border Gateway Protocol} (BGP): it is the only EGP protocol which has been adopted in the whole Internet at the expense of other EGP protocols: all border routers in the whole network of interconnected ASes must adopt the \ul{same EGP protocol} for exchanging external routes, because if two ASes would choose to use different EGP protocols, their border routers could not communicate one with each other (please refer to chapter~\ref{cap:bgp});

%6[A9]
\item \textbf{Inter-Domain Routing Protocol} (IDRP): it was created as an evolution to BGP in order to support OSI addressing, but currently nobody uses it because:
\begin{itemize}
\item it is made up of rather complex parts;
\item since then improvements introduced by IDRP have been ported to the next versions of BGP;
\item it is not compatible with BGP $\Rightarrow$ its adoption by an AS would break interoperability with the rest of the network which is still using BGP.
\end{itemize}
\end{itemize}

%168-179
\section{Redistribution}
On every border router a redistribution process is running from the IGP protocol inside the AS to the EGP protocol outside the AS and vice versa $\Rightarrow$ routes are redistributed first from an AS to the intermediate area and then from here to the other AS:
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{../pic/A1.5/168}
	\caption{Example of redistribution between ASes belonging to different ISPs.}
\end{figure}
%17[A7]
\begin{itemize}
 \item the IGP protocol learns \textbf{external routes} toward destinations which are in other ASes, and propagates them into the AS as internal routes;
 \item the EGP protocol learns \textbf{internal routes} toward destinations which are in the AS, and propagates them to other ASes as external routes.
\end{itemize}
\FloatBarrier

%176-7[A7]-15[A7]
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/A1.6/176}
	\caption{Routes are redistributed both between hierarchical domains inside the AS, and between the AS itself and the external world.}
\end{figure}

%18[A7]
Redistribution defines:
\begin{itemize}
 \item which internal networks must be known to the outside world: private networks for example must not be propagated to other ASes;
 \item which external networks must be known inside the AS: the amount of announced routing information can be reduced by avoiding to include full details about external networks:
 \begin{itemize}
 %10[A7]
 \item announced addresses can be `collapsed' into \ul{aggregate routes} when they share part of their network prefixes;
 \item a single \ul{default route} can be announced when the AS has a single exit point.
 \end{itemize}
\end{itemize}

Redistribution must not introduce incoherences in routing:
\begin{itemize}
 \item a \ul{routing loop} may form if, for example, a route learnt in IGP and exported in EGP is then re-imported in IGP appearing as an external route;
 \item if a certain AS is reachable across multiple border routers of the same AS, these border routers need to agree in order to internally redistribute a \ul{single exit point} for that route.
\end{itemize}

Often redistribution on a border router at the border of an AS is enabled in one way only from the IGP protocol to the EGP protocol: internal routes are exported to the external world, while external routes are replaced by a default route.
%\FloatBarrier