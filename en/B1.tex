\chapter{Hints on the architecture of network devices}
The architecture of routers has been evolving over time to increase more and more their packet processing capability:
\begin{itemize}
\item \ul{first generation} (until early 1990): lower than 500 Mbps (sect.~\ref{sez:router_prima_generazione});
\item \ul{second generation} (early 1990): about 5 Gbps (sect.~\ref{sez:router_seconda_generazione});
\item \ul{third generation} (late 1990): starting from 50 Gbps (sect.~\ref{sez:router_terza_generazione});
\item \ul{multi-chassis} (recent trend): of the order of Tbps (sect.~\ref{sez:router_multichassis}).
\end{itemize}

%2
\section{First generation}
\label{sez:router_prima_generazione}
\begin{figure}
	\centering
	\includegraphics[scale=1.05]{../pic/B1/2}
	\caption{First-generation router architecture.}
\end{figure}

\noindent
First-generation routers were basically modified PCs:
\begin{itemize}
\item \ul{network interfaces}: they are normal NICs, in a greater number and in manifold kinds with respect to the ones on a normal PC;
\item \ul{memory}: solid-state memory (e.g. SD card) is preferred because less subject to failures with respect to a mechanical hard disk;
\item \ul{operating system}: it is optimized specifically for network traffic processing.
\end{itemize}

\paragraph{Advantage} \ul{economy of scale}: it uses components manufactured on a large scale instead of components dedicated to networking world.

\paragraph{Disadvantages}
\begin{itemize}
\item \ul{bottlenecks}:
\begin{itemize}
\item \ul{shared bus}:
\begin{itemize}
\item \ul{slow path}: each packet transits twice over the bus;
\item \ul{arbitrage}: one NIC at a time can use the bus $\Rightarrow$ multiple interfaces can not work in parallel;
\end{itemize}
\item \ul{memory access}: the routing table is stored in a data structure in the generic memory $\Rightarrow$ accesses are not so localized as in traditional PCs, and the cache is little used;
\item \ul{processing capability}: the CPU interrupts the operating system whenever a packet arrives;
\end{itemize}

\item \ul{network interfaces}: traditional network cards have not many ports and only support the most common kinds of interfaces;
\item \ul{maintenance costs}: using open-source components (e.g. PPP client) brings integration troubles and varied configuration ways.
\end{itemize}

Currently this architecture is used for mid-low-end routers, whose performance is adequate for small offices.
\FloatBarrier

\section{Second generation}
\label{sez:router_seconda_generazione}
\begin{figure}
	\centering
	\includegraphics[scale=1.05]{../pic/B1/3}
	\caption{Second-generation router architecture.}
\end{figure}

%3-12-13-14
\noindent
Second-generation routers try to solve the performance problem by moving the processing load from the core CPU to edge units: NICs are replaced by \textbf{line card}, `smart' network cards which add forwarding modules to physical/MAC components:
\begin{itemize}
\item \ul{CPU}: the \textbf{packet processor} is a dedicated processor for `simple' packet processing;
\item \ul{memory}: each incoming packet is stored into a local memory split from the one (generally TCAM) containing the routing table;
\item \ul{routing table}: sometimes the updated routing table is copied from the central unit to line cards.
\end{itemize}

%4
Packets can follow two ways:
\begin{itemize}
\item \textbf{fast path}: the packets falling in the typical case (e.g. IPv4) transit only once over the bus: they are processed directly by the packet processor and are immediately sent to the output interface;
\item \textbf{slow path}: least frequent packets (e.g. IPv6, OSPF) are leaved to the core CPU for a more sophisticated processing, at the cost of a definitely lower throughput.
\end{itemize}

%16
The work of line cards should be coordinated by the core CPU, on which routing protocols (e.g. OSPF) are running too, which is mounted on a card called \textbf{supervisor} in high-end systems.

\paragraph{Advantages}
\begin{itemize}
\item \ul{core CPU}: it works only for packets along the fast path $\Rightarrow$ it can be less powerful;
\item \ul{fast path optimization}: the first packets in a connection follow the slow path, then the central system marks the connection as eligible for the fast path and all following packets will follow the fast path;
\item \ul{flexibility}: the introduction of a new protocol (e.g. IPv6) only requires updating the software on the central system.
\end{itemize}

\paragraph{Disadvantage} \ul{shared bus arbitrage}: one line card at a time can use the bus $\Rightarrow$ multiple interfaces can not work in parallel.
\FloatBarrier

\section{Third generation}
\label{sez:router_terza_generazione}
\begin{figure}
	\centering
	\includegraphics[scale=1.05]{../pic/B1/5}
	\caption{Third-generation router architecture.}
\end{figure}

%5-15
\noindent
Third-generation routers focus on the word parallelization problem by replacing the shared bus with a \textbf{switching fabric} (or crossbar) able to handle multiple transfers: a line card is connected to another line card by shorting the switch at the intersection of their `wires' in the switching matrix.

%6
Two line cards can not be connected to another same line card at the same time $\Rightarrow$ an \textbf{arbiter} is needed which drives switching points and solves contention situations without collisions.

When output interfaces are slow in sending packets with respect to the switching speed of the switching fabric, packets can be queued in various ways:
\begin{itemize}
%7
\item \ul{output queuing}: buffers are placed on output interfaces $\Rightarrow$ worst case: switching fabric $N$ times faster than receiving speed, with $N$ = number of input interfaces;

%8
\item \ul{input queuing}: buffers are placed on input interfaces $\Rightarrow$ the switching speed of the switching fabric does not depend on the number of input interfaces, but suffers from the \textbf{head-of-line blocking} problem:
\begin{enumerate}
\item two packets in two input queues are destined to the same output queue;
\item the arbiter lets one of the packets pass blocking the other one;
\item all the following packets in the input queue, even the ones destined to free output queues, have to wait for the `head of line';
\end{enumerate}

%9
\item \ul{virtual output queuing}: it solves the head-of-line blocking problems by keeping one queue per output at each input;

%10
\item \ul{buffered fabric}: buffers are placed inside the crossbar at the spot of switching nodes.
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
\item \ul{arbiter}:
\begin{itemize}
\item it constitutes a bottleneck, especially in case of frequent decisions due to small packets (e.g. ATM);
\item scheduling policies for quality of service may make its hardware more complex;
\end{itemize}
\item \ul{queue buffers}: they are expensive memories because they should be fast.
\end{itemize}
\FloatBarrier

%11-18-19
\section{Multi-chassis routers}
\label{sez:router_multichassis}
\textbf{Multi-chassis routers} aim at scalability: multiple flanked racks are connected so as to appear as a single router:
\begin{itemize}
\item the \ul{processing power} is multiplied by the number of chassis;
\item there is more physical room for network interfaces $\Rightarrow$ a \ul{large number of links} can be connected to a same device;
\item routing is concentrated in a \ul{single station} (like in the telephone world) instead of a lot of small routers spread over the network $\Rightarrow$ the data station can serve all the users within a wide geographical area (e.g. Piedmont).
\end{itemize}

\section{Service cards}
\label{sez:service_card}
Another recent trend aims at flexibility: routers are no longer purely layer-3 devices, because a lot of other features not belonging to layer 3 are being added, such as cache, firewall, network monitor, ACL, etc. The goal is to customize the service offered by ISPs, bringing back a portion of the business currently in the hands of content providers.

%17
A currently common solution are \textbf{service cards}, line cards enriched with value-added services pre-configured by the manufacturer. Service cards however are not so flexible: being not programmable, a new service card should be purchased to add a service.

%21
\section{Major current issues}
Throughput is no longer a current issue by now, but the current major issues are:
\begin{itemize}
\item \ul{purchase costs}: to reduce the cost of network devices dedicated components are being replaced by general-purpose components made on a large scale:
\begin{itemize}
\item mainstream CPUs have too short product life cycles and too long replacement times $\Rightarrow$ Intel recently proposed embedded CPUs with longer product life cycles and shorter replacement times with respect to mainstream CPUs;
\item in dedicated systems the programmer could exploit the memory hierarchy, by storing the RIB into a slow memory and the FIB into a fast memory (e.g. TCAMs), while general-purpose systems autonomously decide what to put into the cache $\Rightarrow$ GPUs, having partitioned memories but limited processing versatility, should be addressed;
\end{itemize}

\item \ul{operational costs}: power consumption and heat dissipation may be significant;

\item \ul{flexibility}: it is required to move toward the separation of control features from physical hardware (please refer to chapter~\ref{cap:SDN}).
\end{itemize}

% Catalist 6500: e' vecchia, ma continua ad essere presente. Si aggiorna un pezzo per volta. Si tratta di un'architettura estremamente longeva. Uno dei problemi principali, ma anche uno dei punti di forza era la necessita' di compatibilita' con i componenti piu' vecchi.