\chapter[Routing algorithms]{Routing algorithms%
%26
\footnote{Routing algorithms presented in the following assume they work on a network based on routing by network address.}}
%27
A \textbf{routing algorithm} is a process of collaborative type in charge of deciding, in every intermediate node, the directions which must be used to reach destinations:
\begin{enumerate}
 %28
 \item it determines the \textbf{destinations reachable} by each node:
 \begin{itemize}
  \item it \ul{generates} information about the reachability of \ul{local} networks: the router informs its neighbor routers that the local network exists and it is reachable through it;
  \item it \ul{receives} information about the reachability of \ul{remote} networks: a neighbor router informs the router that the remote network exists and it is reachable through it;
  \item it \ul{propagates} information about the reachability of \ul{remote} networks: the router informs the other neighbor routers that the remote network exists and it is reachable through it;
 \end{itemize}
 %29
 \item it computes \textbf{optimal paths} (next hops), in a cooperative way with the other nodes, according to certain criteria:
 \begin{itemize}
  \item a \ul{metric} has to be established: a path may be the best one based on a metric but not based on another metric;
  \item criteria must be \ul{coherent} among all the nodes in the network to avoid loops, black holes, etc.;
  \item the algorithm must operate \ul{automatically} to avoid human errors in manual configuration and to favor scalability;
 \end{itemize}
 %35
 \item it stores local information in the \textbf{routing table} of each node: a routing algorithm is not required to know the entire topology of the network, but it is only interested in building the correct routing table.
\end{enumerate}

\paragraph{Characteristics of an ideal routing algorithm}
\begin{itemize}
%33
 \item \ul{simple} to implement: less bugs, easy to understand, etc.;
 \item \ul{lightweight} to execute: routers should spend as less resources as possible in running the algorithm because they have limited CPU and memory;
 \item \ul{optimal}: the computed paths should be optimal according to the chosen metrics;
 \item \ul{stable}: it should switch to another path just when there is a topology or a cost change to avoid \textbf{route flapping}, that is the frequent change of preferred routes with consequent excess of transient periods;
 \item \ul{fair}: it should not favour any particular node or path;
%34
 \item \ul{robust}: it should be able to automatically adapt to topology or cost changes:
 \begin{itemize}
  \item \textbf{fault detection}: it should not rely on external components to detect a fault (e.g. a fault can not be detected at the physical layer if it occurs beyond a hub);
  \item \textbf{auto-stabilization}: in case of variations in the network it should converge to a solution without any external intervention (e.g. explicit manual configuration);
  \item \textbf{byzantine robustness}: it should recognize and isolate a neighbor node which is sending fake information, due to a fault or a malicious attack.\\
  Internet does not implement byzantine robustness, but it is based on confidence $\Rightarrow$ faults and malicious behaviours require human intervention.
 \end{itemize}
\end{itemize}

%50
\paragraph{Classification of routing algorithms}
\begin{itemize}
 \item \textbf{non-adaptive algorithms} (static): they take decisions independently of how the network is (section~\ref{sez:algoritmi_non_adattativi}):
 \begin{itemize}
 \item \ul{static routing} (or fixed directory routing)
 \item random walk
 \item flooding, selective flooding
 \end{itemize}
 \item \textbf{adaptive algorithms} (dynamic): they learn information about the network to better take decisions (section~\ref{sez:algoritmi_adattativi}):
 \begin{itemize}
 \item \ul{centralized routing}
 \item \ul{isolated routing}: hot potato, backward learning
 \item \ul{distributed routing}: Distance Vector, Link State
 \end{itemize}
 \item \textbf{hierarchical algorithms}: they allow routing algorithms to scale up on wide infrastructures (chapter~\ref{cap:algoritmi_gerarchici}).
\end{itemize}

\section{Metric}
\label{sez:metrica}
%30
A \textbf{metric} is the measure of how good a path is, obtained by transforming a physical quantity (e.g. distance, transmission speed), or a combination of them, in numerical form (\textbf{cost}), in order to choose the least-cost path as the best path.

A best metric does not exist for all the kinds of traffic: for example bandwidth is suitable for file-transfer traffic, while transmission delay is suitable for real-time traffic. The choice of the metric can be determined from the `Type of Service' (TOS) field in the IP packet.

\paragraph{Issues}
\begin{itemize}
  %31
  \item \ul{(non-)optimization}: the primary task of routers is to forward users' traffic, not to spend time in computing paths $\Rightarrow$ it is better to prefer solutions which, even if they are not fully optimized, do not compromise the primary functionality of the network and do not manifest problems which can be perceived by the end user:
  \begin{itemize}
    \item \ul{complexity}: the more criteria are combined, the more complex the algorithm becomes and the more computational resources at run-time it requires;
    \item \ul{stability}: a metric based on the available bandwidth on the link is too unstable, because it depends on the instantaneous traffic load which is very variable in time, and may lead to route flapping;
  \end{itemize}
  %32
  \item \ul{inconsistency}: metrics adopted by nodes in the network must be coherent (for every packet) to avoid the risk of loops, that is packet `bouncing' between two routers using different conflicting metrics.
\end{itemize}

\section{Transients}
%41
Modern routing algorithms are always `active': they exchange service messages all the time to detect faults autonomously. However, they do not change the routing table unless a \textbf{status change} is detected:
\begin{itemize}
 \item \ul{topology changes}: link fault, addition of a new destination;
 \item \ul{cost changes}: for example a 100-Mbps link goes up to 1 Gbps.
\end{itemize}

%36
%Tutti gli algoritmi di instradamento non possono non intervallare il normale funzionamento in convergenza della rete con delle fasi di transitorio:
Status changes result in \textbf{transient phases}: all the nodes in a distributed system can not be updated at the same time, because a variation is propagated throughout the network at a finite speed $\Rightarrow$ during the transient, status information in the network may not be coherent: some nodes already have new information (e.g. the router detecting the fault), while other ones still have old information.

%37
Not all status changes have the same impact on data traffic:
\begin{itemize}
 \item \ul{positive status changes}: the effect of the transient is limited because the network may just work temporarily in a sub-optimal condition:
 \begin{itemize}
 \item a path gets a better cost: some packets may keep following the old path now become less convenient;
 \item a new destination is added: the destination may appear unreachable due to black holes along the path towards it;
 \end{itemize}
 \item \ul{negative status changes}: the effect of the transient manifests itself more severely to the user because it interferes also with the traffic that should not be affected by the fault:
 \begin{itemize}
 \item a link fault occurs: not all routers have learnt that the old path is no longer available $\Rightarrow$ the packet may start `bouncing' back and forth saturating the alternative link (routing loop);
 \item a path worsens its cost: not all routers have learnt that the old path is no longer convenient $\Rightarrow$ analogous to the case of fault (routing loop).
 \end{itemize}
\end{itemize}

In general, two common problems affect routing algorithms during the transient: black holes and routing loops.

%38
\subsection{Black holes}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A1.2/38}
	\caption{Black hole.}
\end{figure}

\noindent
A \textbf{black hole} is defined as a router which, even if at least a path exists through which it could reach a certain destination, does not know any of them (yet).

\paragraph{Effect} The effect on data traffic is limited to packets directed toward the concerned destination, which are dropped until the node updates its routing table and acquires information about how to reach it. Traffic directed to other destinations is not affected by the black hole at all.
\FloatBarrier

%39-40
\subsection{Routing loops}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A1.2/40}
	\caption{Routing loop.}
\end{figure}

\noindent
A \textbf{routing loop} is defined as a cyclic path from the routing point of view: a router sends a packet on a link but, because of an inconsistency in routing tables, the router at the other endpoint of the link sends it back.
\FloatBarrier

\paragraph{Effect} Packets directed toward the concerned destination start `bouncing' back and forth (\textbf{bouncing effect}) $\Rightarrow$ the link gets saturated $\Rightarrow$ also traffic directed to other destinations crossing that link is affected.

%42-43
\subsection{Backup route}
\textbf{Backup route} is a concept mostly used in telephone networks based on a hierarchical organization: every exchange is connected to an upper-level exchange by a primary route, and to another upper-level exchange by a backup route $\Rightarrow$ if a fault occurs in the primary route, the backup route is ready to come into operation without any transient period.

A data network is instead based on a meshed topology, where routers are interconnected in various ways $\Rightarrow$ it is impossible to foresee all possible failures of the network to prearrange backup paths, but when a failure occurs a routing algorithm is preferable automatically computing on the fly an alternative path (even if the computational step requires a transient period).

Backup route in modern networks can still have applications:
\begin{itemize}
 \item a company connected to the Internet via ADSL can keep its connectivity when the ADSL line drops by switching to a backup route via HiperLAN technology (wireless);
 \item the internet backbone is by now in the hands of telephone companies, which have modeled it according to criteria of telephone networks $\Rightarrow$ its organization is hierarchical enough to allow backup routes to be prearranged.
\end{itemize}

%44
\section{Multipath routing}
With routing by network address, all the packets toward a destination follow the same path, even if alternative paths are available $\Rightarrow$ it would be preferable to make part of traffic follow an alternative path, even if more costly, not to saturate the path chosen by the algorithm.

\textbf{Multipath routing}, also known as `load sharing', is a traffic engineering feature aiming at distributing traffic toward the same destination over multiple paths (when available), allowing multiple entries for each destination in the routing table, for a more efficient usage of network resources.

%45
\subsection{Unequal-cost multipath routing}
\label{sez:unequal_multipath_routing}
An alternative path is used only if it has cost $c_\text{sec}$ not too greater than cost $c_\text{prim}$ of the least-cost primary path:
 \[
  \text{given } K \ge 1 , \,  c_\text{sec} \ge c_\text{prim} : \; \text{sec used} \Leftrightarrow c_\text{sec} \le K \cdot c_\text{prim}
 \]

Traffic is distributed inversely proportionally to the cost of the routes. For example in this case:
 \[
  \begin{cases} c_\text{prim} = 10 \\ c_\text{sec} = 20 \end{cases} \Rightarrow \begin{cases} \text{prim} = 66\% \text{ traffic} \\ \text{sec} = 33\% \text{ traffic} \end{cases}
 \]
the router may decide to send the packet with 66\% probability along the primary path and 33\% probability along the secondary path.

\paragraph{Problem} Given a packet, each router autonomously decides on which path to forward it $\Rightarrow$ incoherent decisions between routers may make the packet enter a routing loop, and since the forwarding is usually session-based that packet will never exit the loop:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.47\linewidth]{../pic/A1.2/45}
	\caption{Routing loop caused by unequal-cost multipath routing.}
\end{figure}

\subsection{Equal-cost multipath routing}
%46
An alternative path is used only if it has cost $c_\text{sec}$ exactly equal to cost $c_\text{prim}$ of the primary path ($K=1$):
 \[
  \text{given } c_\text{sec} \ge c_\text{prim} : \; \text{sec used} \Leftrightarrow c_\text{sec} = c_\text{prim}
 \]
 
Traffic is equally partitioned on both the paths (50\%).

%47
\paragraph{Problems} If the first packet follows the slow path and the second packet follows the fast path, TCP mechanisms may cause overall performance to get worse:
\begin{figure}
\centering
	\begin{subfigure}[b]{.47\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/A1.2/46}
		\caption{TCP reordering problem.}
	\end{subfigure}%
	\begin{subfigure}[b]{.53\textwidth}
		\centering
		\includegraphics[width=\linewidth]{../pic/A1.2/47}
		\caption{Transmission rate decrease.}
	\end{subfigure}
\caption{Problems caused by equal-cost multipath routing.}
\end{figure}
\begin{itemize}
 \item \textbf{TCP reordering problem}: packets may arrive out of sequence: the second packet arrives at the destination before the first packet $\Rightarrow$ the process for sequence number reordering keeps busy the computational resources of the receiver;
 \item \textbf{transmission rate decrease}: if the acknowledgment packet (ACK) of the first packet arrives too late, the source thinks that the first packet went lost $\Rightarrow$ when 3 duplicate (cumulative) ACKs have arrived and the timeout expires, TCP sliding-window mechanisms get into the action:\footnote{Please refer to section \textit{Uso dei protocolli a finestra} in chapter \textit{Livello Trasporto: Protocolli TCP-UDP} in lecture notes `Reti di calcolatori'.}
\begin{itemize}
 \item \ul{fast retransmit}: the source unnecessarily transmits again the packet $\Rightarrow$ the packet gets duplicate;
 \item \ul{fast recovery}: the source thinks that the network is congested $\Rightarrow$ it slows down packet sending by limiting the transmission window: it sets the threshold value to half of the current value of the congestion window, and makes the congestion window restart from value 1 (that is only one packet at a time is sent and its ACK is awaited before sending the next packet).
\end{itemize}
\end{itemize}
\FloatBarrier

%48-49
\subsubsection{Criteria}
Real multipath routing implementations split traffic so that traffic toward a same destination follows the same path:
\begin{itemize}
 \item \textbf{flow-based}: every transport-layer session is identified by the quintuple:
 \begin{enumerate}
 \item \texttt{\textless source IP address\textgreater}
 \item \texttt{\textless destination IP address\textgreater}
 \item \texttt{\textless transport-layer protocol type\textgreater} (e.g. TCP)
 \item \texttt{\textless source port\textgreater}
 \item \texttt{\textless destination port\textgreater}
 \end{enumerate}
 and the router stores a table mapping session identifiers with output ports:
\begin{itemize}
 \item extracting the fields forming the session ID from the packet is onerous, due to the variety of supported packet formats (VLAN, MPLS\textellipsis);
 \item information about transport-layer ports is unavailable in case of fragmented IP packets;
 \item searching the session ID table for the quintuple is onerous;
 \item often TCP connection shutdown is not `graceful leaving', that is FIN and ACK packets are not sent $\Rightarrow$ entries in the session ID tables are not cleared $\Rightarrow$ it is required a thread performing sometimes a cleanup of old entries by looking at their timestamps;
\end{itemize}
\item \textbf{packet-based}: the router sends the packets having even (either destination or source or both) IP address to a path, and odd IP address to the other path $\Rightarrow$ the hashing operation is very fast.
\end{itemize}

\paragraph{Problem} Traffic toward a same destination can not use both the paths at the same time $\Rightarrow$ there are troubles in case of very big traffic toward a certain destination (e.g. a nightly backup between two servers).

\section{Non-adaptive algorithms}
\label{sez:algoritmi_non_adattativi}
\subsection{Static routing}
%51
The network administrator manually configures on each router its routing table.

\paragraph{Disadvantage} If a change in the network occurs, routing tables need to be manually updated.

%53
\paragraph{Application} Static routing is mainly used on routers at the network edges:
%52
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{../pic/A1.2/52}
\end{figure}
\begin{itemize}
\item edge routers are not allowed to \ul{propagate} routing information toward the backbone: the core router stops all the advertisements coming from the edge, otherwise a user could advertise a network with the same address as an existing network (e.g. google.com) and redirect towards him a part of traffic directed to that network.\\
Since users can not advertise their own networks, how can they be reachable from outside? The ISP, which knows which addresses are used by the networks it sold to users, must configure the core router so that it advertises those networks to other routers even if they are not locally connected;
\item edge routers are not allowed to \ul{receive} routing information from the backbone: an edge router is typically connected by a single link to a core router $\Rightarrow$ a global default route is enough to reach the entire Internet.
\end{itemize}
\FloatBarrier

%56
\subsection{Random walk}
When a packet arrives, the router chooses a port randomly (but the one from which it was received) and sends it out on that port.

\paragraph{Applications} It is useful when the probability that the packet reaches the destination is high:
\begin{itemize}
 \item peer-to-peer (P2P) networks: for contents lookup;
 \item sensor network: sending messages should be a low-power operation.
\end{itemize}

\subsection{Flooding}
%57
When a packet arrives, the router sends it out on all the ports (but the one from which it was received).

Packets may have a `hop count' field to limit flooding to a network portion.

%58
\paragraph{Applications}
\begin{itemize}
 \item military applications: in case of attack the network could be damaged $\Rightarrow$ it is critical that the packet arrives at destination, even at the cost of having a huge amount of duplicate traffic;
 %\item database distribuiti: l'aggiornamento simultaneo di più database può essere effettuato tramite la trasmissione di un singolo pacchetto;
 \item Link State algorithm: each router when receiving the network map by a neighbor has to propagate it to the other neighbors (please refer to section~\ref{sez:ls_flooding}).
\end{itemize}

\subsection{Selective flooding}
\label{sez:selective_flooding}
%59
When a packet arrives, the router first checks if it has already received and flooded it in the past:
\begin{itemize}
 \item old packet: it discards it;
 \item new packet: it stores and sends it out on all the ports (but the one from which it was received).
\end{itemize}

%60
Each packet is recognized through the sender identifier (e.g. the source IP address) and the sequence number:
\begin{itemize}
 \item if the sender disconnects from the network or shutdowns, when it connects again the sequence number will restart from the beginning $\Rightarrow$ the router sees all the received packets as old packets;
 \item sequence numbers are encoded on a limited number of bits $\Rightarrow$ the sequence number space should be chosen so as to minimize new packets wrongly recognized as old packets.
\end{itemize}

%61-62
\subsubsection{Sequence number spaces}
\paragraph{Linear space} It can be tolerable if selective flooding is used for few control messages:
 \begin{itemize}
 \item when the sequence number reaches the maximum possible value, an overflow error occurs;
 \item old packet: the sequence number is lower than the current one;
 \item new packet: the sequence number is greater than the current one.
 \end{itemize}

\paragraph{Circular space} It solves the sequence number space exhaustion problem, but it fails if a packet arrives with sequence number too far away from the current one:
 \begin{itemize}
 \item when the sequence number reaches the maximum possible value, the sequence number restarts from the minimum value;
 \item old packet: the sequence number is in the half preceding the current one;
 \item old packet: the sequence number is in the half following the current one.
 \end{itemize}

\paragraph{Lollipop space} The first half of the space is linear, the second half is circular.

\section{Adaptive algorithms}
\label{sez:algoritmi_adattativi}
%63-64
\subsection{Centralized routing}
All routers are connected to a centralized control core called \textbf{Routing Control Center} (RCC): every router tells the RCC which its neighbors are, and the RCC uses this information to create the map of the network, compute routing tables and communicate them to all routers.

\paragraph{Advantages}
\begin{itemize}
 \item \ul{performance}: routers have not to have a high computational capacity, all focused on a single device;
 \item \ul{debugging}: the network administrator can get the map of the whole network from a single device to check its correctness;
 \item \ul{maintenance}: intelligence is focused on the control center $\Rightarrow$ to update the algorithm just the control center has to be updated.
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
 \item \ul{fault tolerance}: the control center is a single point of failure $\Rightarrow$ a malfunction of the control center impacts on all the network;
 \item \ul{scalability}: the more routers the network is made up of, the more the work for the control center increases $\Rightarrow$ it is not suitable for wide networks such as Internet.
\end{itemize}

\paragraph{Application} Similar principles are used in telephone networks.

\subsection{Isolated routing}
%65
There is no control center, but all nodes are peer: each node decides its paths autonomously without exchanging information with other routers.

\paragraph{Advantages and disadvantages} They are practically the opposite of the ones of the centralized routing.

%67-68
\subsubsection{Backward learning} Each node learns network information based on packet source addresses:\footnote{Please refer to section \textit{Transparent bridge} in chapter \textit{Repeaters and bridges} in lecture notes `Progetto di reti locali'.}
\begin{itemize}
 \item it works well only with `loquacious' nodes;
 \item it is not easy to realize the need of switching to an alternative path when the best path becomes no longer available;
 \item it is not easy to detect the destinations become unreachable $\Rightarrow$ a special timer is required to delete old entries.
\end{itemize}

\subsection{Distributed routing}
%69
Distributed routing uses a `peer' model: it takes the advantages of centralized routing and the ones of isolated routing:
\begin{itemize}
 \item centralized routing: routers participate in exchange of information regarding connectivity;
 \item isolated routing: routers are equivalent and there is not a `better' router.
\end{itemize}

%70
\paragraph{Applications} Modern routing protocols use two main distributed routing algorithms:
\begin{itemize}
 \item Distance Vector: each node tells all its neighbors what it knows about the network (chapter~\ref{cap:Distance_Vector});
 \item Link State: each node tells all the network what it knows about its neighbors (chapter~\ref{cap:Link_State}).
\end{itemize}

\subsubsection{Comparison}
%148
\paragraph{Neighbors}
\begin{itemize}
 \item LS: needs the `hello' protocol;
 \item DV: knows its neighbors through the DV itself.
\end{itemize}

\paragraph{Routing table} DV and LS create the same routing table, just computed in different ways and with different duration (and behavior) of the transient:
\begin{itemize}
 \item LS: routers cooperate to keep the map of the network up to date, then each of them computes its own spanning tree: each router knows the topology of the network, and knows the precise path to reach a destination;
 \item DV: routers cooperate to compute the routing table: each router knows only its neighbors, and it trusts them for determining the path towards the destination.
\end{itemize}

%149
\paragraph{Simplicity}
\begin{itemize}
 \item DV: single algorithm easy to implement;
 \item LS: incorporates many different components.
\end{itemize}

\paragraph{Debug} Better in LS: each node has the map of the network.

%150
\paragraph{Memory consumption \textmd{(in each node)}} They may be considered equivalent:
\begin{itemize}
 \item LS: each of the $N$ LSs has $A$ adjacencies (Dijkstra: $\sim N\cdot A$);
 \item DV: each of the $A$ DVs has $N$ destinations (Bellman-Ford: $\sim A\cdot N$).
\end{itemize}

\paragraph{Traffic} Better in LS: Neighbor Greeting packets are much smaller than DVs.

\paragraph{Convergence} Better in LS: fault detection is faster because it is based on Neighbor Greeting packets sent with a high frequency.