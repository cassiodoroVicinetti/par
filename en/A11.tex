\chapter{IPv6 routing}
%3
Nowadays routers are mostly ready for IPv6, even though performance in IPv6 is still worse than the one in IPv4 because of lack of experience and lower traffic demand. Often IPv6 routing is turned off by default even if the device supports IPv6 (on Cisco routers this is enabled by command \texttt{ipv6 unicast-routing}).

%2
Two aspects have to be considered:
\begin{itemize}
\item \ul{routing tables}: how to handle the forwarding of data packets? (sect.~\ref{sez:IPv6_routing_tables})
\item \ul{routing protocols}: how to distribute routes across the network? (sect.~\ref{sez:IPv6_routing_protocols})
\end{itemize}

%Tipically routers supporting IPv6 adopt the dual stack `ships in the night'-like approach: IPv4 and IPv6 are supported by two independent stacks for transport layer $ \Rightarrow $ this requires the complete duplication for all components: routing protocols, routing tables, access lists, etc.

\section{Routing tables}
\label{sez:IPv6_routing_tables}
%4
Routing in IPv6 is performed in the same way as IPv4 but it requires two distinct routing tables, one for IPv4 routes and another for IPv6 routes. IPv6 routing tables can store several types of entries, including:
%10
\begin{itemize}
 \item \ul{indirect entries} (O/S codes): they specify the addresses, typically link local, of the interfaces of the next-hop routers to which to send packets addressed towards remote links;
 \item \ul{direct entries}: they specify the interfaces of the router itself through which to send packets addressed towards local links:
 %11
 \begin{itemize}
  \item \ul{connected networks} (C code): they specify the prefixes of the local links;
  \item \ul{interface addresses} (L code): they specify the interface identifiers in the local links.
 \end{itemize}
\end{itemize}

\subsection{Next hop}
%12
As the next hop in computed \textbf{dynamic routes}, routing protocols always use \ul{link local addresses}, even if a global address is configured on the neighbor interface, for the sake of simplicity: link local addresses always exist, while global addresses may not be used in some portions of the network.

%13-14-20
However the usage of link local addresses makes difficult the task of determining the \ul{location} of that address: the network address of a global address at least allows to identify the network in which the host should be present and thus determine the output interface, but a link local address that begins with FE80:: can be everywhere $\Rightarrow$ next to the next hop address, routers also print the output \textbf{local interface} to solve ambiguities, such as:\\
\centerline{\texttt{2001:1::/64 via FE80::1, \textbf{FastEthernet0/0}}}

%21-22
For \textbf{static routes}, the choice is left to the network manager, who can use the address he prefers as the next hop:\\
\centerline{\texttt{ipv6 route \textsl{address}/\textsl{netmask} [\textsl{local interface}] [\textsl{next hop}] [\textsl{distance}]}}
\begin{itemize}
\item \ul{broadcast interface} (e.g. Ethernet): the address of the next hop needs to be specified:
\begin{itemize}
\item \ul{global address}: the local interface does not need to be specified because it can be determined from the network prefix:\\
\centerline{\texttt{ipv6 route 2001:1::/64 2001::1}}
\item \ul{link local address}: the local interface needs to be specified too to identify the scope of the link local address:\\
\centerline{\texttt{ipv6 route 2001:1::/64 \textbf{FastEthernet0/0} fe80::1}}
\end{itemize}

\item \ul{point-to-point interface} (e.g. serial): the address of the next hop does not need to be specified because it is uniquely identified by the local interface:\\
\centerline{\texttt{ipv6 route 2001:1::/64 serial 0}}
\end{itemize}

%15
Since static routes can not adapt to network changes, it is strongly recommended to use \ul{global addresses} as the next hop for static routes. This avoids that a route becomes invalid if the next hop changes: for example, if the network card on the next hop router is replaced because of a hardware fault:
\begin{itemize}
\item link local address: it depends on the MAC address of the card $\Rightarrow$ the route needs to be changed;
\item global address: the new interface should just be assigned the same global address.
\end{itemize}

%5-8
\section{Routing protocols}
\label{sez:IPv6_routing_protocols}
Routing protocols supporting IPv6 can adopt two approaches:
\begin{itemize}
%6
  \item \textbf{integrated routing} (IS-IS, MP-BGP4): the protocol allows to exchange both IPv4 and IPv6 routing information at the same time:
  \begin{itemize}
  \item[\pro] \ul{efficiency}: IPv4 and IPv6 addresses belonging to the same destination can be transported via a single message;
  \item[\con] \ul{flexibility}: a single protocol transports multiple address families;
  \item[\pro] \ul{reactivity}: if a fault or a network change occurs, the protocol discovers it for both address families;
  \item[\con] \ul{bugs}: a problem in the protocol affects IPv4 and IPv6 networks in the same way;
  \item[\con] \ul{migration}: if the protocol uses IPv4 to transport Hello packets, IPv4 can not be abolished in the network;
  \end{itemize}
  
%7
  \item \textbf{ships in the night} (RIPng, EIGRP, OSPFv3): the protocol allows to exchange only IPv6 routing information:
  \begin{itemize}
  \item[\con] \ul{efficiency}: given a destination, a message needs to be exchanged for its IPv4 address and another message for its IPv6 address, and the messages are completely independent of each other;
  \item[\pro] \ul{flexibility}: two different protocols can be used, one for IPv4 routing information and another for IPv6 routing information;
  \item[\con] \ul{reactivity}: if a fault or a network change occurs, both protocols have to discover it, each one with its timings and duplicate messages;
  \item[\pro] \ul{bugs}: a problem in the protocol does not affect routing in the other one;
  \item[\pro] \ul{migration}: each routing protocol generates messages of the address family it belongs to.
  \end{itemize}
\end{itemize}

\subsection{RIPng}
%27
\textbf{RIPng} adopts the `ships in the night' approach, and makes improvements to RIP mainly in Cisco command-line interface:
\begin{itemize}
\item \ul{support for multiple instances}: the \texttt{tag} field allows to specify the protocol instance;\footnote{Support for multiple instances was already present in RIPv2, but was not configurable on Cisco routers.}
%28
\item \ul{per-interface configuration}: new commands have been introduced:
\begin{itemize}
\item \texttt{ipv6 rip \textsl{\textless tag\textgreater} enable}: it replaces the \texttt{network} command and automatically configures RIP on that interface without having to specify an address;
\item \texttt{ipv6 rip \textsl{\textless tag\textgreater} default-information originate}: it originates the default route (::/0), that is the rest of the world can be reached via this interface.
\end{itemize}
\end{itemize}

\subsection{OSPFv3}
\label{sez:OSPFv3}
\textbf{OSPFv3} adopts the `ships in the night' approach, and differs from OSPF mainly for three aspects:
\begin{itemize}
%49-50
\item \ul{per-interface configuration}: the \texttt{ipv6 ospf \textsl{\textless process ID\textgreater} area \textsl{\textless area ID\textgreater}} has been introduced which specifies that all the networks and the addresses configured on this interface will be advertized as belonging to the specified area;
%54-55
\item \ul{Router ID}: unfortunately OSPFv3 still uses a 32-bit-long Router ID, which it is not even able to automatically set when no IPv4 addresses is available $\Rightarrow$ the \texttt{ipv6 router-id \textsl{\textless number\textgreater}} command becomes compulsory when the router is IPv6-only or is in an IPv6-only network;
%68
\item \ul{tunnel}: a IPv6-on-IPv4 tunnel can be configured to connect together IPv6 islands through an IPv4 network.
\end{itemize}

\subsection{IS-IS}
\textbf{IS-IS} for IPv6 adopts the `integrated routing' approach: in fact it uses its own layer-3 protocol to transport protocol-specific packets, independently of the underlying IP protocol version.

\subsection{MP-BGP4}
\textbf{MP-BGP4} can adopt both approaches depending on configuration: TCP packets can be enveloped into IPv4 or IPv6 as needed.

The most common deployment follows the `integrated routing' approach, because of the need to use the AS number (which is the same for both IPv4 and IPv6) for the BGP process. Integration also reflects to policies: IPv4 addresses and IPv6 addresses can be mixed at will.