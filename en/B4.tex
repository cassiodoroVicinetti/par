\chapter{Software-based packet filtering}
Software which is able to parse fields in packets finds place, running on integrated circuits or microprocessors, in a variety of applications:
\begin{itemize}
 \item \ul{switch}: learning algorithms are based on frame source and destination MAC addresses\footnote{Please refer to section \textit{Transparent bridge} in chapter \textit{Repeaters and bridges} in lecture notes `Progetto di reti locali'.}, and frame forwarding is based on destination MAC addresses;
 \item \ul{router}: packet forwarding is based on source and destination IP addresses;
 \item \ul{firewall}: if a rule based on packet fields is matched, it throws the associated filtering action (e.g. drop);
 \item \ul{NAT}: it converts IP addresses between private and public and TCP/UDP ports for every packet in transit;\footnote{Please refer to chapter \textit{NAT} in lecture notes `Reti di calcolatori'.}
 \item \ul{URL filter}: it blocks HTTP traffic from/to URLs of websites in a black list;
 \item \ul{protocol stack}: the operating system delivers the packet to the proper network-layer stack (e.g. IPv4 or IPv6), then the packet goes to the proper transport-layer stack (e.g. TCP or UDP), at last based on the quintuple identifying the session the packet is made available to the application through the right socket;
 \item \ul{packet capture}: applications for traffic capture (e.g. Wireshark, tcpdump) can set a filter to reduce the amount of captured packets.
\end{itemize}

\section{Typical architecture of a packet filtering system}
\begin{figure}
 %3
 \centering
 \includegraphics[scale=1.05]{../pic/B4/3.pdf}
 \caption{Typical architecture of a packet filtering system.}
\end{figure}

\paragraph{Kernel-level components}
\begin{itemize}
%5
\item \textbf{network tap}: it intercepts packets from the network card and delivers them to one or more\footnote{Each capture application has its own filtering stack, but all of them share the same network tap.} filtering stacks;
%6
\item \textbf{packet filter}: it allows only packets satisfying the filter specified by the capture application to pass, increasing the capture efficiency: unwanted packets are immediately discarded, and a smaller number of packets is copied into the kernel buffer;
%9
\item \textbf{kernel buffer}: it stores packets before they are delivered to the user level;
%12
\item \textbf{kernel-level API}: it provides the user level with the primitives, typically \texttt{ioctl} system calls, needed to access underlying layers.
\end{itemize}

\paragraph{User-level components}
\begin{itemize}
%13
\item \textbf{user buffer}: it stores packets into the address space of the user application;
%14-15-17
\item \textbf{user-level library} (e.g. libpcap, WinPcap): it exports functions which are mapped with the primitives provided by the kernel-level API, and provides a high-level compiler to create on the fly the pseudo-assembler code to be injected into the packet filter.
\end{itemize}
\FloatBarrier

\section{Main packet filtering systems}
\subsection{CSPF}
%16
\textbf{CMU/Stanford Packet Filter} (CSPF, 1987) was the first packet filter, and was implemented in parallel with the other protocol stacks.

It introduced some key improvements:
\begin{itemize}
 %4
 \item \ul{implementation at kernel level}: processing is faster because the cost for context switches between kernel space and user space is avoided, although it is easier to corrupt the entire system;
 %10
 \item \textbf{packet batching}: the kernel buffer does not delivers immediately a packet arrived at the application, but waits for a number to be stored and then copies them all together into the user buffer to reduce the number of context switches;
 %7
 \item \textbf{virtual machine}: filters are no longer hard-coded, but the user-level code can instantiate at run time a piece of code in pseudo-assembler language specifying the filtering operations to determine if the packet can pass or must be discarded, and a virtual machine in the packet filter, made up in practice of a \texttt{switch case} over all the possible instructions, emulates a processor which interprets that code for each packet in transit.
\end{itemize}

\subsection{BPF/libpcap}
%18
\textbf{Berkeley Packet Filter} (BPF, 1992) was the first serious implementation of a packet filter, adopted historically by BSD systems and still used today coupled with the \textbf{libpcap} library in user space.

\paragraph{Architecture}
\begin{itemize}
%5
\item \ul{network tap}: it is integrated in the NIC driver, and can be called by explicit calls to capture components;
%11
\item \ul{kernel buffer}: it is split into two \ul{separate memory areas}, so that kernel-level and user-level processes can work independently (the first one writes while the second one is reading) without the need for synchronization exploiting two CPU cores in parallel:
\begin{itemize}
 \item the \textbf{store buffer} is the area where the kernel-level process writes into;
 \item the \textbf{hold buffer} is the area where the user-level process reads from.
\end{itemize}
\end{itemize}

%http://www.winpcap.org/docs/docs_412/html/group__NPF.html
\subsection{NPF/WinPcap}
%19
The \textbf{WinPcap} library (1998), initially developed at the Politecnico di Torino, can be considered as a porting for Windows of the entire BPF/libpcap architecture.

%20
\paragraph{Architecture}
\begin{itemize}
 %21
 \item \textbf{Netgroup Packet Filter} (NPF): it is the kernel-level component and includes:
 \begin{itemize}
  %5
  \item \ul{network tap}: it sits on top of the NIC driver, registering itself as a new network-layer protocol next to stardard protocols (such as IPv4, IPv6);
  %24-25
  \item \ul{packet filter}: the virtual machine is a \textbf{just in time} (JIT) \textbf{compiler}: instead of interpreting the code, it translates it into x86-processor-native instructions;
  %11
  \item \ul{kernel buffer}: it is implemented as a \textbf{circular buffer}: kernel-level and user-level processes write to the same memory area, and the kernel-level process overwrites the data already read by the user-level process $\Rightarrow$ it optimizes the \ul{space} where to store packets, but:
  \begin{itemize}
 \item if the user-level process is too slow in reading data, the kernel-level process may overwrite data not yet read (\textbf{cache pollution}) $\Rightarrow$ \ul{synchronization} between the two processes is needed: the writing process needs to periodically inspect a shared variable containing the current read position;
 \item the memory area is shared among the CPU cores $\Rightarrow$ the circular buffer is \ul{less CPU efficient};
  \end{itemize}
 \end{itemize}
 
 %22
 \item \textbf{Packet.dll}: it exports at the user level functions, independent of the operating system, which are mapped with the primitives provided by the kernel-level API;
 
 %23
 \item \textbf{Wpcap.dll}: it is the dynamic-link library with which the application directly interacts:
 \begin{itemize}
  \item it offers to the programmer high-level \ul{library functions} needed to access underlying layers (e.g. \texttt{pcap\_open\_live()}, \texttt{pcap\_setfilter()}, \texttt{pcap\_next\_ex()}/\texttt{pcap\_loop()});
  \item it includes the \ul{compiler} which, given a user-defined filter (e.g. string \texttt{ip}), creates the pseudo-assembler code (e.g. ``if field `EtherType' is equal to 0x800 return true'') to be injected into the packet filter for the JIT compiler;
  \item it implements the \ul{user buffer}.
 \end{itemize}
\end{itemize}

%19
\paragraph{New features}
\begin{itemize}
 \item \textbf{statistics mode}: it records statistical data in the kernel without any context switch;
 \item \textbf{packet injection}: it sends packets through the network interface;
 \item \textbf{remote capture}: it activates a remote server which captures packets and delivers them locally.
\end{itemize}

\section{Performance optimizations}
\begin{figure}
%37
\centering
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[scale=1.1]{../pic/B4/37a}
		\caption{Traditional architecture.}
	\end{subfigure}%
	\hspace{0.03\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
		\centering
		\includegraphics[scale=1.1]{../pic/B4/37b}
		\caption{Architecture with shared buffer.}
	\end{subfigure}%
	\hspace{0.03\textwidth}
	\begin{subfigure}[b]{.3\textwidth}
	%TODO
	%TODO
		\centering
		\includegraphics[scale=1.1]{../pic/B4/37c}
		\caption{Architecture with accelerated driver.}
	\end{subfigure}
	\caption{Evolution of performance optimization techniques.}
\end{figure}

%28
\noindent
In recent years network traffic has grown faster than computer performance (memory, CPU). Packet processing performance can be improved in various ways:
\begin{itemize}
\item \ul{increase capture performance}: improve the capacity of delivering data to software;
\item \ul{create smarter analysis components}: only the most interesting data are delivered to software (e.g. URL for a URL filter);
\item \ul{optimize architecture}: try to exploit application characteristics to improve performance.
\end{itemize}

%34
\paragraph{Profiling data} (WinPcap 3.0, 64-byte-long packets)
\begin{itemize}
\item[\con] {\small [49.02\%]} \ul{NIC driver and operating system}: when entering the NIC, the packet takes a lot of time just to arrive at the capture stack:
\begin{enumerate}
\item the NIC transfers the packet into its kernel memory area via DMA (this does not use the CPU);
\item the NIC throws an \textbf{interrupt} (IRQ) to the NIC driver, stopping the currently running program;
\item the NIC driver copies the packet from the NIC memory into a kernel memory area of the operating system (this uses the CPU);
\item the NIC driver invokes the operating system giving it the control;
\item the operating system calls the various registered protocol stacks, including the capture driver;
\end{enumerate}
\item[\con] {\small [17.70\%]} \ul{tap processing}: operations performed by the capture driver at the beginning of the capture stack (e.g. receiving packets, setting interrupts);
\item[\con] {\small [8.53\%]} \ul{timestamping}: the packet is associated its timestamp;
\item[\pro] {\small [3.45\%]} \ul{packet filter}: filter costs are proportionally low thanks to the JIT compiler;
\item[\con] \ul{double copy into buffers}: the more packets are big, the more copy costs increase:
\begin{enumerate}
\item {\small [9.48\%]} kernel buffer copy: the packet is copied from the operating system memory to the kernel buffer;
\item {\small [11.50\%]} user buffer copy: the packet is copied from the kernel buffer to the user buffer;
\end{enumerate}
\item[\pro] {\small [0.32\%]} \ul{context switch}: it has an insignificant cost thanks to packet batching.
\end{itemize}

\subsection{Interrupts}
%32-33
In all operating systems, at a certain input rate the percentage of packets arriving at the capture application not only does no longer increase, but drastically decreases because of \textbf{livelock}: interrupts are so frequent that the operating system has no time for reading packets from the NIC memory and copying them into the kernel buffer in order to deliver them to the application $\Rightarrow$ the system is alive and is doing some work, but is not doing some useful work.

%35
Several solutions exist to cut down interrupt costs:
\begin{itemize}
\item \textbf{interrupt mitigation} (hardware-based): an interrupt is triggered only when a certain number of packet has been received (a timeout avoids starvation if the minimum threshold has not been achieved within a certain time);
\item \textbf{interrupt batching} (software-based): when an interrupt arrives, the operating system serves the arrived packet and then works in polling mode: it immediately serves the following packets arrived in the meanwhile, until there are no more packets and the interrupt can be enabled back on the card;
\item \textbf{device polling} (e.g. BSD [Luigi Rizzo]): the operating system does no longer wait for an interrupt, but autonomously checks by an infinite loop the NIC memory $\Rightarrow$ since a CPU core is perennially busy in the infinite loop, this solution is suitable when really high performance is needed.
\end{itemize}

%36
\subsection{Timestamping}
Two solutions exist to optimize timestamping:
\begin{itemize}
\item \ul{approximate timestamp}: the actual time is read just sometimes, and the timestamp is based on the number of clock cycles elapsed since the last read $\Rightarrow$ the timestamp depends on the processor clock rate, and processors are getting greater and greater clock rates;
\item \ul{hardware timestamp}: the timestamp is directly implemented in the network card $\Rightarrow$ packets arrive at software already having their timestamps.
\end{itemize}

\subsection{User buffer copy}
The kernel memory area where there is the kernel buffer is \ul{mapped to user space} (e.g. via \texttt{nmap()}) $\Rightarrow$ the copy from the kernel buffer to the user buffer is no longer needed: the application can read the packet straight from the \textbf{shared buffer}.

\paragraph{Implementation} This solution has been adopted in nCap by Luca Deri.

\paragraph{Issues}
\begin{itemize}
%TODO
\item \ul{security}: the application accesses kernel memory areas $\Rightarrow$ it may damage the system;

\item \ul{addressing}: the kernel buffer is seen through two different addressing spaces: the addresses used in kernel space are different from the addresses used in user space;
\item \ul{synchronization}: the application and the operating system need to work on shared variables (e.g. data read and write positions).
\end{itemize}

%38
\subsection{Kernel buffer copy}
The operating system is not made to support large network traffic, but has been engineered to run user applications with limited memory consumption. Before arriving at the capture stack, each packet is stored into a memory area of the operating system which is dynamically allocated as a linked list of small buffers (\texttt{mbuf} in BSD and \texttt{skbuf} in Linux) $\Rightarrow$ costs for mini-buffer allocation and freeing is too onerous with respect to a large, statically allocated buffer where to store packets of any size.

Capture-dedicated cards are no longer seen by the operating system, but use \textbf{accelerated drivers} incorporated into the capture stack: the NIC copies the packet into its kernel memory area, and the application reads straight from that memory area without the intermediation of the operating system.

%43
\paragraph{Implementations} This solution is adopted in netmap by Luigi Rizzo and DNA by Luca Deri.

\paragraph{Issues}
\begin{itemize}
\item \ul{applications}: the other protocol stacks (e.g. TCP/IP stack) disappeared $\Rightarrow$ the machine is completely dedicated to capture;
\item \ul{operating system}: an intrusive change to the operating system is required;
\item \ul{NIC}: the accelerated driver is strongly tied to the NIC $\Rightarrow$ another NIC model can not be used;
\item \ul{performance}: the bottleneck remains the bandwidth of the PCI bus;
\item \ul{timestamp}: it is not precise because of delays due to software.
\end{itemize}

\subsection{Context switch}
%TODO
Processing is moved to kernel space, avoiding the context switch to user space.

\paragraph{Implementation} This solution has been adopted in Intel Data Plane Development Kit (DPDK), with the purpose of making network devices programmable via software on Intel hardware.

\paragraph{Issues}
\begin{itemize}
\item \ul{packet batching}: the context switch cost is ridiculous thanks to packet batching;
\item \ul{debug}: it is easier in user space;
\item \ul{security}: the whole application works with kernel memory;
\item \ul{programming}: it is more difficult to write code in kernel space.
\end{itemize}

%39
\subsection{Smart NICs}
%TODO
Processing is performed directly by the NIC (e.g. Endace):
\begin{itemize}
\item \ul{hardware processing}: it avoids the bottleneck of the PCI bus, limiting data displacement (even though the performance improvement is limited);
\item \ul{timestamp precision}: there is no delay due to software and it is based on GPS $\Rightarrow$ these NICs are suitable for captures over geographically wide networks.
\end{itemize}

%40
\subsection{Parallelization in user space}
FFPF proposed an architecture which tries to exploit the application characteristics to go faster, by increasing \ul{parallelism in user space}: the capture application is multi-thread and runs on multi-core CPUs.

Hardware can help parallelization: the NIC can register itself to the operating system with multiple adapters, and each of them is a distinct logical queue from which packets exit depending on their classification, performed by \textbf{hardware filters}, based on their fields (e.g. MAC addresses, IP addresses, TCP/UDP ports) $\Rightarrow$ multiple pieces of software can read from distinct logical queues in parallel.

\paragraph{Applications}
\begin{itemize}
\item \ul{receive side scaling} (RSS): the classification is based on the session identifier (quintuple) $\Rightarrow$ all the packets belonging to the same session will go to the same queue $\Rightarrow$ load on web servers can be balanced;\footnote{Please see section~\ref{sez:CDN_SLB}.}
\item \ul{virtualization}: each virtual machine (VM) on a server has a different MAC address $\Rightarrow$ packets will directly enter the right VM without being touched by the operating system (hypervisor).\footnote{Please refer to section~\ref{sez:NFV}.}
\end{itemize}
%\FloatBarrier