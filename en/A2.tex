\chapter{Routing Information Protocol}
%3
\textbf{Routing Information Protocol} (RIP) is an intra-domain routing protocol based on the Distance Vector (DV) algorithm. RIP version 1 was defined in 1988 and was the first routing protocol used on the Internet.

RIP, depending on the implementation, includes split horizon, route poisoning and path hold down mechanisms to limit propagation of incorrect routing information.

%23
RIP is suitable for small, stable and homogeneous networks:
 \begin{itemize}
  \item \ul{small}: the metric is simply based on the \textbf{hop count} (each link has cost 1), but the 16-hop limit can not be exceeded $\Rightarrow$ more than 15 routers in a cascade within a same RIP domain are not allowed;
  \item \ul{stable}: status changes may trigger long-lasting transients;
  \item \ul{homogeneous}:
  \begin{itemize}
  \item homogeneous links: costs on different links can not be differentiated based on bandwidth;
  \item homogeneous routers: every router needs to finish processing before producing its new DV $\Rightarrow$ the transient duration is bound to performance of the slowest router.
 \end{itemize}
\end{itemize}

\section{Packet format}
%4
RIP packets have the following format:
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c}
    \multicolumn{1}{r@{}}{\footnotesize{8}} & \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{32}} & \multicolumn{1}{}{} \\
    \cline{1-3}
    Command & Version (1) & \makebox[3cm]{0} & \\
    \hline
    \multicolumn{2}{|c|}{Address Family Identifier} & \makebox[3cm]{0} & \parbox[t]{0mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\footnotesize{$\times$ N $\leq$ 25}}}} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{IP Address} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{0} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{0} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Metric} \\
    \hline
  \end{tabular}
  \caption{Format of a RIP packet (24 to 504 bytes).}
\end{table}
%5
\noindent
where the most significant fields are:
\begin{itemize}
 \item \ul{Command} field (1 byte): it specifies the type of message:
 \begin{itemize}
  \item `Response' value: the packet is transporting a DV containing one or more addresses;
  \item `Request' value: a router newly connected to the network is notifying its neighbors of its presence $\Rightarrow$ neighbors will send back their DVs without having to wait the timeout, increasing the convergence speed;
 \end{itemize}
 \item \ul{Address Family Identifier} field (2 bytes): it specifies the network-layer protocol being used (e.g. value 2 = IP);
 \item \ul{IP Address} field (4 bytes): it specifies the IP address being announced (without netmask).\\
 Up to 25 addresses can be announced in a single RIP packet;
 \item \ul{Metric} field (4 bytes): it specifies the cost related to the announced address.
\end{itemize}

%6
\paragraph{Encapsulation} The RIP packet is encapsulated into an UDP packet:
\begin{itemize}
 \item the destination \ul{UDP port} is port 520, which at the time was chosen as a security mechanism since ports lower than 1024 can be used only under administrative privileges;
 \item the destination \ul{IP address} is the broadcast address (255.255.255.255) $\Rightarrow$ all devices can receive it, including hosts, although it is better to disable routing protocols on the host side to protect them from malicious attacks and learn better routes by listening to possible ICMP Redirect messages:
 \begin{figure}[H]
	\centering
	\includegraphics[width=0.35\linewidth]{../pic/A2/6.png}
 \end{figure}
\end{itemize}

\section{Timers}
RIP is heavily based on timers:
\begin{itemize}
 \item it is difficult to precisely comply with fixed timers because the CPU may be busy $\Rightarrow$ uncertainties introduce further delays;
 \item all the routers within the network must use the same timers, otherwise routers may interact in an uncoordinated way.
\end{itemize}

%7
\subsection[Routing update timer]{Routing update timer \textmd{{\small (default 30 s)}}}
It defines how often gratuitous Response messages containing information about DVs are sent.

%10
\paragraph{Router synchronization} It is tried to be avoided by not resetting the routing update timer on sending a triggered update and by sending gratuitous Response messages with a variable delay between 25 and 35 seconds.

\subsection[Route invalid timer]{Route invalid timer \textmd{{\small (default 180 s)}}}
It defines how long an entry can keep being valid in the routing table without being refreshed. When the router invalid timer expires, the hop count of the entry is set to cost infinity (16), marking the destination as unreachable.

\paragraph{Fault detection} The router invalid timer is useful especially to detect a missing connectivity toward a neighbor when the `link down' signal is not available.

%8
\subsection[Route flush timer]{Route flush timer \textmd{{\small (default 240 s)}}}
It defines how long an entry can stay in the routing table without being refreshed. When the route flush timer expires, the entry is deleted from the routing table.

%12
\paragraph{Route poisoning} When the route invalid timer expires and the entry is marked as invalid, 60 s (with default values) are left when the router can announce the destination at cost infinity, to inform the other routers before the entry is deleted.

%13
\subsection[Hold down timer]{Hold down timer \textmd{{\small (default 180 s)}}}
It defines how long an entry is not subject to changes following a suspected start of count of infinity. The hold down timer is a proprietary feature by Cisco.

\paragraph{Path hold down} The hold down timer starts when the hop count is rising to a higher value, to avoid triggering a count to infinity and allow the route to get stable.

%12
\paragraph{Route poisoning} Also a destination blocked by the path hold down algorithm can be propagated at cost infinity.

\section{Limitations}
\subsection{Netmask}
The first version of RIP was defined when \textbf{classful addressing}, where the subnet mask can be automatically obtained from the network address itself, was still in use $\Rightarrow$ network addresses announced in DVs lack information about their netmasks $\Rightarrow$ version 1 of RIP can be used only in networks where each address belongs to an \textbf{address class} according to the old classful addressing rules.

A stratagem can be adopted to make version 1 of RIP work in networks with variable-length netmask addresses: given an announced network address, the router scans the network addresses assigned to its connected interfaces:
\begin{itemize}
 \item if at least an interface is assigned an address having a \ul{subnet mask} equal to the subnet mask of the announced address, the router assumes the netmask from the address of the interface as a netmask for the announced address;
 \item if no interface is assigned an address having a subnet mask equal to the subnet mask of the announced address, the router assumes its subnet mask as a netmask for the announced address.
\end{itemize}

\paragraph{Issues} A wrong netmask could be assumed if:
\begin{itemize}
 \item no one of the interfaces of the router has the subnet mask being searched for;
 \item the announced address really has a netmark other than the one of the address of the selected interface.
\end{itemize}

%9
\subsection{Hop count limit}
RIP defines the hop count limit equal to 16 $\Rightarrow$ destinations whose distance is larger than 15 are considered unreachable.

Such a low maximum value was chosen to limit the well-known problem of count to infinity of DV-based algorithms: when a route cost reaches value 16, the route is considered unreachable and its cost can not rise even more.

This does \ul{not} mean that the network can not have more than 15 routers in a cascade: the only effect is that two routers too far away can not communicate directly one with each other. This problem can be solved by partitioning the network into two routing domains, handled by two different RIP protocol instances, and by enabling the redistribution process between them so as to `falsify' costs for external routes.

%14
\subsection{Lack of `age' field}
RIP does not associate an `age' field to routes in DVs $\Rightarrow$ the announced information could be old, but the receiving router assumes it as new and resets its timers to zero $\Rightarrow$ the more one is far from the status change, the more the transient duration increases.

\paragraph{Example} In a network with topology A\textemdash B\textemdash C:\footnote{It is assumed: default timer values, no triggered updates, no route poisoning.}
\begin{itemize}
 \item time 0 s: a failure on link A\textemdash B occurs $\Rightarrow$ node A is no longer reachable;
 \item time 179 s: node B announces to node C its DV, the last one including destination A;
 \item time 180 s: node B marks destination A as unreachable;
 \item time 359 s: node C marks destination A as unreachable.
\end{itemize}

\section{RIP version 2}
%16
\textbf{RIP version 2} extends the first version of RIP by exploiting some fields which were unused in messages:
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c}
    \multicolumn{1}{r@{}}{\footnotesize{8}} & \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{32}} & \multicolumn{1}{}{} \\
    \cline{1-3}
    Command & Version (2) & Routing Domain & \\
    \hline
    \multicolumn{2}{|c|}{Address Family Identifier} & Route Tag & \parbox[t]{0mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{\footnotesize{$\times$ N $\leq$ 25}}}} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{IP Address} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Subnet Mask} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Next Hop} \\
    \cline{1-3}
    \multicolumn{3}{|c|}{Metric} \\
    \hline
  \end{tabular}
  \caption{Format of a RIP packet version 2 (24 to 504 bytes).}
\end{table}
%15
\noindent
where the new fields are:
\begin{itemize}
 \item \ul{Routing Domain} field (2 bytes): it specifies the routing domain for which this RIP message is intended to handle multiple routing domain on the same border router:
 \begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{../pic/A2/15}
	\caption{Router A discards messages intended for domain B.}
 \end{figure}
 %17
 \item \ul{Route Tag} field (2 bytes): it specifies if the announced address is an \textbf{external route}, that is it was learnt through a redistribution process from another routing domain;
 \item \ul{Subnet Mask} field (4 bytes): it contains the \textbf{netmask} associated to the announced network address to support classless addressing;
 %18
 \item \ul{Next Hop} field (4 bytes): it optimizes routing when multiple RIP routers belong to the same LAN but to two different RIP domains, and therefore traffic from a domain to another one would always cross the border router $\Rightarrow$ the border router can announce to send traffic to the next hop router in the other domain:
 \begin{figure}[H]
	\centering
	\includegraphics[scale=0.97125]{../pic/A2/18}
	\caption{Border router B teaches router A to use router C as its next hop for destination D.}
 \end{figure}
\end{itemize}

%19
\subsection{Authentication}
RIP version 2 introduces a \textbf{password-based authentication} mechanism: a router must be authenticated in order to be able to announce its DV to its neighbors.

If the first entry in the RIP packet has the `Address Family Identifier' field equal to value 0xFFFF, then the remainder of the entry contains authentication information:
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|}
    \multicolumn{1}{r@{}}{\footnotesize{16}} & \multicolumn{1}{r@{}}{\footnotesize{32}} \\
    \hline
    0xFFFF & Authentication Type \\
    \hline
    \multicolumn{2}{|c|}{} \\
    \hdashline
    \multicolumn{2}{|c|}{Authentication} \\
    \hdashline
    \multicolumn{2}{|c|}{} \\
    \hdashline
    \multicolumn{2}{|c|}{} \\
    \hline
  \end{tabular}
  \caption{Format of the authentication entry in a RIP packet.}
\end{table}
\noindent
where fields are:
\begin{itemize}
 \item \ul{Authentication Type} field (2 bytes): any type `simple password' has been defined (value 2);
 \item \ul{Authentication} field (16 bytes): it contains the password in clear text.
\end{itemize}

This authentication mechanism is quite weak because the password can be easily sniffed $\Rightarrow$ it is rarely used. More complex authentication mechanisms are not possible because of the lack of space in the RIP message.

%20
\subsection{Multicast}
RIP version 1 sends DV in broadcast $\Rightarrow$ all entities, including hosts, have to process RIP messages.

RIP version 2 defines a destination multicast IP address (224.0.0.9), so that the RIP packet is received only by entities which have subscribed to the \textbf{multicast group} $\Rightarrow$ hosts and routers not using the RIP protocol can discard the packet at the data-link layer.

%23
\section{Advantages}
\begin{itemize}
 \item it is suitable for small, stable and homogeneous networks;
 \item it requires few processing resources;
 \item it is simple to implement;
 \item it is simple to configure (there are no subdomains like in OSPF);
 \item it is available on a wide range of devices, even on cheap routers.
\end{itemize}