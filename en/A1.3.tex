\chapter{The Distance Vector algorithm}
\label{cap:Distance_Vector}
%72
The \textbf{Distance Vector} (DV) algorithm is based on distribution of information about the whole network within the neighborhood of the router.

%73
Every router periodically generates a DV, that is a set of destination-cost pairs:
\begin{itemize}
 \item \ul{destination}: all the destinations known by the generating router (%
 %96
 in real IP networks they are network addresses with netmask);
 \item \ul{cost}: the cost of the path from the generating router to the destination.
\end{itemize}

%74
The receiving router learns from each DV:
\begin{itemize}
 \item \ul{reachable destinations}: they are added to the ones already known locally;
 \item \ul{direction}: those destinations are reachable through the generating router;
 \item \ul{cost}: the one reported by the generating router plus the cost of the link between the receiving router and the generating router.
\end{itemize}

%75-76
Each node stores all the DVs coming from its neighbors, and integrates them by selecting the best costs for every destination in order to build its routing table and its DV:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.66\linewidth]{../pic/A1.3/76}
	\caption{Process generating the routing table and the new DV for node A.}
\end{figure}

%77
\section{Basic algorithm}
\begin{itemize}
 \item main process:
 \begin{enumerate}
  \item the DV is announced to adjacent routers;
  \item it waits for timeout;
  \item it comes back to step 1;
 \end{enumerate}
 \item upon receiving a new DV:
 \begin{enumerate}
  \item the DV is saved into memory;
  \item the DV is merged with stored DVs;
 \end{enumerate}
 %79
 \item upon failure of a link (detected at the physical layer):
 \begin{enumerate}
  \item all the DVs coming from that link are deleted;
  \item remaining DVs are merged;
 \end{enumerate}
 \item when a DV has not been received within timeout:
 \begin{enumerate}
  \item the missing DV is deleted;
  \item remaining DVs are merged.
 \end{enumerate}
\end{itemize}

\paragraph{Remarks}
\begin{itemize}
 \item \ul{reliability}: timeouts avoid the use of link-up signals that may not be always available (e.g. if the failure occurs beyond a hub);
 \item \ul{efficiency}: on a link failure, the router gets its new routing table without exchanging any DVs with its adjacent nodes;
 \item \ul{convergence speed}: when a router changes its DV, it does not announce it until the next timeout of the main process (no triggered updates).
\end{itemize}

\section{Triggered updates}
%80
A router can send its updated DV as soon as it updates its routing table, without waiting for the default timeout, to improve convergence time. It can announce either the entire DV or, like it is more frequent in real implementations, just the changed routes.

%81
The triggered update does not reset the timeout of the main process, to avoid that routers start generating DVs at the same time (\textbf{synchronization}).

\section{Count to infinity}
\label{sez:count_to_infinity}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{../pic/A1.3/83}
	\caption{Example of count to infinity.}
	\label{fig:A1_3_esempiocti}
\end{figure}

%83
\noindent
A \textbf{count to infinity} is triggered when the cost to reach a destination, which is no longer reachable, is progressively increased to the infinity.

\paragraph{Example} In figure~\ref{fig:A1_3_esempiocti}, a failure on the link between A and B triggers a count to infinity:
\begin{enumerate}
\item B detects the failure at the physical layer and deletes the DV from A, but C is not able to detect the failure at the physical layer;
\item C announces to B it can reach A through a path of cost 2, which really was the old one crossing B;
\item B updates the DV from C, appearing that A became reachable through an alternative path at cost 3 crossing C;
\item B in turn sends its DV to C, which updates it and increase the cost to 4, and so on.
\end{enumerate}
\FloatBarrier

%84
\paragraph{Effect} B thinks it can reach A through C, while C thinks it can reach A through B $\Rightarrow$ a packet which is directed to A starts bouncing between B and C (bouncing effect) saturating the link between B and C until its TTL goes down to 0.

%82-86
\paragraph{Cause} Unlike black hole and routing loop, count to infinity is a specific problem of the DV algorithm, due to the fact that the information included in the DV does not consider the network topology.

\paragraph{Possible solutions}
\begin{itemize}
\item \ul{threshold for infinity}: upper bound to count to infinity;
%87-95
\item additional algorithms: they prevent count to infinity, but they make the protocol heavier and tend to reduce its reliability because they can not foresee all the possible failures:
\begin{itemize}
\item \ul{route poisoning}: bad news is better than no news;
\item \ul{split horizon}: if C reaches destination A through B, it does not make sense for B to try to reach A through C;
\item \ul{path hold down}: let the rumors calm down waiting for the truth.
\end{itemize}
\end{itemize}

%85
\subsection{Threshold for infinity}
\label{sez:soglia_infinito}
A \ul{threshold value} can be defined: when the cost reaches the threshold value, the destination is considered no longer reachable.

For example RIP has a threshold value equal to 16: more than 15 routers in a cascade can not be connected.

Protocols with complex metrics (e.g. IGRP) require a very high threshold value to consider differentiated costs: for example a metric based on bandwidth may result in a wide range of cost values.

If the bouncing effect takes place on a low-cost link, it is required too much time to increase costs up to the threshold value $\Rightarrow$ \ul{two metrics} at the same time can be used:
\begin{itemize}
\item a metric for path costs (e.g. based on link bandwidth);
\item a metric for count to infinity (e.g. based on hop count).
\end{itemize}

When the metric used for count to infinity returns `infinity', the destination is considered unreachable whatever the path cost is.

%94
\subsection{Route poisoning}
\label{sez:route_poisoning}
The router which detected the failure propagates the destinations no longer reachable with cost equal to infinity $\Rightarrow$ the other routers hear about the failure and in turn propagate the `poisoned' information.

\subsection{Split horizon}
%88
Every router differentiates DVs sent to its neighbors: in each DV it omits the destinations which are reachable through a path crossing the neighbor to which it is sending it $\Rightarrow$ it does not trigger `ghost' paths to appear toward a destination no longer reachable after sending obsolete information in the DV.

\paragraph{Characteristics}
\begin{itemize}
 \item it avoids count to infinity between two nodes (%
 %91-92-108
 except in case of particular loops);
 \item it improves convergence time of the DV algorithm;
 \item routers have to compute a different DV for each link.
\end{itemize}

\subsubsection{Split horizon with poisoned reverse}
\label{sez:poisoned_reverse}
%89
In real implementations, the DV may be fragmented into multiple packets $\Rightarrow$ if some entries in the DV are omitted, the receiving node does not know whether those entries were intentionally omitted by the split horizon mechanism or the packets where they were included went lost.

%90
In split horizon with poisoned reverse, destinations instead of being omitted are transmitted anyway but `poisoned' with infinite cost, so the receiving node is sure it has received all the packets composing the DV $\Rightarrow$ this increases convergence time.

%93
\subsection{Path hold down}
If the path toward a destination increases its cost, it is likely to trigger a count to infinity $\Rightarrow$ that entry is `frozen' for a specific period of time waiting for the rest of the network to find a possible alternative path, whereupon if no one is still announcing that destination it will be considered unreachable and its entry will be deleted.

\section{DUAL}
\label{sez:dual}
%99
\textbf{Diffusing Update Algorithm} (DUAL) is an additional algorithm that aims at improving the scalability of the DV algorithm by guaranteeing the absence of routing loops even during the transient:
%100-101
\begin{itemize}
 %109
 \item \ul{positive status change}: if any neighbor node announces an alternative path with a lower cost, it is immediately accepted because definitely it will not cause a routing loop;
 \item \ul{negative status change}: if
 %112
 \begin{itemize}
 %110
 \item either the current next hop announces the increase of the current route (worsening announces by other neighbor nodes are ignored),
 %111
 \item or the router detects at the physical layer a fault on the link belonging to the current route
 \end{itemize}
 then the DUAL algorithm must be activated:
 %102
  \begin{enumerate}
   \item \textbf{selection of a feasible successor}: another neighbor is selected only if it guarantees that the alternative path across it will not cause routing loops;
   \item \textbf{diffusing process}: if no feasible successors can be found, the node enters a sort of `panic mode' and asks its neighbors for help, waiting for someone to report a feasible path toward that destination.
  \end{enumerate}
\end{itemize}

\subsection{Selection of a feasible successor}
%103
If the current route is no longer available due to a negative status change, an alternative path is selected only if it can be proved that the new path does not create loops, that is if it is certain that the new next hop does not use the node itself to reach the destination.

%104
A neighbor node $K$ is a \textbf{feasible successor} for router $R$ if and only if its distance toward destination $D$ is smaller than the distance that router $R$ had before the status change:
\[
 d \left( K,D \right) < d \left( R,D \right)
\]

%105-106
This guarantees that neighbor $K$ can reach destination $D$ by using a path that does not go through router $R$: if path $K \rightarrow D$ passed across $R$, its cost could not be lower than the one of sub-path $R \rightarrow D$.

%107
In case more than one feasible successor exists, neighbor $X$ is selected offering the least-cost path toward destination $D$:
\[
 \min{ \{ L \left( R, X \right) + d \left( X, D \right) \} }
\]
where:
\begin{itemize}
 \item $L \left( R , X \right)$ is the cost of the link between router $R$ and its neighbor $X$;
 \item $d \left( X,D \right)$ is the distance between neighbor $X$ and destination $D$.
\end{itemize}

%112-113
The selected feasible successor is not guaranteed to be the neighbor which the best possible path toward the destination goes across. If the mechanism does not select the best neighbor, the latter will keep announcing the path which is really the best one without changing its cost $\Rightarrow$ the router will recognize the existence of a new, better path which was not selected and adopt the new path (positive status change).

\subsection{Diffusing process}
%114
If router $R$ can not find any feasible successor for the destination:
\begin{enumerate}
 \item it temporarily freezes the entry in its routing table related to the destination $\Rightarrow$ packets keep taking the old path, which definitely is free of loops and at most is no longer able to lead to the destination;
 \item it enters an \textbf{active state}:
 %115
 \begin{enumerate}
  \item it sends to each of its neighbors, but the next hop of the old path, a \textbf{query} message asking if it is able to find a path which is better than its old path and which is definitely free of loops;
  %116
  \item it waits for a \textbf{reply} message to be received from each of its neighbor;
  \item it chooses the best path exiting from the active state.
 \end{enumerate}
\end{enumerate}

Each neighbor router $X$ receiving the query message from router $R$ sends back a reply message containing its DV related to a path across it:
\begin{itemize}
 \item if router $R$ is not its next hop toward the destination, and therefore the cost of its path toward the destination has not been changed, then router $X$ reports that router $R$ can use that path;
 \item if router $R$ is its next hop toward the destination, then router $X$ should in turn set out to search for a new path, by either selecting a feasible successor or entering the active state too.
\end{itemize}

%97-98
\section{Advantages and disadvantages}
%151
\paragraph{Advantages}
\begin{itemize}
 \item very easy to implement, and protocols based on the DV algorithm are simple to configure;
 \item it requires limited processing resources $\Rightarrow$ cheap hardware in routers;
 \item suitable for small and stable networks with not too frequent negative status changes;
 %99
 \item the DUAL algorithm guarantees loop-free networks: no routing loops can occur, even in the transient (even though black holes are still tolerated).
\end{itemize}

\paragraph{Disadvantages}
\begin{itemize}
 \item the algorithm has an exponential worst case and it has a normal behavior between $O \left( n^2 \right)$ and $O \left( n^3 \right)$;
 \item convergence may be rather slow, proportional to the slowest link and the slowest router in the network;
 \item difficult to understand and predict its behavior in big and complex networks: no node has a map of the network $\Rightarrow$ it is difficult to detect possible routing loops;
 \item it may trigger routing loops due to particular changes in topology;
 %117
 \item additional techniques for improving its behavior make the protocol more complex, and they do not solve completely the problem of the missing topology knowledge anyway;
 \item the threshold `infinity' limits the usage of this algorithm only to small networks (e.g. with few hops).
\end{itemize}

%118
\section{The Path Vector algorithm}
\label{sez:path_vector}
The \textbf{Path Vector} (PV) algorithm adds information about the announced routes: also the path, that is the \ul{list of crossed nodes} along it, is announced:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.74\linewidth]{../pic/A1.3/118}
	\caption{Process generating the routing table and the new PV for node A.}
\end{figure}

The list of crossed nodes allows to avoid the appearance of routing loops: the receiving node is able to detect that the announced route crosses it by observing the presence of its identifier in the list, discarding it instead of propagating it $\Rightarrow$ paths crossing twice the same node can not form.

Path Vector is an intermediate algorithm between Distance Vector and Link State: it adds the strictly needed information about announced paths without having the complexity related to Link State where the whole network topology needs to be known.

\paragraph{Application} The PV algorithm is used in inter-domain routing by the BGP protocol (please refer to section~\ref{sez:bgp_path_vector}).