\chapter{Multicast routing}
\paragraph{Multicast routing protocols}
\begin{itemize}
%23
\item \ul{DVMRP}: DV (TRPB, RPM), unicast-protocol-independent, source-specific tree (sect.~\ref{sez:DVMRP});
\item \ul{MOSPF}: LS, works on OSPF protocol, source-specific tree (sect.~\ref{sez:MOSPF});
%24
\item \ul{PIM-DM}: DV (RPM), unicast-protocol-independent, source-specific tree;
\item \ul{PIM-SM}: CBT, unicast-protocol-independent, hybrid between shared and source-specific tree (sect.~\ref{sez:PIMSM});
\item \ul{BGMP} (Border Gateway Multicast Protocol): BGP-based inter-domain multicast routing protocol.
\end{itemize}

\section{DVMRP}
\label{sez:DVMRP}
%31a-26
\textbf{Distance Vector Multicast Routing Protocol} (DVMRP) was the first multicast routing protocol, and was used at the origins in \textbf{Multicast backbone} (Mbone), a virtual network born in 1992 in the IETF scope which relies for transmission on the same physical structure as the Internet to provide users with the possibility to exploit multicast for multimedia communications.

%25
DVMRP is based on the DV algorithm:
\begin{itemize}
\item version 1: it adopts TRPB\footnote{Please see section~\ref{sez:TRPB}.};
\item version 3: it adopts RPM\footnote{Please see section~\ref{sez:RPM}.}.
\end{itemize}

The DVMRP protocol instance is run in parallel with the unicast protocol instance: DVMRP ignores routing information from other protocols, and computes routes which can differ from the ones used for unicast traffic.

%24a
It uses a metric based on the \ul{hop count}, that is the number of mrouters which speak DVMRP. In DVMRP \textbf{tunnels} can be manually configured: the path connecting two DVMRP neighbors can include routers not supporting DVMRP (or on which DVMRP is disabled):
\begin{itemize}
%25a
\item \ul{local end-point}: it specifies the mrouter at the beginning of the tunnel;
\item \ul{remote end-point}: it specifies the mrouter at the other end of the tunnel;
%26a
\item \ul{metric}: it specifies the cost measure of the tunnel;
\item \ul{threshold}: it specifies the minimum TTL value which a packet needs to have in order to be routed through the tunnel $\Rightarrow$ it allows to define the packet visibility: packets which must not exit the corporate network are generated with a TTL equal to the threshold.
\end{itemize}

\section{MOSPF}
\label{sez:MOSPF}
%27
\textbf{Multicast OSPF} (MOSPF) is a multicast routing protocol based on the LS algorithm\footnote{Please see section~\ref{sez:Link_State_multicast_routing}.} which extends OSPF, enabling single routers to have a full knowledge of the network topology and costs related to single links.

%29
MOSPF is backward-compatible with OSPF: MOSPF routers can be mixed with OSPF-only routers, although multicast packets are forwarded only among MOSPF routers and the paths chosen for multicast packets do not cross OSPF-only routers.

%30
MOSPF adds \textbf{LSA type 6}:
\begin{itemize}
\item MOSPF \ul{internal routers} produce LSAs type 6 to inform the other routers within their area of multicast groups being active on their networks;
\item MOSPF \ul{ABRs} produce LSAs type 6 to inform the other routers within area 0 of multicast groups being active on their edge areas (even by aggregation).
\end{itemize}

\section{PIM}
%31
\textbf{Protocol Independent Multicast} (PIM) directly uses tables containing routing information independently of the underlying unicast protocol (DV or LS) which has built them.

%32
It exists in two versions, incompatible among them, dealing with different issues related to spatiality of receivers:
\begin{itemize}
%33
\item \textbf{Dense Mode} (DM):
\begin{itemize}
\item it is suitable for \ul{small networks} (LAN/MAN) with a lot of \ul{concentrated receivers};
\item it is greedy in terms of bandwidth: packets can be forwarded to areas not interested in the particular multicast group;
\item it adopts the DV-based \ul{RPM algorithm} (like DVMRPv3);
\item \ul{Implicit Join Protocol}: in the absence of explicit Prune messages, packets are forwarded to a certain network;
\item \ul{Prune messages} should be generated to stop multicast traffic (like DVMRP);
\end{itemize}

%37
\item \textbf{Sparse Mode} (SM):
\begin{itemize}
\item it is suitable for \ul{wide networks} (WAN) with few \ul{scattered receivers};
\item it limits as much as possible the bandwidth overhead: it never uses flooding;
\item it is an evolution to the \ul{CBT algorithm}\footnote{Please see section~\ref{sez:core_based_tree}.}: it always starts from a shared tree, which becomes a source-specific tree when advantageous;
\item \ul{Explicit Join Protocol}: in the absence of explicit Join messages, packets are not forwarded to a certain network (routing information however goes to all routers, not only the ones which will receive traffic);
\item \ul{Join messages} should be generated to start multicast traffic (like MOSPF).
\end{itemize}
\end{itemize}

\subsection{PIM-SM}
\label{sez:PIMSM}
%38
PIM-SM handles two kinds of distribution trees:
\begin{itemize}
%39
\item \textbf{RP-Tree} (RPT): it is the shared tree used at the beginning for all the packets of the multicast group, but is not optimized based on the source $\Rightarrow$ the first packets of the transmission can be delivered in a short time to receivers without having to wait for the tree computation:
\begin{itemize}
\item no shortest paths are used;
\item traffic is centralized;
\item one tree is corresponding to each multicast group;
\end{itemize}

%40
\item \textbf{Shortest-Path Tree} (SPT): it is the source-specific tree built at a later time if routers believe it is convenient (it is not mandatory):
\begin{itemize}
\item shortest paths are used;
\item traffic is distributed;
\item one tree is corresponding to each (group, source) pair.
\end{itemize}
\end{itemize}

PIM-SM defines three special nodes:
\begin{itemize}
\item \textbf{rendez-vous point} (RP): it is the core router, elected based on an algorithmic formula starting from a list called RP-set, which is in charge of:
\begin{itemize}
\item receiving Join messages by DRs;
\item receiving registration requests by sources;
\item receiving and sending to DRs the first multicast data packets transmitted by a source;
\end{itemize}

%50
\item \textbf{designated router} (DR): it is the router, elected based on the shortest path toward the RP (or on the higher IP address if there is a tie), which is in charge of:
\begin{itemize}
\item receiving subscription requests by hosts in a certain LAN;
\item sending the Join message to the RP to join the RPT;
\item sending the Join message to the source to join the SPT;
\item sending the Join message periodically to adapt to group changes;
\end{itemize}
%51
\item \textbf{bootstrap router} (BSR): it is the router, elected based on the best administrative cost (or on the higher IP address if there is a tie), which is in charge of distributing the RP-set to the whole PIM-SM domain.
\end{itemize}

\subsubsection{Algorithm}
\paragraph{RPT}
\begin{enumerate}
%41
\item \ul{subscription of DRs as receivers}: each DR sends a Join message to the RP;
%42
\item \ul{registration of the source as a transmitter}: the source sends a registration request to the RP;
%43
\item \ul{registration of the RP as a receiver}: the RP sends a Join message to the source;
\item \ul{transmission along the RPT}: the source sends in multicast the first data packets, forwarded by intermediate routers up to the RP;
\item \ul{propagation along the RPT}: the RP propagates in multicast the received data packets, forwarded by intermediate routers up to the DRs.
\end{enumerate}

\paragraph{SPT}
\begin{enumerate}
%44
\item \ul{subscription of DRs as receivers}: each DR sends a Join message to the source;
%45
\item \ul{transmission along the SPT}: the source sends in multicast data packets, forwarded by intermediate routers to each DR (besides the RP);
%46
\item \ul{detachment from the RPT}: each DR sends a Prune message to the RP;
%47-48
\item \ul{joining to the SPT}: another DR sends a Join message to the RP, then sends a Join message to the source.
\end{enumerate}