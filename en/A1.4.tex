\chapter{The Link State algorithm}
\label{cap:Link_State}
%120-121
The \textbf{Link State} (LS) algorithm is based on distribution of information about the neighborhood of the router over the whole network. Each node can create the map of the network (the same for all nodes), from which the routing table has to be obtained.

%122
\section{Components}
\begin{itemize}
 \item \ul{Neighbor Greeting}s (section~\ref{sez:ls_neighbor_greeting})
 \item \ul{Link State}s (section~\ref{sez:ls_link_state})
 \item \ul{flooding algorithm} (section~\ref{sez:ls_flooding})
 \item \ul{Dijkstra's algorithm} (section~\ref{sez:ls_dijkstra})
 \item \ul{adjacency bring-up} (section~\ref{sez:ls_riallineamento})
\end{itemize}

%123
\subsection{Neighbor Greetings}
\label{sez:ls_neighbor_greeting}
Neighbor Greetings are messages periodically exchanged between adjacent nodes to collect information about adjacencies. Each node:
\begin{itemize}
 \item sends Neighbor Greetings to report its existence to its neighbors;
 \item receives Neighbor Greetings to learn which are its neighbors and the costs to reach them.
\end{itemize}

Neighbor Greetings implement \textbf{fault detection} based on a maximum number of consecutive Neighbor Greetings not received:
\begin{itemize}
 \item \ul{fast}: Neighbor Greetings can be sent with a high frequency (e.g. every 2 seconds) to recognize variations on adjacencies in a very short time:
 \begin{itemize}
  \item once they are received, they are not propagated but stop on the first hop $\Rightarrow$ they do not saturate the network;
  \item they are packets of small size because they do not include information about nodes other than the generating node;
  \item they require a low overhead for routers, which are not forced to compute again their routing table whenever they receive one of them;
 \end{itemize}
 \item \ul{reliable}: it does not rely on the `link-up' signal, unavailable in presence of hubs.
\end{itemize}

\subsection{Link States}
\label{sez:ls_link_state}
%124
Each router generates a LS, which is a set of adjacency-cost pairs:
\begin{itemize}
 \item \ul{adjacency}: all the neighbors of the generating node;
 \item \ul{cost}: the cost of the link between the generating router and its neighbor.
\end{itemize}

%125
Each node stores all the LSs coming from all nodes in the network into the \textbf{Link State Database}, then it scans the list of all adjacencies and builds a graph by merging nodes (routers) with edges (links) in order to build the map of the network.

%133-134
LS generation is mainly \textbf{event-based}: a LS is generated following a change in the local topology (= in the neighborhood of the router):
\begin{itemize}
 \item the router has got a new neighbor;
 \item the cost to reach a neighbor has changed;
 \item the router has lost its connectivity to a neighbor previously reachable.
\end{itemize}

Event-based generation:
\begin{itemize}
 \item allows a \ul{better utilization} of the network: it does not consume bandwidth;
 \item it requires the `hello' component, based on Neighbor Greetings, as the router can no longer use the periodic generation for detecting faults toward its neighbors.
\end{itemize}

In addition, routers implement also a periodic generation, with a very low frequency (in the order of tens of minutes):
\begin{itemize}
 \item this increases \ul{reliability}: if a LS for some reason goes lost, it can be sent again without having to wait for the next event;
 \item this allows to include an \ul{`age' field}: the entry related to a disappeared destination remains in the routing table and packets keep being sent to that destination until the piece of information, if not refreshed, ages enough that it can be deleted.
\end{itemize}

\subsection{Flooding algorithm}
\label{sez:ls_flooding}
%126
Each LS must be sent in `broadcast' to all the routers in the network, which must receive it unchanged $\Rightarrow$ real protocols implement a sort of \textbf{selective flooding}, representing the only way to reach all routers with the same data and with minimum overhead. Broadcast is limited only to LSs, to avoid saturating the network.

%124
LS propagation takes place at a high speed: unlike DVs, each router can immediately propagate the received LS and in a later time process it locally.

%135
Real protocols implement a reliable mechanism for LS propagation: every LS must be confirmed `hop by hop' by an acknowledgment, because the router must be sure that the LS sent to its neighbors has been received, also considering that LSs are generated with a low frequency.

\subsection{Dijkstra's algorithm}
\label{sez:ls_dijkstra}
%127
After building the map of the network from its adjacency lists, each router is able to compute the \textbf{spanning tree} of the graph, that is the tree with least-cost paths having the node as a root, thanks to the Dijkstra algorithm: on every iteration all the links are considered connecting nodes already selected with nodes not yet selected, and the closest adjacent node is selected.\footnote{Please refer to section \textit{Algoritmo di Dijkstra} in chapter \textit{I cammini minimi} in lecture notes `Algoritmi e programmazione'.}

%131
All nodes have the same Link State Database, but each node has a different routing tree to the destinations, because the obtained spanning tree changes as the chosen node as a root changes:
\begin{itemize}
 \item \ul{better distribution of the traffic}: reasonably there are no unused links (unlike Spanning Tree Protocol);
 \item obviously the routing tree must be \ul{consistent} among the various nodes.
\end{itemize}

\subsection{Adjacency bring-up}
\label{sez:ls_riallineamento}
Bringing up adjacencies is required to synchronize Link State Databases of routers when a new adjacency is detected:
\begin{itemize}
 %136
 \item a \ul{new node} connects to the network: the adjacent node communicates to it all the LSs related to the network, to populate its Link State Database from which it will be able to compute its routing table;
 %137
 \item two \ul{partitioned subnetworks} (e.g. due to a failure) are re-connected together: each of the two nodes at the link endpoints communicates to the other node all the LSs related to its subnetwork.
\end{itemize}

%138
\paragraph{Procedure}
\begin{enumerate}
 \item a new adjacency is detected by the `hello' protocol, which keeps adjacencies under control;
 \item synchronization is a point-to-point process, that is it affects only the two routers at the endpoints of the new link;
 \item the LSs which had previously been unknown are sent to other nodes in the network in flooding.
\end{enumerate}

\section{Behaviour over broadcast data-link-layer networks}
%139
The LS algorithms models the network as a set of point-to-point links $\Rightarrow$ it suffers in presence of broadcast\footnote{To be precise, on all the data-link-layer networks with multiple access (e.g. also on Non-Broadcast Multiple Access [NBMA]).} data-link-layer networks (such as Ethernet), where any entity has direct access to any other entity on the same data link (shared bus), hence creating a full-mesh set of adjacencies ($N$ nodes $\rightarrow$ $\tfrac{N \left( N-1 \right)}{2}$ point-to-point links).

%140-141
The high number of adjacencies has a severe impact on the LS algorithm:
\begin{itemize}
 %132-145
 \item \ul{computation problems}: the convergence of the Dijkstra's algorithm depends on the number of links ($L \cdot \log{N}$), but the number of links explodes on broadcast networks;
 \item \ul{unnecessary overhead when propagating LSs}: whenever a router needs to send its LS on the broadcast network, it has to generate $N-1$ LSs, one for every neighbor, even if it would be enough to send it only once over the shared channel to reach all its neighbors, then each neighbor will in turn propagate multiple times the received LS ($\sim N^2$);
 \item \ul{unnecessary overhead when bringing up adjacencies}: whenever a new router is added to the broadcast network, it has to start $N-1$ bring-up phases, one for every neighbor, even if it would be enough to re-align the database just with one of them.
\end{itemize}

%142
The solution is to transform the broadcast topology in a \textbf{star topology}, by adding a pseudo-node (NET): the network is considered an active component that will start advertising its adjacencies, becoming the center of a virtual star topology:
\begin{itemize}
 \item one of the routers is `promoted' which will be in charge of sending also those additional LSs on behalf of the broadcast network;
 \item all the other routers advertise an adjacency to that node only.
\end{itemize}

%143-144
The virtual star topology is valid only for LS propagation and adjacency bring-up, while normal data traffic still uses the real broadcast topology:
 \begin{itemize}
  \item \ul{LS propagation}: the generating node sends a LS to the pseudo-node, which sends it to the other nodes ($\sim N$);
  \item \ul{adjacency bring-up}: the new node activates a bring-up phase only with the pseudo-node.
 \end{itemize}
 
%147
\section{Advantages}
\begin{itemize}
 \item \ul{fast convergence}:
 \begin{itemize}
  \item LSs are quickly propagated without any intermediate processing;
  \item every node has certain information because coming directly from the source;
 \end{itemize}
 \item \ul{short duration of routing loops}: they may happen during transient for a limited amount of time;
 \item \ul{debug simplicity}: each node has a map of the network, and all nodes have identical databases $\Rightarrow$ it is enough to query a single router to have the full map of the network in case of need;
 \item \ul{good scalability}, although it is better not to have large domains (e.g. OSPF suggests not to have more than 200 routers in a single area).
\end{itemize}