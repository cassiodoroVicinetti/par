\chapter{Inter-domain routing: peering and transit in the Internet}
%20
Traffic within the AS is almost `free', excluding infrastructure costs (maintenance, administration, electricity, etc.) $\Rightarrow$ ISPs try to convince users to spend most of their time inside the AS.

However, an AS should connect to other ASes for two reasons:
\begin{itemize}
 \item an AS must be able to reach all the destinations present in the Internet for Metcalfe's law (= the network must be as more extended as possible to be useful);
 \item an AS would like to achieve resilience in its connections toward the outside world.
\end{itemize}

%13-31
ASes on the Internet are interconnected by a \ul{hierarchical organization}:
\begin{itemize}
 \item \textbf{Tier 1} (e.g. Seabone, Sprint): international operator interconnecting major towns by long-distance, broadband links and transporting big traffic flows along backbones;
 \item \textbf{Tier 2} (e.g. Telecom Italia): national operator collecting traffic from single users through a lot of access points thanks to its house-to-house presence throughout the territory;
 \item \textbf{Tier 3}: local operator serving a very restricted geographical area.
\end{itemize}

\section{Commercial agreements among ASes}
\label{sez:accordi_as}
%21
Interconnections between an operator and another one may not come for free: usually, the interconnection between two ASes is established only upon an \textbf{economic agreement}. Two types of agreements are possible:
\begin{itemize}
 \item \ul{transit}: it represents the most natural choice from the economic viewpoint (section~\ref{sez:transito});
 \item \ul{peering}: when two ASes discover that they can do better (section~\ref{sez:peering}).
\end{itemize}

%28-59
Inter-domain routing over the Internet is mainly driven by commercial agreements among operators at various hierarchical levels:
\begin{itemize}
 %32-33
 \item Tier 1: it can advertise, independently of the geographical coverage of its network, the reachability of the \ul{full route} (0.0.0.0/0), that is the reachability of (almost) every destination AS on the Internet, without having to buy transit from other providers or to pay some access fee;
 %34
 \item Tier 2: it needs to buy transit from a Tier-1 operator in order to be able to reach the whole Internet, and it can establish a lot of \ul{peering} agreements with other Tier-2 providers;
 \item Tier 3: it has not any peering agreement, and simply buys \ul{transit} from a Tier-2 (or Tier-1) provider.
\end{itemize}

%22
\subsection{Transit}
\label{sez:transito}
An agreement is \textbf{transit} when an ISP has to pay another ISP to connect to its AS. The ISP receiving the money guarantees the `transit', that is the right to use its network, to the traffic coming from the other AS.

The economic agreement may establish:
\begin{itemize}
 \item the payment method:
\begin{itemize}
 \item \ul{fee by volume}: a maximum amount of bytes of data per day or per month, plus additional cost for traffic exceeding that amount;
 \item \ul{flat fee}: a monthly fee for a maximum bandwidth (the bandwidth can be limited via software on the access interface).
\end{itemize}
\item which destinations are reachable through the transit:
\begin{itemize}
 \item \ul{full route}: all destinations around the world must be reachable;
 \item only destinations in a certain \ul{geographical area} (e.g. USA): packets directed toward other destinations are dropped.
\end{itemize}
\end{itemize}

The price may be influenced by the importance of the ISP selling the transit:
\begin{itemize}
 \item an US ISP has control of the most important part of the network because inside its AS there are the most visited web servers in the world;
 \item a very large ISP can offer a good reachability with the rest of the world thanks to its high number of interconnections.
\end{itemize}

%23
\subsection{Peering}
\label{sez:peering}
An agreement is \textbf{peering} when two peer ISPs agree to exchange traffic between themselves without having to pay each other.

Two ISPs can decide to stipulate a peering agreement if they determine that the costs for direct interconnection are lower than the costs for buying transit from each other: costs for setup and maintenance of the direct link between the ASes are equally split by the two ISPs, which can send data at the full speed allowed by the link.

Tier-1 operators work in a very competitive market:
\begin{itemize}
 %35
 \item Tier-2 operators can establish new peering agreements among themselves as soon as they become more convenient than transit;
 \item a Tier-2 operator can shortly move to a more convenient Tier-1 operator;
 %38
 \item a dominant operator may be forced by the market guarantor to offer peering connections with minor ISPs.
\end{itemize}

\section{Routing policies}
\label{sez:politiche_interdominio}
%171[A1]-3[A9]
In inter-domain routing, other requirements are more important than simple network connectivity:
\begin{itemize}
 \item \ul{economic} (who pays for the bandwidth?): sometimes longer paths may be preferred to best paths (section~\ref{sez:interdominio_economici});
 \item \ul{administrative} (is it allowed to go?): sometimes some paths are omitted to the other party (section~\ref{sez:interdominio_amministrativi});
 \item \ul{security} (is that administrative domain trusted?): sometimes safer (and longer) paths may be preferred to best paths (section~\ref{sez:interdominio_sicurezza}).
\end{itemize}

%9-19-24
The path chosen by the routing protocol is not necessarily the least-cost path from the technical point of view, but it is the best path among the ones which satisfy the constraints established by \textbf{routing policies} configured by the network administrator, which reflect commercial agreements among ASes.

%49[A9]
The \textbf{decision process} on border routers is affected by routing policies:
\begin{itemize}
 %174[A1]
 \item \ul{routing table}: the choice of some cheaper routes can be favoured and the choice of other ones across untrusted ASes can be discouraged;
 %178[A1]
 \item \ul{route advertisements}: the routes announced toward other ASes may not correspond to the actual network topology.
\end{itemize}

%172[A1]
\subsection{Economic requirements}
\label{sez:interdominio_economici}
\begin{figure}
	\centering
	\includegraphics[scale=1.0175]{../pic/A1.6/172}
	\caption{Example of freeriding.}
	\label{fig:requisiti_economici}
\end{figure}

\noindent
Sending traffic on a transit link costs $\Rightarrow$ an AS can take advantage of a peering link, even if it is not a direct link, to make the other peer AS pay the transit cost (\textbf{freeriding}).

In the example in figure~\ref{fig:requisiti_economici}, two Italian ASes A and B are interconnected in peering, and each of them is connected in transit with US AS C. The best path according to the traditional routing rules is path $x$ because it is made up of a direct link, but A needs to pay to make traffic go through that link. A can set a policy which prefers a \ul{cheaper path} $y$: it deviates all the traffic directed to C to the link toward B, which is a low-cost link for A $\Rightarrow$ B will send A's traffic to its transit link toward C, paying instead of A.
\FloatBarrier

%175[A1]
\subsection{Administrative requirements}
\label{sez:interdominio_amministrativi}
\begin{figure}
	\centering
	\includegraphics[scale=1.0175]{../pic/A1.6/175}
	\caption{Example of route hiding.}
	\label{fig:requisiti_amministrativi}
\end{figure}

\noindent
An AS can set a routing policy in order not to announce connectivity with other ASes to an AS (\textbf{route hiding}).

In the example in figure~\ref{fig:requisiti_amministrativi}, B has a transit link toward C and uses it for its traffic, but advertizes \ul{partial connectivity} by omitting the information about this link in the advertisements which it sends to A, in order to avoid that A takes advantage of the peering link to save on the transit cost (and vice versa). A could not trust this advertisement and in turn set a policy forcing statically all traffic toward C to be sent to B anyhow $\Rightarrow$ B can defend itself by setting an \textbf{Access Control List} (ACL) on its border router to discard all packets coming from A and directed toward C.
\FloatBarrier

%173[A1]
\subsection{Security requirements}
\label{sez:interdominio_sicurezza}
\begin{figure}
	\centering
	\includegraphics[scale=1.0175]{../pic/A1.6/173}
	\caption{Example of untrusted operator.}
	\label{fig:requisiti_sicurezza}
\end{figure}

\noindent
A network operator can represent a security threat because for example is used to make sniffing actions on traffic crossing its AS $\Rightarrow$ an AS would like to avoid that its traffic directed to other ASes go through that untrusted operator.

In the example in figure~\ref{fig:requisiti_sicurezza}, A to reach C prefers a longer but \ul{safer path} $x$ because it does not cross untrusted operator B, even if the latter is advertising low-cost path $y$ toward C.
\FloatBarrier

\section{Internet Exchange Point}
%47
Interconnecting two ASes by \textbf{direct connection}, that is by a single wide-area link between them, is not convenient:
\begin{itemize}
 \item \ul{link cost}: its installation may require digging operations;
 \item \ul{cost of interfaces on routers}: they have to send the signal over long distances;
 \item \ul{flexibility}: intervention is necessary on the physical infrastructure to create a new interconnection.
\end{itemize}

%45
An \textbf{Internet Exchange Point} (IXP) allows multiple border routers of different ASes (ISPs) to exchange external routing information in a more dynamic and flexible way.

%48-49
Routers are connected through an intermediate \textbf{data-link-layer} Local Area Network: technically all routers are directly reachable, but in practice routing policies define interconnections according to commercial agreements among ASes $\Rightarrow$ to create a new interconnection, it is sufficient to configure routing policies on single routers without having to change the physical infrastructure. An interconnection can also be active but used just as a backup (selection done in BGP).

%50-51
Usually each AS pays a monthly fee, depending on the speed of the connection to the IXP. The IXP is in charge of the technical functioning of switches within the intermediate network:
\begin{itemize}
 \item \ul{single location}: often all routers are concentrated inside a room in a datacenter, where they are provided with:
 \begin{itemize}
 %46
 \item high-speed data-link-layer network;
 \item electrical power, conditioning system;
 \item monitoring service;
 \item proximity to optical-fiber backbones;
 \end{itemize}
 \item \ul{distributed infrastructure}: multiple access points are available in the main towns over the territory (for example, TOPIX runs across the entire Piedmont region).
\end{itemize}

%48
The IXP is also known as \textbf{Neutral Access Point} (NAP): the IXP has to be neutral and uninvolved in its customers' business. An IXP can decide to disallow transit agreements: for example, MIX in Milan is a nonprofit organization which only admits peering agreements to favour internet diffusion in Italy, but this may limit the amount of traffic exchanged across the IXP because ISPs available only for transit agreements will choose other IXPs.

\section{Network neutrality}
\textbf{Network neutrality} is the principle according to which all traffic should be treated equally, without privileging or damaging a part of traffic for economic interests.

%55
Network operators can be tempted to give `preferential treatment' to portions of traffic:
\begin{itemize}
 \item \ul{privilege} some traffic: offer a better service for a certain kind of traffic (e.g. higher speed);
 \item \ul{damage} some traffic: offer a worse service, or no service at all, for a certain kind of traffic.
\end{itemize}

%56-57
A neutral network guarantees that all entities (e.g. content providers) have the same service, without making some service be killed at the discretion of the network operator, but enforcing `pure' network neutrality implies that traffic control, which may be useful in many cases, is not possible at all; on the other end, if it is admitted that the network may not be neutral, the network operator is given the power to privilege some traffic or content. In an open market the ball is leaved to the user: if users do not agree that their VoIP traffic is discriminated, they can switch to another network operator (although in practice this may not always be possible due to cartels among network operators).

\paragraph{Examples of non-neutrality}
\begin{itemize}
%53-54
\item \ul{content providers}: ISPs would like to have a part of revenues of content providers $\Rightarrow$ an ISP may privilege traffic directed to a content provider with which it stipulated a revenue sharing agreement;
\item \ul{peer-to-peer} (P2P):
\begin{itemize}
 %29-30
 \item end users do not care about destination of their traffic, but P2P traffic can reach every user in every AS around the world making the ISP pay high costs $\Rightarrow$ an ISP may privilege traffic which is generated within the AS (e.g. AdunanzA by Fastweb);
 \item P2P traffic is more symmetric because it uses a lot the upload bandwidth, while networks have been sized to support asymmetric traffic $\Rightarrow$ an ISP may privilege asymmetric traffic (e.g. normal web traffic);
\end{itemize}
%53
\item \ul{quality of service} (QoS): an ISP may privilege traffic with a higher priority level (e.g. VoIP traffic);
%56
\item \ul{security}: an ISP may block traffic from malicious users (e.g. DDoS attack).
\end{itemize}